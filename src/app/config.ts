import {RestApiPath} from './service/rest-api/rest-api-path';

// DISTRIBUTION
export const PATH_DISTRIBUTION_YEARS = new RestApiPath('/distribution/years/model',
  {post: true, get: true});
export const PATH_DISTRIBUTION_YEARS_WITH_ID = new RestApiPath('/distribution/years/model/:id',
  {get: true, put: true, del: true});
export const PATH_DISTRIBUTION_ACTIVE_YEAR = new RestApiPath('/distribution/years/active',
  {get: true});
export const PATH_DISTRIBUTION_ACTIVE_YEAR_WITH_ID = new RestApiPath('/distribution/years/active/:id',
  {put: true});

export const PATH_DISTRIBUTION_PROMOTIONS = new RestApiPath('/distribution/promotions/model',
  {post: true, get: false});
export const PATH_DISTRIBUTION_PROMOTIONS_WITH_ID = new RestApiPath('/distribution/promotions/model/:id',
  {get: true, put: true, del: true});

export const PATH_DISTRIBUTION_CATEGORIES = new RestApiPath('/distribution/categories/model',
  {post: true, get: true});
export const PATH_DISTRIBUTION_CATEGORIES_WITH_ID = new RestApiPath('/distribution/categories/model/:id',
  {get: true, put: true, del: true});

export const PATH_DISTRIBUTION_PHASES = new RestApiPath('/distribution/phases/model',
  {post: true, get: true});
export const PATH_DISTRIBUTION_PHASES_WITH_ID = new RestApiPath('/distribution/phases/model/:id',
  {get: true, put: true, del: true});
export const PATH_DISTRIBUTION_PHASE_ACTIVE_WITH_ID = new RestApiPath('/distribution/phases/active/:id',
  {put: true});
export const PATH_DISTRIBUTION_PHASES_BY_USER = new RestApiPath('/distribution/phases/getByUser',
  {get: true});
export const PATH_DISTRIBUTION_PHASES_BY_PROMOTION_AND_YEAR = new RestApiPath('/distribution/phases/year/:yearId/promotion/:promotionId',
  {get: true});

export const PATH_DISTRIBUTION_LOCATIONS = new RestApiPath('/distribution/locations/model',
  {post: true, get: true});
export const PATH_DISTRIBUTION_LOCATIONS_WITH_ID = new RestApiPath('/distribution/locations/model/:id',
  {get: true, put: true, del: true});

export const PATH_DISTRIBUTION_PERIODS = new RestApiPath('/distribution/periods/model',
  {post: true, get: true});
export const PATH_DISTRIBUTION_PERIODS_WITH_ID = new RestApiPath('/distribution/periods/model/:id',
  {get: true, put: true, del: true});
export const PATH_DISTRIBUTION_PERIODS_BLOCKED = new RestApiPath('/distribution/periods/blocked',
  {get: true});

export const PATH_DISTRIBUTION_TERRAINTYPES = new RestApiPath('/distribution/terraintypes/model',
  {post: true, get: true});
export const PATH_DISTRIBUTION_TERRAINTYPES_WITH_ID = new RestApiPath('/distribution/terraintypes/model/:id',
  {get: true, put: true, del: true});
export const PATH_DISTRIBUTION_TERRAINTYPES_OPEN_WITH_YEAR_ID_AND_PROMOTION_ID = new RestApiPath('/distribution/terraintypes/open/year/:yearId/promotion/:promotionId',
  {get: true});

export const PATH_DISTRIBUTION_TERRAIN = new RestApiPath('/distribution/terrains/model',
  {post: true, get: true});
export const PATH_DISTRIBUTION_TERRAIN_WITH_ID = new RestApiPath('/distribution/terrains/model/:id',
  {get: true, put: true, del: true});
export const PATH_DISTRIBUTION_TERRAIN_WITH_YEAR_ID_AND_PROMOTION_ID = new RestApiPath('/distribution/terrains/year/:yearId/promotion/:promotionId',
  {get: true, post: true});

export const PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION = new RestApiPath('/distribution/terrain-attributions/model',
  {post: true, get: true});
export const PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_WITH_ID = new RestApiPath('/distribution/terrain-attributions/model/:id/phase/:phaseId',
  {get: true, put: true, del: true});
export const PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_BY_USER = new RestApiPath('/distribution/terrain-attributions/user/:userId',
  {get: true});
export const PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_PRIVATE = new RestApiPath('/distribution/terrain-attributions/private/model',
  {post: true});
export const PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_WITH_ID_PRIVATE = new RestApiPath('/distribution/terrain-attributions/private/model/:id',
  {del: true});
export const PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_BY_PHASE = new RestApiPath('/distribution/terrain-attributions/private/by-phase/:phaseId',
  {get: true});
export const PATH_DISTRIBUTION_ADMIN_TERRAIN_ATTRIBUTION = new RestApiPath('/distribution/terrain-attributions/admin/model',
  {get: true});
export const PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_MULTIPLE = new RestApiPath('/distribution/terrain-attributions/admin/terrain/:terrainId',
  {put: true, del: true});

export const PATH_DISTRIBUTION_EXPORT_PDF = new RestApiPath('/distribution/exportation/pdf',
  {get: true});
export const PATH_DISTRIBUTION_EXPORT_CSV = new RestApiPath('/distribution/exportation/csv',
  {get: true});
// END DISTRIBUTION

// AUTH
export const PATH_AUTH_LOGIN = new RestApiPath('/auth/token',
  {post: false});
export const PATH_AUTH_REFRESHTK_ISVLD = new RestApiPath('/auth/token/refresh/isValid',
  {get: false});
export const PATH_AUTH_ACCESSTK_ISVLD = new RestApiPath('/auth/token/access/isValid',
  {get: false});
export const PATH_AUTH_ACCESSTK_REFRESH = new RestApiPath('/auth/token/access/refresh',
  {get: false});
// END AUTH


// HOME CONTENT
export const PATH_PAGE_CONTENT = new RestApiPath('/pages/content',
  {post: true});
export const PATH_PAGE_CONTENT_WITH_NAME = new RestApiPath('/pages/content/:name',
  {get: false});
export const PATH_PAGE_NAMES = new RestApiPath('/pages/names',
  {get: false});
// END HOME CONTENT

// USER
export const PATH_USER = new RestApiPath('/user/model',
  {post: false, get: true});
export const PATH_USER_WITH_ID = new RestApiPath('/user/model/:id',
  {get: true, put: true});
export const PATH_USER_UPDATE_PASSWORD = new RestApiPath('/user/update-password',
  {put: true});
export const PATH_USER_ADMIN = new RestApiPath('/user/admin/:userId',
  {get: true, post: true});
export const PATH_USER_GROUP = new RestApiPath('/user/group',
  {put: true, get: true});
export const PATH_USER_BY_USER_PROMOTION = new RestApiPath('/user/promotion',
  {get: true});
export const PATH_USER_BY_PROMOTION = new RestApiPath('/user/promotion/:id',
  {get: true});
export const PATH_USER_REDOUBLING = new RestApiPath('/user/redoubling',
  {put: true});
export const PATH_USER_SWITCH_PROMOTION = new RestApiPath('/user/switch/from/:promotionFromId/to/:promotionToId',
  {put: true});

export const PATH_USER_REQUEST_PASSWORD = new RestApiPath('/user/request-password',
  {post: false});
export const PATH_USER_RESET_PASSWORD = new RestApiPath('/user/reset-password',
  {post: false});
export const PATH_USER_RESET_PASSWORD_WITH_TOKEN = new RestApiPath('/user/reset-password/:token',
  {get: false});
// END USER


// CONTACTS
export const PATH_CONTACT = new RestApiPath('/contacts/model',
  {post: true, get: true});
export const PATH_CONTACT_WITH_ID = new RestApiPath('/contacts/model/:id',
  {get: true, put: true, del: true});
// END CONTACTS


// DOCUMENTS
export const PATH_FILE_DOCUMENT = new RestApiPath('/files/documents/model',
  {post: true, get: true});
export const PATH_FILE_DOCUMENT_WITH_ID = new RestApiPath('/files/documents/model/:id',
  {get: true, del: true});
export const PATH_FILE_DOCUMENT_BY_PROMOTION = new RestApiPath('/files/documents/promotion/:promotionId',
  {get: true});
// END DOCUMENTS

// LOGS
export const PATH_LOGS = new RestApiPath('/logs/model',
  {get: true});
// END LOGS

// GARDES
const GARDES_MANAGEMENT = '/gardes-management';
export const PATH_GARDE_TERRAIN = new RestApiPath(GARDES_MANAGEMENT + '/terrains/',
  {post: true, get: true});
export const PATH_GARDE_TERRAIN_WITH_ID = new RestApiPath(GARDES_MANAGEMENT + '/terrains/:id',
  {get: true, put: true, del: true});

export const PATH_GARDE_CONFIG_WITH_YEAR_ID = new RestApiPath(GARDES_MANAGEMENT + '/config/:yearId',
  {get: true, put: true});
export const PATH_GARDE_WEEK_HOLYDAYS_BY_DATE = new RestApiPath(GARDES_MANAGEMENT + '/holydays',
  {get: true});

export const PATH_GARDE_PLANNING_WITH_YEAR_ID = new RestApiPath(GARDES_MANAGEMENT + '/planning/:yearId',
  {get: true, put: true});

export const PATH_GARDES = new RestApiPath(GARDES_MANAGEMENT + '/gardes',
  {get: true});
export const PATH_GARDES_WITH_ID = new RestApiPath(GARDES_MANAGEMENT + '/gardes/:id',
  {put: true});
export const PATH_GARDES_FREE = new RestApiPath(GARDES_MANAGEMENT + '/free',
  {get: true});
export const PATH_GARDES_FREE_WITH_ID = new RestApiPath(GARDES_MANAGEMENT + '/free/:gardeId',
  {put: true});
// END GARDES

// MAILING

export const PATH_MAILING_CSV = new RestApiPath('/mailing/:promotionId',
  {get: true});
// END MAILING
