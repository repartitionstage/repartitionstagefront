import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminGuard} from '../../guard/admin/admin.guard';
import {AdminHomeComponent} from './admin-home/admin-home.component';
import {YearListComponent} from './distribution/configuration/year-list/year-list.component';
import {PromotionListComponent} from './distribution/configuration/promotion-list/promotion-list.component';
import {LocationListComponent} from './distribution/configuration/location-list/location-list.component';
import {CategoryListComponent} from './distribution/configuration/category-list/category-list.component';
import {PeriodListComponent} from './distribution/configuration/period-list/period-list.component';
import {TerrainTypeListComponent} from './distribution/configuration/terrain-type-list/terrain-type-list.component';
import {TerrainListComponent} from './distribution/configuration/terrain-list/terrain-list.component';
import {PageContentComponent} from './page-content/page-content.component';
import {ManageHomeComponent} from './distribution/manage/manage-home/manage-home.component';
import {ManagePromotionComponent} from './distribution/manage/manage-promotion/manage-promotion.component';
import {PdfExportComponent} from './distribution/exportation/pdf-export/pdf-export.component';
import {UserListComponent} from './user/user-list/user-list.component';
import {UserProfileComponent} from './user/user-profile/user-profile.component';
import {ContactComponent} from './contact/contact.component';
import {DocumentComponent} from './document/document.component';
import {AttributionsPerUserComponent} from './distribution/attributions-per-user/attributions-per-user.component';
import {PromotionSwitcherComponent} from './user/promotion-switcher/promotion-switcher.component';
import {LogsComponent} from './logs/logs.component';
import {GardeTerrainListComponent} from './gardes/garde-terrain-list/garde-terrain-list.component';
import {GardeYearConfigComponent} from './gardes/garde-year-config/garde-year-config.component';
import {GardeExportComponent} from './gardes/garde-export/garde-export.component';
import {GardePerUserComponent} from './gardes/garde-per-user/garde-per-user.component';
import {CsvExportComponent} from './distribution/exportation/csv-export/csv-export.component';
import {MailingComponent} from './mailing/mailing.component';


const routes: Routes = [
  {
    path: 'home',
    canActivate: [AdminGuard],
    component: AdminHomeComponent,
    data: {
      title: 'Administration',
    }
  },
  {
    path: 'contact',
    canActivate: [AdminGuard],
    component: ContactComponent,
    data: {
      title: 'Modifier les contacts importants',
    }
  },
  {
    path: 'document',
    canActivate: [AdminGuard],
    component: DocumentComponent,
    data: {
      title: 'Modifier les documents importants',
    }
  },
  {
    path: 'logs',
    canActivate: [AdminGuard],
    component: LogsComponent,
    data: {
      title: 'Logs',
    }
  },
  {
    path: 'mailing',
    canActivate: [AdminGuard],
    component: MailingComponent,
    data: {
      title: 'Mailing',
    }
  },
  {
    path: 'distribution',
    data: {
      title: 'Répartition',
    },
    children: [
      {
        path: 'years',
        canActivate: [AdminGuard],
        component: YearListComponent,
        data: {
          title: 'Modifier les années disponibles',
        }
      },
      {
        path: 'promotions',
        canActivate: [AdminGuard],
        component: PromotionListComponent,
        data: {
          title: 'Modifier les promotions disponibles',
        }
      },
      {
        path: 'categories',
        canActivate: [AdminGuard],
        component: CategoryListComponent,
        data: {
          title: 'Modifier les catégories de stages disponibles',
        }
      },
      {
        path: 'locations',
        canActivate: [AdminGuard],
        component: LocationListComponent,
        data: {
          title: 'Modifier les localisations disponibles',
        }
      },
      {
        path: 'terraintypes',
        canActivate: [AdminGuard],
        component: TerrainTypeListComponent,
        data: {
          title: 'Modifier les types de terrains',
        }
      },
      {
        path: 'periods',
        canActivate: [AdminGuard],
        component: PeriodListComponent,
        data: {
          title: 'Modifier les périodes de stages',
        }
      },
      {
        path: 'terrains',
        canActivate: [AdminGuard],
        component: TerrainListComponent,
        data: {
          title: 'Modifier les terrains de stages',
        }
      },
      {
        path: 'phases',
        canActivate: [AdminGuard],
        data: {
          title: 'Phases',
        },
        loadChildren: './distribution/configuration/phase/phase.module#PhaseModule'
      },
      {
        path: 'manage',
        canActivate: [AdminGuard],
        component: ManageHomeComponent,
        data: {
          title: 'Gestion de la répartition',
        },
      },
      {
        path: 'manage/promotion/:promotionId/phase/:phaseId',
        canActivate: [AdminGuard],
        component: ManagePromotionComponent,
        data: {
          title: 'Gestion de la répartition d\'une promotion',
        },
      },
      {
        path: 'pdf-export',
        canActivate: [AdminGuard],
        component: PdfExportComponent,
        data: {
          title: 'Exporter la répartition'
        }
      },
      {
        path: 'csv-export',
        canActivate: [AdminGuard],
        component: CsvExportComponent,
        data: {
          title: 'Exporter la répartition'
        }
      },
      {
        path: 'attributions-per-user',
        canActivate: [AdminGuard],
        component: AttributionsPerUserComponent,
        data: {
          title: 'Stages par utilisateurs'
        }
      },

    ]
  },
  {
    path: 'page_content',
    canActivate: [AdminGuard],
    component: PageContentComponent,
    data: {
      title: 'Modifier le contenu des pages du site',
    }
  },
  {
    path: 'gardes',
    data: {
      title: 'Gardes'
    },
    children: [
      {
        path: 'terrain-list',
        canActivate: [AdminGuard],
        component: GardeTerrainListComponent,
        data: {
          title: 'Terrains de garde',
          breadcrumbs: 'Terrains'
        }
      },
      {
        path: 'garde-per-user',
        canActivate: [AdminGuard],
        component: GardePerUserComponent,
        data: {
          title: 'Gardes par utilisateur',
          breadcrumbs: 'Gardes par utilisateur'
        }
      },
      {
        path: 'config',
        canActivate: [AdminGuard],
        component: GardeYearConfigComponent,
        data: {
          title: 'Configuration d\'une année',
          breadcrumbs: 'Configuration'
        }
      },
      {
        path: 'export',
        canActivate: [AdminGuard],
        component: GardeExportComponent,
        data: {
          title: 'Exportation du planning de garde',
          breadcrumbs: 'Exportation'
        }
      }
    ]
  },
  {
    path: 'users',
    data: {
      title: 'Utilisateurs',
    },
    children: [
      {
        path: 'list',
        canActivate: [AdminGuard],
        component: UserListComponent,
        data: {
          title: 'Gestion des utilisateurs'
        }
      },
      {
        path: 'profile/:userId',
        canActivate: [AdminGuard],
        component: UserProfileComponent,
        data: {
          title: 'Profil'
        }
      },
      {
        path: 'promotion-switcher',
        canActivate: [AdminGuard],
        component: PromotionSwitcherComponent,
        data: {
          title: 'Changements de promotions'
        }
      },
      {
        path: '*',
        redirectTo: 'list',
      },
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
    ]
  },
  {
    path: '*',
    redirectTo: 'home',
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
