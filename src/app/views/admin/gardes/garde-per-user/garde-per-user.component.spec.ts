import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GardePerUserComponent} from './garde-per-user.component';

describe('GardePerUserComponent', () => {
  let component: GardePerUserComponent;
  let fixture: ComponentFixture<GardePerUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GardePerUserComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GardePerUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
