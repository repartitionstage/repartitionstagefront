import {Component, OnInit, ViewChild} from '@angular/core';
import {GardeService} from '../../../../service/garde/garde.service';
import {User} from '../../../../service/user/user';
import {UserService} from '../../../../service/user/user.service';
import {Result} from '../../../../service/rest-api/result';
import {InfoCardComponent} from '../../../utils/info-card/info-card.component';
import {Garde} from '../../../../service/garde/garde';

@Component({
  selector: 'rs-garde-per-user',
  templateUrl: './garde-per-user.component.html',
  styleUrls: ['./garde-per-user.component.scss']
})
export class GardePerUserComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  allUsers: User[];

  selectedUserId: number;

  gardes: Garde[];
  freeGardes: Garde[];
  selectedGardeId: number;

  constructor(private gardeService: GardeService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.userService.getAllUsers()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.allUsers = result.data.list;
            this.allUsers.sort(
              (a: User, b: User) => {
                if (a.lastName === b.lastName) {
                  return a.firstName.localeCompare(b.firstName);
                } else {
                  return a.lastName.localeCompare(b.lastName);
                }
              }
            );
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  showGardes() {
    if (this.selectedUserId) {
      this.gardeService.getGardesByUserId(this.selectedUserId)
        .subscribe(
          (result: Result) => {
            if (result.success) {
              this.gardes = result.data;
              this.getFreeGardes();
            } else {
              this.card.error(result.errors, 4000);
            }
          }
        );
    }
  }

  removeUserFromGarde(gardeId: number) {
    this.gardeService.removeUserFromGarde(gardeId)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Garde supprimée avec succès!'], 3000);
            this.showGardes();
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  addUserToGarde() {
    if (this.selectedGardeId !== 0) {
      this.gardeService.updateGarde({id: this.selectedGardeId, user_id: this.selectedUserId})
        .subscribe(
          (result: Result) => {
            if (result.success) {
              this.card.success(['Garde ajoutée avec succès!'], 3000);
              this.showGardes();
            } else {
              this.card.error(result.errors, 4000);
            }
          }
        );
    }
  }

  getFreeGardes() {
    this.selectedGardeId = 0;
    this.gardeService.getFreeGardes()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.freeGardes = result.data;
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }
}
