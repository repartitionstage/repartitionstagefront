import {Component, OnInit, ViewChild} from '@angular/core';
import {DistributionService} from '../../../../service/distribution/distribution.service';
import {Result} from '../../../../service/rest-api/result';
import {Year} from '../../../../service/distribution/models/year';
import {InfoCardComponent} from '../../../utils/info-card/info-card.component';
import {GardeService} from '../../../../service/garde/garde.service';

@Component({
  selector: 'rs-garde-config',
  templateUrl: './garde-year-config.component.html',
  styleUrls: ['./garde-year-config.component.scss']
})
export class GardeYearConfigComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;
  years: Year[];

  yearId: number;

  config: {
    beginDate: string,
    endDate: string,
    exceptions: {
      promotion: number,
      dates: string[]
    }[],
    holydays: string[],
  };

  constructor(private distributionService: DistributionService,
              private gardeService: GardeService) {
  }

  ngOnInit() {
    this.distributionService.getYears()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.years = result.data;
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );

  }

  getExceptionsByPromotionId(promotionId: number) {
    for (const exception of this.config.exceptions) {
      if (exception.promotion === promotionId) {
        return exception;
      }
    }
  }

  refresh() {
    if (this.yearId) {
      this.gardeService.getConfig(this.yearId)
        .subscribe(
          (result: Result) => {
            if (result.success) {
              this.config = result.data.config;
              if (!this.config.holydays){
                this.config.holydays = [];
              }
              if(!this.config.exceptions) {
                this.config.exceptions = [];
                this.config.exceptions.push({
                  promotion: 2,
                  dates: []
                });
                this.config.exceptions.push({
                  promotion: 3,
                  dates: []
                });
                this.config.exceptions.push({
                  promotion: 4,
                  dates: []
                });

              }
            } else {
              this.card.error(result.errors, 4000);
            }
          }
        );
    }
  }

  updateConfig() {
    this.gardeService.updateConfig(this.yearId, this.config)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Configuration sauvegardée avec succès'], 3000);
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  addHolyday(date: string) {
    if (date && date !== '' && this.config.holydays.indexOf(date) === -1) {
      this.config.holydays.push(date);
      this.config.holydays.sort((a, b) => a.localeCompare(b));
    }
  }

  removeHolyday(date: string) {
    if (date && date !== '' && this.config.holydays.indexOf(date) !== -1) {
      this.config.holydays.splice(this.config.holydays.indexOf(date), 1);
    }
  }

  addException(promotionId: number, date: string) {
    let array = this.getExceptionsByPromotionId(promotionId).dates;
    if (date && date !== '' && array && array.indexOf(date) === -1) {
      array.push(date);
      array.sort((a, b) => a.localeCompare(b));
    }
  }

  removeException(promotionId: number, date: string) {
    const array = this.getExceptionsByPromotionId(promotionId).dates;
    if (date && date !== '' && array && array.indexOf(date) !== -1) {
      array.splice(array.indexOf(date), 1);
    }
  }
}
