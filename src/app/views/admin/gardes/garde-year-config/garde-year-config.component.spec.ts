import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GardeYearConfigComponent} from './garde-year-config.component';

describe('GardeConfigComponent', () => {
  let component: GardeYearConfigComponent;
  let fixture: ComponentFixture<GardeYearConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GardeYearConfigComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GardeYearConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
