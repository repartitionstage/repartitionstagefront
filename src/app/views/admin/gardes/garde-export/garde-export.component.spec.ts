import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GardeExportComponent} from './garde-export.component';

describe('GardeExportComponent', () => {
  let component: GardeExportComponent;
  let fixture: ComponentFixture<GardeExportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GardeExportComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GardeExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
