import {Component, OnInit, ViewChild} from '@angular/core';
import {Result} from '../../../../service/rest-api/result';
import {InfoCardComponent} from '../../../utils/info-card/info-card.component';
import {Year} from '../../../../service/distribution/models/year';
import {DistributionService} from '../../../../service/distribution/distribution.service';
import {GardeService} from '../../../../service/garde/garde.service';

@Component({
  selector: 'rs-garde-export',
  templateUrl: './garde-export.component.html',
  styleUrls: ['./garde-export.component.scss']
})
export class GardeExportComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;
  years: Year[];

  yearId: number;

  constructor(private distributionService: DistributionService,
              private gardeService: GardeService) {
  }

  ngOnInit() {
    this.distributionService.getYears()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.years = result.data;
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  getYearById(yearId: number) {
    for (const year of this.years) {
      if (year.id === yearId) {
        return year;
      }
    }
  }

  genPlanning() {
    this.card.info(['Génération du planning de garde en cours...']);

    this.gardeService.genPlanning(this.yearId)
      .subscribe(
        (result: Result) => {
          this.card.hide();
          if (result.success) {
            this.card.success(['Génération du planning réussie', 'Téléchargement disponible'], 3000);
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  downloadPlanning() {
    this.gardeService.downloadPlanning(this.yearId, 'Planning de gardes.xlsx')
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Téléchargement réussie'], 3000);
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

}
