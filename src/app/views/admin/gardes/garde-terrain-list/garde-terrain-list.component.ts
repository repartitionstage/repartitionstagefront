import {Component, OnInit} from '@angular/core';
import {GardeService} from '../../../../service/garde/garde.service';

@Component({
  selector: 'rs-garde-terrain-list',
  templateUrl: './garde-terrain-list.component.html',
  styleUrls: ['./garde-terrain-list.component.scss']
})
export class GardeTerrainListComponent implements OnInit {

  columns = [
    {
      id: 'id',
      name: 'id',
      editable: false,
      visible: true,
      width: 10
    },
    {
      id: 'name',
      name: 'Nom',
      editable: true,
      visible: true,
      width: 20
    },
    {
      id: 'description',
      name: 'Description',
      editable: true,
      visible: true,
      width: 60
    },
    {
      id: 'night',
      name: 'Nuit complète',
      editType: 'checkbox',
      editable: true,
      visible: true,
      width: 10
    }
  ];
  min = 0;
  pas = 0;
  get = ((service) => (limit: number, offset: number) => service.getTerrains(limit, offset))(this.gardeService);
  add = ((service) => (terrain: any) => {
    if (terrain.night === undefined) {
      terrain.night = false;
    }
    return service.addTerrain(terrain);
  })(this.gardeService);
  edit = ((service) => (terrain: any) => service.updateTerrain(terrain))(this.gardeService);
  del = ((service) => (terrainId: number) => service.deleteTerrain(terrainId))(this.gardeService);

  constructor(private gardeService: GardeService) {
  }


  ngOnInit() {
  }

}
