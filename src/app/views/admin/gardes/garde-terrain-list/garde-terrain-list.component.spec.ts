import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GardeTerrainListComponent} from './garde-terrain-list.component';

describe('GardeTerrainListComponent', () => {
  let component: GardeTerrainListComponent;
  let fixture: ComponentFixture<GardeTerrainListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GardeTerrainListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GardeTerrainListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
