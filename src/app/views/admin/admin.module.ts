import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminRoutingModule} from './admin-routing.module';
import {AdminHomeComponent} from './admin-home/admin-home.component';
import {NgxEditorModule} from 'ngx-editor';
import {FormsModule} from '@angular/forms';

import {RsUtilsModule} from '../utils/rs-utils.module';
import {YearListComponent} from './distribution/configuration/year-list/year-list.component';
import {PromotionListComponent} from './distribution/configuration/promotion-list/promotion-list.component';
import {LocationListComponent} from './distribution/configuration/location-list/location-list.component';
import {CategoryListComponent} from './distribution/configuration/category-list/category-list.component';
import {PeriodListComponent} from './distribution/configuration/period-list/period-list.component';
import {DragulaModule} from 'ng2-dragula';
import {TerrainTypeListComponent} from './distribution/configuration/terrain-type-list/terrain-type-list.component';
import {TerrainListComponent} from './distribution/configuration/terrain-list/terrain-list.component';
import {PageContentComponent} from './page-content/page-content.component';
import {ManageHomeComponent} from './distribution/manage/manage-home/manage-home.component';
import {ManagePromotionComponent} from './distribution/manage/manage-promotion/manage-promotion.component';
import {ManagePhaseGroupComponent} from './distribution/manage/manage-phase-group/manage-phase-group.component';
import {ManagePhaseTerrainComponent} from './distribution/manage/manage-phase-terrain/manage-phase-terrain.component';
import {ManagePhaseTextualComponent} from './distribution/manage/manage-phase-textual/manage-phase-textual.component';
import {AccordionModule, TabsModule} from 'angular-admin-lte';
import {NgSelectModule} from '@ng-select/ng-select';
import {RsPipeModule} from '../../pipe/rs-pipe.module';
import {RowCountPipe} from './distribution/manage/manage-phase-terrain/pipe/row-count/row-count.pipe';
import {ColCountPipe} from './distribution/manage/manage-phase-terrain/pipe/col-count/col-count.pipe';
import {CollapseModule} from 'ngx-bootstrap';
import {UserListComponent} from './user/user-list/user-list.component';
import {UserProfileComponent} from './user/user-profile/user-profile.component';
import {ContactComponent} from './contact/contact.component';
import {DocumentComponent} from './document/document.component';
import {AttributionsPerUserComponent} from './distribution/attributions-per-user/attributions-per-user.component';
import {PromotionSwitcherComponent} from './user/promotion-switcher/promotion-switcher.component';
import {LogsComponent} from './logs/logs.component';
import {GardeTerrainListComponent} from './gardes/garde-terrain-list/garde-terrain-list.component';
import {GardeYearConfigComponent} from './gardes/garde-year-config/garde-year-config.component';
import {GardeExportComponent} from './gardes/garde-export/garde-export.component';
import {GardePerUserComponent} from './gardes/garde-per-user/garde-per-user.component';
import {ValuePipe} from './distribution/manage/manage-phase-terrain/pipe/value.pipe';
import {PdfExportComponent} from './distribution/exportation/pdf-export/pdf-export.component';
import {CsvExportComponent} from './distribution/exportation/csv-export/csv-export.component';
import {MailingComponent} from './mailing/mailing.component';

@NgModule({
  declarations: [
    AdminHomeComponent,

    YearListComponent,
    PromotionListComponent,
    LocationListComponent,
    CategoryListComponent,
    PeriodListComponent,
    TerrainTypeListComponent,
    TerrainListComponent,

    ManageHomeComponent,
    ManagePromotionComponent,

    PageContentComponent,

    ManagePhaseGroupComponent,
    ManagePhaseTerrainComponent,
    ManagePhaseTextualComponent,
    RowCountPipe,
    ColCountPipe,

    UserListComponent,
    UserProfileComponent,
    ContactComponent,
    DocumentComponent,
    AttributionsPerUserComponent,
    PromotionSwitcherComponent,
    LogsComponent,
    GardeTerrainListComponent,
    GardeYearConfigComponent,
    GardeExportComponent,
    GardePerUserComponent,
    ValuePipe,

    CsvExportComponent,
    PdfExportComponent,
    MailingComponent,

  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    NgxEditorModule,
    DragulaModule,
    RsUtilsModule,
    AccordionModule,
    NgSelectModule,
    RsPipeModule,
    CollapseModule,
    TabsModule
  ],
  providers: []
})
export class AdminModule {
}

