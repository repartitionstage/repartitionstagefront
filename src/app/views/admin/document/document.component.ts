import {Component, OnInit, ViewChild} from '@angular/core';
import {Document} from '../../../service/document/document';
import {DocumentService} from '../../../service/document/document.service';
import {Result} from '../../../service/rest-api/result';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';
import {DistributionService} from '../../../service/distribution/distribution.service';
import {Promotion} from '../../../service/distribution/models/promotion';

@Component({
  selector: 'rs-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  documents: Document[];

  promotions: Promotion[];
  showDelBtn: boolean;

  data: {
    name: string,
    description: string,
    promotionId: number
    file: any,
  };


  constructor(private distribution: DistributionService,
              private documentService: DocumentService) {
  }

  ngOnInit() {
    this.setDefaultState();

    this.distribution.getPromotions()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.promotions = result.data;
            this.promotions.unshift(new Promotion(0, 'Toutes', false));
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );

    this.documentService.getDocumentList()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.documents = result.data;
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  setDefaultState() {
    this.showDelBtn = true;
    this.data = {
      name: '',
      description: '',
      promotionId: 0,
      file: undefined
    };
  }

  deleteDocument(id: number) {
    this.documentService.deleteDocument(id)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.ngOnInit();
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  downloadDocument(id: number, name: string) {
    this.documentService.downloadDocument(id, name)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Téléchargement lancé !'], 2000);
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  uploadDocument() {
    const formData: FormData = new FormData();
    formData.append('name', this.data.name);
    formData.append('description', this.data.description);
    formData.append('promotionId', this.data.promotionId.toString());
    formData.append('document', this.data.file, this.data.file.name);

    this.documentService.uploadNewDocument(formData)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.ngOnInit();
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  getPromotionName(id: number): string {
    for (const promotion of this.promotions) {
      if (promotion.id === id) {
        return promotion.name;
      }
    }
  }

  handleFileInput(files: FileList) {
    this.data.file = files.item(0);
  }
}
