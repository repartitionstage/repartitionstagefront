import {Component, OnInit, ViewChild} from '@angular/core';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';
import {DistributionService} from '../../../service/distribution/distribution.service';
import {Result} from '../../../service/rest-api/result';
import {Promotion} from '../../../service/distribution/models/promotion';
import {MailingService} from '../../../service/mailing/mailing.service';

@Component({
  selector: 'rs-mailing',
  templateUrl: './mailing.component.html',
  styleUrls: ['./mailing.component.scss']
})
export class MailingComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;
  promotions: Promotion[];

  promotionId: number;

  constructor(private distributionService: DistributionService,
              private mailingService: MailingService) {
  }

  ngOnInit() {
    this.distributionService.getPromotions()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.promotions = result.data;
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  downloadMailing() {
    this.mailingService.getMailingCSV(this.promotionId, 'Mailing-' + this.promotionId + '.csv')
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Téléchargement réussie'], 3000);
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }
}
