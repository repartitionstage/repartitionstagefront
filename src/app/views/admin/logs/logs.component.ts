import {Component, OnInit, ViewChild} from '@angular/core';
import {LogService} from '../../../service/log/log.service';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';
import {Result} from '../../../service/rest-api/result';
import {Log} from '../../../service/log/log';
import {User} from '../../../service/user/user';
import {UserService} from '../../../service/user/user.service';

@Component({
  selector: 'rs-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  users: User[];

  executorId: number;
  targetId: number;
  type: string;

  logs: Log[];

  logTypes = LogService.TYPES;

  constructor(private logService: LogService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.userService.getAllUsers()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.users = result.data.list;
            this.users.sort((a, b) => ((a.lastName === b.lastName) ? a.firstName.localeCompare(b.firstName) : a.lastName.localeCompare(b.lastName)));
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  getLogs(): void {
    this.logService.getLogs(this.executorId, this.targetId, this.type)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.logs = result.data;
            this.logs.forEach(
              (log) => {
                log['translation'] = this.logService.getTranslation(log.type, log.executorId, log.targetId, log.data, this.users);
              }
            );
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  getUserNameById(executorId: number) {
    for (const user of this.users) {
      if (user.id === executorId) {
        return user.firstName + ' ' + user.lastName.toUpperCase();
      }
    }
  }
}
