import {Component, OnInit, ViewChild} from '@angular/core';
import {PageContentService} from '../../../service/page-content/page-content.service';
import {Result} from '../../../service/rest-api/result';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';

@Component({
  selector: 'rs-page-content',
  templateUrl: './page-content.component.html',
  styleUrls: ['./page-content.component.scss']
})
export class PageContentComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  pageNames: { name: string }[];
  selectedName: string;
  htmlContent: string;

  constructor(private pageContentService: PageContentService) {
  }

  ngOnInit() {
    this.pageContentService.getPageNames().subscribe(
      (result: Result) => {
        if (result.success) {
          this.pageNames = result.data;
        }
      }
    );
  }

  refresh() {
    this.htmlContent = undefined;
    this.pageContentService.getPageContent(this.selectedName).subscribe(
      (result: Result) => {
        if (result.success) {
          this.htmlContent = result.data.htmlContent;
        } else {
          this.card.error(['Aucun contenu trouvé.'], 2000);
        }
      }
    );
  }

  updatePageContent() {
    this.pageContentService.updatePageContent(this.selectedName, this.htmlContent).subscribe(
      (result: Result) => {
        if (result.success) {
          this.card.success(['Contenu modifié avec succès'], 2000);
        } else {
          this.card.error(result.errors, 2000);
        }
      }
    );
  }
}
