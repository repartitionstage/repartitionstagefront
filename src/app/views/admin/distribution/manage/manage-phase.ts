import {Input} from '@angular/core';
import {Phase} from '../../../../service/distribution/models/phase';

export class ManagePhase {

  @Input() promotion: { id: number, name: string };
  @Input() phase: Phase;
  ready: boolean;

  refresh() {
  }
}
