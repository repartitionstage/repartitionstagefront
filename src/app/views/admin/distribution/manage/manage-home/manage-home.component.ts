import {Component, OnInit, ViewChild} from '@angular/core';
import {DistributionService} from '../../../../../service/distribution/distribution.service';
import {Result} from '../../../../../service/rest-api/result';
import {InfoCardComponent} from '../../../../utils/info-card/info-card.component';
import {Router} from '@angular/router';
import {HistoryService} from '../../../../../service/history/history.service';

@Component({
  selector: 'rs-phase-admin-base',
  templateUrl: './manage-home.component.html',
  styleUrls: ['./manage-home.component.scss']
})
export class ManageHomeComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  // YEARS - BEGIN
  activeYear: { id: number, name: string };

  years: { id: number, name: string }[];
  selectedYearId: string | number;

  showUpdateActiveYearConfirmation = false;
  disableActiveYearSelection = false;

  confirmUpdateActiveYear = {
    title: 'Confirmer le changement d\'année active ?'
  };

  // YEARS - END

  // PROMOTIONS
  promotions: { id: number, name: string }[];
  selectedPromotionId;
  // PROMOTIONS - END

  constructor(private distributionService: DistributionService,
              private router: Router,
              private history: HistoryService) {
  }

  ngOnInit() {
    this.refreshActiveYear();

    this.distributionService.getYears().subscribe(
      (result: Result) => {
        if (result.success) {
          this.years = result.data;
        } else {
          this.card.error(result.errors, 2000);
        }
      }
    );

    this.distributionService.getPromotions(false).subscribe(
      (result: Result) => {
        if (result.success) {
          this.promotions = result.data;

          if (this.history.map.has('distribution-manage-home-selected-promotion')) {
            this.selectedPromotionId = this.history.map.get('distribution-manage-home-selected-promotion');
          }

        } else {
          this.card.error(result.errors, 2000);
        }
      }
    );
  }

  askForConfirmationYearUpdate() {
    this.disableActiveYearSelection = true;
    this.showUpdateActiveYearConfirmation = true;
  }

  updateActiveYear($event: any) {
    if ($event === true) {
      this.selectedYearId = Number(this.selectedYearId);
      this.distributionService.updateActiveYear(this.selectedYearId)
        .subscribe(
          (result: Result) => {
            if (result.success) {
              this.card.success(['Année active mise à jour avec succès'], 2000);
              this.refreshActiveYear();
            } else {
              this.card.error(result.errors, 2000);
            }
            this.disableActiveYearSelection = false;
            this.showUpdateActiveYearConfirmation = false;
          }
        );
    } else {
      this.disableActiveYearSelection = false;
      this.showUpdateActiveYearConfirmation = false;
    }
  }

  refreshActiveYear() {
    this.distributionService.getActiveYear()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.activeYear = result.data;
          } else {
            this.card.error(result.errors, 2000);
          }
        }
      );
  }

  navigateToSelectedPromotion() {
    this.history.map.set('distribution-manage-home-selected-promotion', this.selectedPromotionId);

    this.router.navigate(['/admin/distribution/manage/promotion/' + this.selectedPromotionId + '/phase/default']);
    /*
    this.distributionService.getPhases('active', this.selectedPromotionId)
      .subscribe(
        (result: Result) => {
          const data = result.data;
          data.sort((a, b) => (a.order - b.order));

          for (let i = 0; i < data.length; i++) {
            if (data[i].active) {
              this.router.navigate(['/admin/distribution/manage/promotion/' + this.selectedPromotionId + '/phase/' + i]);
              break;
            }
          }
        }
      );*/
  }
}
