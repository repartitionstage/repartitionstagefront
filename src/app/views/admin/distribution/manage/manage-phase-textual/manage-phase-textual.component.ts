import {Component, OnInit} from '@angular/core';
import {ManagePhase} from '../manage-phase';

@Component({
  selector: 'rs-manage-phase-textual',
  templateUrl: './manage-phase-textual.component.html',
  styleUrls: ['./manage-phase-textual.component.scss']
})
export class ManagePhaseTextualComponent extends ManagePhase implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
