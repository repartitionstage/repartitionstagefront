import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ManagePhaseTextualComponent} from './manage-phase-textual.component';

describe('ManagePhaseTextualComponent', () => {
  let component: ManagePhaseTextualComponent;
  let fixture: ComponentFixture<ManagePhaseTextualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManagePhaseTextualComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePhaseTextualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
