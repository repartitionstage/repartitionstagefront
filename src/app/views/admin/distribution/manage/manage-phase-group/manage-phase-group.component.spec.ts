import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ManagePhaseGroupComponent} from './manage-phase-group.component';

describe('ManagePhaseGroupComponent', () => {
  let component: ManagePhaseGroupComponent;
  let fixture: ComponentFixture<ManagePhaseGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManagePhaseGroupComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePhaseGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
