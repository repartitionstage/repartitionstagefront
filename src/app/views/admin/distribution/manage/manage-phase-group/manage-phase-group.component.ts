import {Component, OnInit, ViewChild} from '@angular/core';
import {ManagePhase} from '../manage-phase';
import {UserService} from '../../../../../service/user/user.service';
import {Result} from '../../../../../service/rest-api/result';
import {InfoCardComponent} from '../../../../utils/info-card/info-card.component';

@Component({
  selector: 'rs-manage-phase-group',
  templateUrl: './manage-phase-group.component.html',
  styleUrls: ['./manage-phase-group.component.scss']
})
export class ManagePhaseGroupComponent extends ManagePhase implements OnInit {

  Math = Math;

  @ViewChild('card') card: InfoCardComponent;

  groupA: { id: number, firstName: string, lastName: string, group: number, groupBlocked: boolean }[];
  groupB: { id: number, firstName: string, lastName: string, group: number, groupBlocked: boolean }[];
  noGroup: { id: number, firstName: string, lastName: string, group: number, groupBlocked: boolean }[];

  users: { id: number, firstName: string, lastName: string, group: number, groupBlocked: boolean }[];

  constructor(private userService: UserService) {
    super();
  }

  ngOnInit() {
    this.refresh();
  }

  refresh() {
    this.userService.getUsersByPromotion(this.promotion.id).subscribe(
      (result: Result) => {
        if (result.success) {
          this.users = result.data;
          this.filterUsers();
        }
      }
    );
  }

  filterUsers() {
    const sort = (user1: { id: number, firstName: string, lastName: string, group: number }, user2: { id: number, firstName: string, lastName: string, group: number }): number => {
      if (user1.lastName !== user2.lastName) {
        return user1.lastName.localeCompare(user2.lastName);
      } else {
        return user1.firstName.localeCompare(user2.firstName);
      }
    };

    this.users.sort(sort);
    this.noGroup = this.users.filter(user => (user.group === 1));

    this.groupA = this.users.filter(user => (user.group === 2));
    let index = 1;
    this.groupA.forEach(
      (user) => {
        user['showedId'] = index++;
      }
    );

    this.groupB = this.users.filter(user => (user.group === 3));
    index = 1;
    this.groupB.forEach(
      (user) => {
        user['showedId'] = index++;
      }
    );


  }

  switchToRight(id: number) {
    const user = this.getUser(id);
    user.group = (user.group === 1 ? 3 : 1);
    this.updateUser(user);
    this.filterUsers();
  }

  switchToLeft(id: number) {
    const user = this.getUser(id);
    user.group = (user.group === 1 ? 2 : 1);
    this.updateUser(user);
    this.filterUsers();
  }

  getUser(id: number) {
    for (const user of this.users) {
      if (user.id === id) {
        return user;
      }
    }
  }

  updateUser(user: { id: number, firstName: string, lastName: string, group: number }) {
    delete user['showedId'];
    this.userService.updateUser(user)
      .subscribe(
        (result: Result) => {
          if (!result.success) {
            this.card.error(result.errors, 2000);
          }
        }
      );
  }

}
