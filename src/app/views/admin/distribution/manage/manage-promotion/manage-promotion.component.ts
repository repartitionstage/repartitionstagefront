import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DistributionService} from '../../../../../service/distribution/distribution.service';
import {Result} from '../../../../../service/rest-api/result';
import {Phase} from '../../../../../service/distribution/models/phase';
import {InfoCardComponent} from '../../../../utils/info-card/info-card.component';
import {ManagePhase} from '../manage-phase';

@Component({
  selector: 'rs-phase-admin-group',
  templateUrl: './manage-promotion.component.html',
  styleUrls: ['./manage-promotion.component.scss']
})
export class ManagePromotionComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  @ViewChild('phaseManager') phaseManager: ManagePhase;

  promotionId: number;
  promotionInfo: { id: number, name: string };

  phases: Phase[];
  selectedPhaseId = 0;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private distribution: DistributionService) {
    this.route.url.subscribe(() => {
      this.promotionId = Number(this.route.snapshot.params['promotionId']);
      this.selectedPhaseId = Number(this.route.snapshot.params['phaseId']);

      if (isNaN(this.promotionId)) {
        this.router.navigate(['/admin']);
        return;
      }

      const refreshTask = () => {
        if (this.phaseManager && this.phaseManager.ready) {
          this.phaseManager.refresh();
        } else {
          setTimeout(refreshTask, 50);
        }
      };

      if (this.promotionInfo && this.phases) {
        refreshTask();
      }

      if (!this.promotionInfo) {
        this.distribution.getPromotion(this.promotionId)
          .subscribe(
            (result: Result) => {
              if (result.success) {
                this.promotionInfo = result.data;
              } else {
                this.card.error(result.errors, 2000,
                  () => {
                    this.router.navigate(['/admin']);
                  });
              }
            }
          );
      }

      if (!this.phases) {
        this.updatePhases();
      }

    });
  }

  ngOnInit() {

  }

  updatePhases() {
    this.distribution.getPhases('active', this.promotionId)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.phases = result.data;
            this.phases.sort((a, b) => (a.order - b.order));
            this.updateSelectedPhaseComponent();
          } else {
            this.card.error(result.errors, 2000,
              () => {
                this.router.navigate(['/admin']);
              });
          }
        }
      );
  }

  updateSelectedPhaseComponent() {
    if (this.selectedPhaseId !== 0 && isNaN(this.selectedPhaseId)) {
      this.selectedPhaseId = 0; // default value
      for (let i = 0; i < this.phases.length; i++) {
        if (this.phases[i].active) {
          this.selectedPhaseId = i;
          break;
        }
      }
    }

    if (this.phaseManager && this.phaseManager.ready) {
      this.phaseManager.phase = this.phases[this.selectedPhaseId];
      this.phaseManager.promotion = this.promotionInfo;
    }

    this.phases.forEach(value => (value['selected'] = false));
    this.phases[this.selectedPhaseId]['selected'] = true;

    this.router.navigate(['/admin/distribution/manage/promotion/' + this.promotionId + '/phase/' + this.selectedPhaseId]);
  }

  selectPreviousPhase() {
    if (this.selectedPhaseId !== 0) {
      this.selectedPhaseId--;
      this.updateSelectedPhaseComponent();
    }
  }

  selectNextPhase() {
    if (this.selectedPhaseId !== (this.phases.length - 1)) {
      this.selectedPhaseId++;
      this.updateSelectedPhaseComponent();
    }
  }

  activatePhase() {
    this.distribution.activePhase(this.phases[this.selectedPhaseId].id)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Phase activée avec succès.'], 2000);
            this.updatePhases();
          } else {
            this.card.error(result.errors, 2000);
          }
        }
      );
  }

  swapPhaseFinishState() {
    this.distribution.updatePhase({id: this.phases[this.selectedPhaseId].id, finish: !this.phases[this.selectedPhaseId].finish})
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Phase arrêtée avec succès.'], 2000);
            this.updatePhases();
          } else {
            this.card.error(result.errors, 2000);
          }
        }
      );
  }
}
