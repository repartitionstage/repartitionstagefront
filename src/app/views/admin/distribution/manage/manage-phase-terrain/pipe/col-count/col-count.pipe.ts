import {Pipe, PipeTransform} from '@angular/core';
import {TerrainAttributionState} from '../../../../../../../service/distribution/models/terrain-attribution-state.enum';
import {Group} from '../../../../../../../service/distribution/models/group.enum';

@Pipe({
  name: 'colCount'
})
export class ColCountPipe implements PipeTransform {

  transform(value: any, config: { periodId?: number, max?: boolean, state?: TerrainAttributionState, group?: Group }): number {
    let amount = 0;

    const countCell = (checkedCell) => {
      let subAmount = 0;
      if (config.max === true) {
        subAmount += checkedCell.terrain.maxAmountPlaces;
      } else {
        if (config.state !== undefined) {
          for (const attrib of checkedCell.attributions) {
            if (attrib.state === config.state) {
              subAmount++;
            }
          }
        } else {
          subAmount += checkedCell.attributions.length;
        }
      }
      return subAmount;
    };

    value.rows.forEach(
      (row) => {
        if (config.periodId) {
          const cell = row.getTerrainCellByPeriodId(config.periodId);
          amount += countCell(cell);
        } else {
          row.terrainCells.forEach(
            (cell) => {
              amount += countCell(cell);
            }
          );
        }
      }
    );

    return amount;
  }

}
