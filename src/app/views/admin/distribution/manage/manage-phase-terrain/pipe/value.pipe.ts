import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'value'
})
export class ValuePipe implements PipeTransform {

  transform<K, V>(values: Map<K, V>, sorter: (a: V, b: V) => number): V[] {
    const array = [];
    values.forEach(
      (value) => {
        array.push(value);
      }
    );
    array.sort(sorter);
    return array;
  }

}
