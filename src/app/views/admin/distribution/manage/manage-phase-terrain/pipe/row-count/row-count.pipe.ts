import {Pipe, PipeTransform} from '@angular/core';
import {Terrain} from '../../../../../../../service/distribution/models/terrain';
import {TerrainAttribution} from '../../../../../../../service/distribution/models/terrain-attribution';

@Pipe({
  name: 'rowCount'
})
export class RowCountPipe implements PipeTransform {

  transform(value: Map<number, { terrain: Terrain, attribution: TerrainAttribution[], borderColor: string }>, args?: any): string {
    let amount = 0;
    let maxAmount = 0;

    value.forEach(
      (cell) => {
        amount += cell.attribution.length;
        maxAmount += cell.terrain.maxAmountPlaces;
      }
    );
    return amount + '/' + maxAmount;
  }

}
