import {Component, OnInit, ViewChild} from '@angular/core';
import {ManagePhase} from '../manage-phase';
import {DistributionService} from '../../../../../service/distribution/distribution.service';
import {Result} from '../../../../../service/rest-api/result';
import {TerrainAttribution} from '../../../../../service/distribution/models/terrain-attribution';
import {Terrain} from '../../../../../service/distribution/models/terrain';
import {TerrainType} from '../../../../../service/distribution/models/terrain-type';
import {Period} from '../../../../../service/distribution/models/period';
import {TerrainAttributionState} from '../../../../../service/distribution/models/terrain-attribution-state.enum';
import {Group} from '../../../../../service/distribution/models/group.enum';
import {UserService} from '../../../../../service/user/user.service';
import {InfoCardComponent} from '../../../../utils/info-card/info-card.component';
import {Location} from '../../../../../service/distribution/models/location';

@Component({
  selector: 'rs-manage-phase-terrain',
  templateUrl: './manage-phase-terrain.component.html',
  styleUrls: ['./manage-phase-terrain.component.scss']
})
export class ManagePhaseTerrainComponent extends ManagePhase implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  TerrainAttributionState = TerrainAttributionState;
  Group = Group;

  data: Map<number, Map<number, Cell>> = new Map<number, Map<number, Cell>>();

  table: Table = new Table();

  locations: Map<number, Location>;
  periods: Period[];
  terrainTypes: Map<number, TerrainType>;
  terrains: Map<number, Terrain>;
  attributions: TerrainAttribution[];

  dataCalculated = false;

  users: { id: number, firstName: string, lastName: string }[];

  selectedGroup = Group.A;

  sorter: (a: Row, b: Row) => number;

  constructor(private distributionService: DistributionService,
              private userService: UserService) {
    super();
  }

  static countStatedAttribution(attribs: TerrainAttribution[], state: TerrainAttributionState): number {
    let i = 0;
    for (const attrib of attribs) {
      if (attrib.state === state) {
        i++;
      }
    }
    return i;
  }

  static arrayToMap<A, B>(array: B[], ft: (B) => A): Map<A, B> {
    const map = new Map<A, B>();
    array.forEach(
      (value) => {
        map.set(ft(value), value);
      }
    );
    return map;
  }

  getPeriodById(id: number): Period {
    for (const period of this.periods) {
      if (period.id === id) {
        return period;
      }
    }
  }

  sorterByAlphabeticOrder = (a: Row, b: Row): number => a.headerCell.terrainType.name.localeCompare(b.headerCell.terrainType.name);
  sorterByDistanceInvertOrder = (a: Row, b: Row): number => b.headerCell.location.distance - a.headerCell.location.distance;

  swapSort() {
    if (!this.sorter || this.sorter === this.sorterByDistanceInvertOrder) {
      this.sorter = this.sorterByAlphabeticOrder;
    } else {
      this.sorter = this.sorterByDistanceInvertOrder;
    }
    this.refresh(false);
  }

  ngOnInit() {
    this.ready = true;
  }

  refresh(resetData = true) {
    this.dataCalculated = false;

    if (!this.sorter) {
      this.sorter = this.sorterByAlphabeticOrder;
    }

    if (resetData === true) {
      this.table = new Table();

      this.userService.getUsersByPromotion(this.promotion.id)
        .subscribe(
          (result: Result) => {
            if (result.success) {
              this.users = result.data;

              this.users.sort(
                (a, b) => {
                  if (a.lastName === b.lastName) {
                    return a.firstName.localeCompare(b.firstName);
                  }
                  return a.lastName.localeCompare(b.lastName);
                }
              );
            } else {
              this.card.error(result.errors, 4000);
            }
          }
        );
    }

    this.distributionService.getTerrainAttributionForAdmin(this.phase.id, this.selectedGroup, resetData)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            if (resetData === true) {
              this.periods = result.data.periods;
              this.periods.sort((a, b) => a.order - b.order);

              this.locations = ManagePhaseTerrainComponent.arrayToMap<number, Location>(result.data.locations, (location: Location) => location.id);
              this.terrainTypes = ManagePhaseTerrainComponent.arrayToMap<number, TerrainType>(result.data.terrainTypes, (tt: TerrainType) => tt.id);
              this.terrains = ManagePhaseTerrainComponent.arrayToMap<number, Terrain>(result.data.terrains, (t: Terrain) => t.id);
            }
            this.attributions = result.data.attributions;
            this.calculateData();
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );

  }

  getGroupPeriod(groups: Group[]): Period[] {
    const selectedPeriods = [];

    this.periods.forEach(
      (period) => {
        if (groups.indexOf(period.group) !== -1) {
          selectedPeriods.push(period);
        }
      }
    );

    return selectedPeriods;
  }

  calculateData() {
    const groupPeriodIds = this.getGroupPeriod([this.selectedGroup, Group.TWICE]).map(period => period.id);

    this.terrains.forEach(
      (terrain) => {
        if (groupPeriodIds.includes(terrain.periodId)) {
          let row: Row = this.table.getRowByTerrainTypeId(terrain.terrainTypeId);
          if (!row) {
            this.table.rows.push(row = new Row());
            row.headerCell = new HeaderCell();
            row.headerCell.terrainType = this.terrainTypes.get(terrain.terrainTypeId);
            row.headerCell.location = this.locations.get(row.headerCell.terrainType.locationId);
          }

          let cell: TerrainCell = row.getTerrainCellByPeriodId(terrain.periodId);
          if (cell) {
            this.genTerrainCell(terrain, cell);
          } else {
            cell = this.genTerrainCell(terrain);
            row.terrainCells.push(cell);
          }
        }
      }
    );

    this.table.rows.sort(this.sorter);
    this.table.rows.forEach(
      (row) => row.terrainCells.sort(
        (a, b) => this.getPeriodById(a.terrain.periodId).order - this.getPeriodById(b.terrain.periodId).order
      )
    );

    this.dataCalculated = true;
  }

  getAttributionsByTerrain(terrainId: number): TerrainAttribution[] {
    const attrib: TerrainAttribution[] = [];

    for (const ta of this.attributions) {
      if (ta.terrainId === terrainId) {
        attrib.push(ta);
      }
    }

    attrib.sort((a, b) => {
      if (a.lastName === b.lastName) {
        return a.firstName.localeCompare(b.firstName);
      } else {
        return a.lastName.localeCompare(b.lastName);
      }
    });

    return attrib;
  }

  genTerrainCell(terrain: Terrain, cell?: TerrainCell): TerrainCell {
    if (!cell) {
      cell = new TerrainCell();
    }

    if (cell.terrain !== terrain) {
      cell.terrain = terrain;
    }
    cell.attributions = this.getAttributionsByTerrain(terrain.id);

    // RED if too many people
    // ORANGE if something to do
    // BLUE if nothing to do
    // GREEN if terrain is full and close

    if (cell.attributions.length >= cell.terrain.maxAmountPlaces) {
      if (ManagePhaseTerrainComponent.countStatedAttribution(cell.attributions, TerrainAttributionState.ACCEPTED) === cell.attributions.length) {
        cell.borderColor = '#28a745';
      } else {
        if (cell.attributions.length === cell.terrain.maxAmountPlaces) {
          if (cell.terrain.maxAmountPlaces === 0) {
            cell.borderColor = '#a3a7a1';
          } else {
            cell.borderColor = '#f39c12';
          }
        } else {
          cell.borderColor = '#f33231';
        }
      }
    } else {

      if (ManagePhaseTerrainComponent.countStatedAttribution(cell.attributions, TerrainAttributionState.ACCEPTED) === cell.attributions.length) {
        cell.borderColor = '#28a745';
      } else {
        cell.borderColor = '#f39c12';
      }
    }

    return cell;
  }

  addAttribution(terrainId: number, userId: any) {
    this.distributionService.addTerrainAttribution({userId, terrainId})
      .subscribe(
        (result) => {
          if (result.success) {
            this.refresh(false);
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  deleteAttribution(attributionId: number) {
    this.distributionService.deleteTerrainAttribution(attributionId, this.phase.id)
      .subscribe(
        (result) => {
          if (result.success) {
            this.refresh(false);
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  validate(attributionId: number) {
    this.distributionService.updateTerrainAttribution({id: attributionId, state: TerrainAttributionState.ACCEPTED}, this.phase.id)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.refresh(false);
          } else {
            this.card.error(result.errors, 4000);

          }
        }
      );
  }

  unvalidate(attributionId: number) {
    this.distributionService.updateTerrainAttribution({id: attributionId, state: TerrainAttributionState.WAITING}, this.phase.id)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.refresh(false);
          } else {
            this.card.error(result.errors, 4000);

          }
        }
      );
  }

  switchValidation(attributionId: number, state: TerrainAttributionState) {
    if (state === TerrainAttributionState.WAITING || state === TerrainAttributionState.NOT_PRIORITY) {
      this.validate(attributionId);
    } else if (state === TerrainAttributionState.ACCEPTED) {
      this.unvalidate(attributionId);
    }
  }

  validateAll(terrainId: number) {
    this.distributionService.validateMultipleTerrainAttributions(this.phase.id, terrainId)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.refresh(false);
          } else {
            this.card.error(result.errors, 4000);

          }
        }
      );
  }

  unvalidateAll(terrainId: number) {
    this.distributionService.unvalidateMultipleTerrainAttributions(this.phase.id, terrainId)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.refresh(false);
          } else {
            this.card.error(result.errors, 4000);

          }
        }
      );
  }

  deleteAll(terrainId: number) {
    this.distributionService.deleteMultipleTerrainAttributions(terrainId)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.refresh(false);
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  swapGroup() {
    if (this.selectedGroup === Group.A) {
      this.selectedGroup = Group.B;
    } else if (this.selectedGroup === Group.B) {
      this.selectedGroup = Group.A;
    }
    this.refresh();
  }

  trackByFn(type) {
    return (index, item) => type + '-' + item.id; // unique id corresponding to the item
  }

  trackRowByFn(index, item: Row) {
    return 'row-' + item.headerCell.terrainType.id;
  }

  trackTerrainCellByFn(index, item: TerrainCell) {
    return 'terrain-' + item.terrain.id;
  }

}

class Table {
  periods;
  rows: Row[]; // Map<TerrainTypeId, Row>
  constructor() {
    this.periods = [];
    this.rows = [];
  }

  getRowByTerrainTypeId(terrainTypeId: number) {
    for (const row of this.rows) {
      if (row.headerCell.terrainType.id === terrainTypeId) {
        return row;
      }
    }
  }
}

class Row {
  headerCell: HeaderCell;
  terrainCells: TerrainCell[]; // Map<PeriodId, Row>

  constructor() {
    this.terrainCells = [];
  }

  getTerrainCellByPeriodId(periodId: number) {
    for (const cell of this.terrainCells) {
      if (cell.terrain.periodId === periodId) {
        return cell;
      }
    }
  }

}

class Cell {
  borderColor = 'default';
  collapsed = true;
}

class HeaderCell extends Cell {
  terrainType: TerrainType;
  location: Location;
}

class TerrainCell extends Cell {
  terrain: Terrain;
  attributions: TerrainAttribution[] = [];
}
