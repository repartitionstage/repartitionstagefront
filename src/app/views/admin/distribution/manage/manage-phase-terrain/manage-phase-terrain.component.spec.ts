import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ManagePhaseTerrainComponent} from './manage-phase-terrain.component';

describe('ManagePhaseTerrainComponent', () => {
  let component: ManagePhaseTerrainComponent;
  let fixture: ComponentFixture<ManagePhaseTerrainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManagePhaseTerrainComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePhaseTerrainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
