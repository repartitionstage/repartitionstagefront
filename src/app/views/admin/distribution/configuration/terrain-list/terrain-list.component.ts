import {Component, OnInit, ViewChild} from '@angular/core';
import {Result} from '../../../../../service/rest-api/result';
import {DistributionService} from '../../../../../service/distribution/distribution.service';
import {Router} from '@angular/router';
import {InfoCardComponent} from '../../../../utils/info-card/info-card.component';
import {Terrain} from '../../../../../service/distribution/models/terrain';
import {HistoryService} from '../../../../../service/history/history.service';
import {KeyValue} from '@angular/common';

@Component({
  selector: 'rs-terrain-list',
  templateUrl: './terrain-list.component.html',
  styleUrls: ['./terrain-list.component.scss']
})
export class TerrainListComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  // CONSTANT
  years: { id: number, name: string }[];
  promotions: { id: number, name: string }[];
  periods: { id: number, beginDate: string, endDate: string, order: number }[];
  terrainTypes: { id: number, name: string }[];
  terrains: Terrain[];

  yearId: number;
  promotionId: number;

  dataCalculated = false;
  data: Map<number, Map<number, Terrain>>;

  addableTerrainTypes: { id: number, name: string }[];

  toDelete: Terrain[] = [];

  constructor(private distributionService: DistributionService,
              private router: Router,
              private history: HistoryService) {
  }

  ngOnInit() {
    this.distributionService.getYears().subscribe(
      (result: Result) => {
        if (result.success) {
          this.years = result.data;
          if (this.history.map.has('terrain-list-selected-year')) {
            this.yearId = this.history.map.get('terrain-list-selected-year');
            this.refresh();
          }
        } else {
          this.card.error(result.errors, 2000, () => this.router.navigate(['/admin']));
        }
      }
    );

    this.distributionService.getPromotions().subscribe(
      (result: Result) => {
        if (result.success) {
          this.promotions = result.data;

          if (this.history.map.has('terrain-list-selected-promotion')) {
            this.promotionId = this.history.map.get('terrain-list-selected-promotion');
            this.refresh();
          }

        } else {
          this.card.error(result.errors, 2000, () => this.router.navigate(['/admin']));
        }
      }
    );
  }

  refresh() {
    if (this.promotionId && this.yearId) {
      this.toDelete = [];
      this.data = undefined;
      this.promotionId = Number(this.promotionId);
      this.yearId = Number(this.yearId);

      this.history.map.set('terrain-list-selected-promotion', this.promotionId);
      this.history.map.set('terrain-list-selected-year', this.yearId);

      if (!isNaN(this.promotionId) && !isNaN(this.yearId)) {
        let periodsLoad, terrainsLoad, terrainTypesLoad = false;

        const updateFonction = () => {
          this.genData();
          this.calculateAddableTerrainTypes();
        };

        this.distributionService.getPeriods(this.yearId, this.promotionId)
          .subscribe(
            (result: Result) => {
              if (result.success) {
                this.periods = result.data;
                periodsLoad = true;
                if (periodsLoad && terrainTypesLoad && terrainsLoad) {
                  updateFonction();
                }
              } else {
                this.card.error(result.errors, 2000);
              }
            }
          );

        this.distributionService.getTerrainTypes()
          .subscribe(
            (result: Result) => {
              if (result.success) {
                this.terrainTypes = result.data;
                terrainTypesLoad = true;
                if (periodsLoad && terrainTypesLoad && terrainsLoad) {
                  updateFonction();
                }

              } else {
                this.card.error(result.errors, 2000);
              }
            }
          );

        this.distributionService.getTerrainsByYearAndPromotion(this.yearId, this.promotionId)
          .subscribe(
            (result: Result) => {
              if (result.success) {
                this.terrains = result.data;
                terrainsLoad = true;
                if (periodsLoad && terrainTypesLoad && terrainsLoad) {
                  updateFonction();
                }

              } else {
                this.card.error(result.errors, 2000);
              }
            }
          );
      } else {
        this.card.error(['Une erreur est survenue : ' + 2], 2000);
      }
    }
  }

  genData() {
    this.data = new Map<number, Map<number, Terrain>>();

    for (const terrain of this.terrains) {
      if (!this.data.has(terrain.terrainTypeId)) {
        const map = new Map<number, Terrain>();

        for (const period of this.periods) {
          if (period.id === terrain.periodId) {
            map.set(period.id, terrain);
          } else {
            map.set(period.id, new Terrain(0, period.id, this.promotionId, terrain.terrainTypeId, this.yearId));
          }
        }

        this.data.set(terrain.terrainTypeId, map);
      } else {
        this.data.get(terrain.terrainTypeId).set(terrain.periodId, terrain);
      }
    }

    this.dataCalculated = true;
  }

  reverseGenData(): Terrain[] {
    const terrains: Terrain[] = [];

    this.data.forEach(
      (map) => {
        map.forEach(
          (terrain) => {
            terrains.push(terrain);
          }
        );
      }
    );

    return terrains;
  }

  // must be called after genData()
  calculateAddableTerrainTypes() {
    this.addableTerrainTypes = [];
    for (const terrainType of this.terrainTypes) {
      if (!this.data.has(terrainType.id)) {
        this.addableTerrainTypes.push(terrainType);
      }
    }
  }

  lessOne(terrainTypeId: number, periodId: number) {
    if (this.data.has(terrainTypeId) && this.data.get(terrainTypeId).has(periodId)) {
      const terrain = this.data.get(terrainTypeId).get(periodId);
      terrain.maxAmountPlaces--;
      if (terrain.maxAmountPlaces < 0) {
        terrain.maxAmountPlaces = 0;
      }
    }
  }

  plusOne(terrainTypeId: number, periodId: number) {
    if (this.data.has(terrainTypeId) && this.data.get(terrainTypeId).has(periodId)) {
      this.data.get(terrainTypeId).get(periodId).maxAmountPlaces++;
    }
  }

  save() {
    const data = {
      toUpdate: this.reverseGenData(),
      toDelete: this.toDelete.map(terrain => terrain.id)
    };

    this.distributionService.updateTerrainsByYearAndPromotion(this.yearId, this.promotionId, data)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.refresh();
            this.card.info(['Terrains sauvegardés avec succès'], 2000);
          } else {
            this.card.error(result.errors, 2000);
          }
        }
      );
  }

  addTerrainType(terrainTypeId: number | string) {
    if (this.addableTerrainTypes.length > 0) {
      terrainTypeId = Number(terrainTypeId);
      if (!isNaN(terrainTypeId)) {
        this.toDelete = this.toDelete.filter((terrain) => (terrain.terrainTypeId !== terrainTypeId));

        const map = new Map<number, Terrain>();
        for (const period of this.periods) {
          map.set(period.id, new Terrain(0, period.id, this.promotionId, terrainTypeId, this.yearId));
        }
        this.data.set(terrainTypeId, map);

        this.calculateAddableTerrainTypes();
      } else {
        this.card.error(['Une erreur est survenue : ' + 1], 2000);
      }
    }
  }

  removeTerrainType(id: number | string) {
    id = Number(id);
    if (!isNaN(id) && this.data.has(id)) {
      this.data.get(id).forEach(
        (terrain) => {
          this.toDelete.push(terrain);
        }
      );
      this.data.delete(id);
      this.calculateAddableTerrainTypes();
    }
  }

  getTerrainType(id: number) {
    for (const tt of this.terrainTypes) {
      if (tt.id === id) {
        return tt;
      }
    }
  }

  sorter(terrainTypes) {
    const getTerrainType = (id: number) => {
      for (const tt of terrainTypes) {
        if (tt.id === id) {
          return tt;
        }
      }
    };
    return (a: KeyValue<number, Map<number, Terrain>>, b: KeyValue<number, Map<number, Terrain>>) =>
      getTerrainType(a.key).name.localeCompare(getTerrainType(b.key).name);
  }


}
