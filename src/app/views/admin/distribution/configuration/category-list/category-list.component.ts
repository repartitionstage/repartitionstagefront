import {Component, OnInit} from '@angular/core';
import {DistributionService} from '../../../../../service/distribution/distribution.service';

@Component({
  selector: 'rs-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {

  columns = [
    {
      id: 'id',
      name: 'id',
      editable: false,
      visible: true,
      width: 10
    },
    {
      id: 'name',
      name: 'Nom',
      editable: true,
      visible: true,
      width: 20
    },
    {
      id: 'maxAttributions',
      name: 'Nombre max de stage',
      editType: 'number',
      editable: true,
      visible: true,
      width: 20
    },
    {
      id: 'maxAttributionsPerYear',
      name: 'Nombre max de stages par an',
      editType: 'number',
      editable: true,
      visible: true,
      width: 20
    },
    {
      id: 'maxChoices',
      name: 'Nombre de choix maximum',
      editType: 'number',
      editable: true,
      visible: true,
      width: 20
    }
  ];
  min = 0;
  pas = 0;
  get = ((service) => (limit: number, offset: number) => service.getCategories(limit, offset))(this.distributionService);
  add = ((service) => (year: any) => service.addCategory(year))(this.distributionService);
  edit = ((service) => (year: any) => service.updateCategory(year))(this.distributionService);
  del = ((service) => (yearId: number) => service.deleteCategory(yearId))(this.distributionService);

  constructor(private distributionService: DistributionService) {
  }

  ngOnInit(): void {
  }

}
