import {Component, OnInit} from '@angular/core';
import {DistributionService} from '../../../../../service/distribution/distribution.service';

@Component({
  selector: 'rs-promotion-list',
  templateUrl: './promotion-list.component.html',
  styleUrls: ['./promotion-list.component.scss']
})
export class PromotionListComponent implements OnInit {

  columns = [
    {
      id: 'id',
      name: 'id',
      editable: false,
      visible: true,
      width: 25
    },
    {
      id: 'name',
      name: 'Nom',
      editable: true,
      visible: true,
      width: 65
    },
    {
      id: 'selectableOnRegistration',
      name: 'Sélectionnable à l\'inscription',
      editType: 'checkbox',
      editable: true,
      visible: true,
      width: 10
    }
  ];
  min = 0;
  pas = 0;
  get = ((service) => (limit: number, offset: number) => service.getPromotions(false, limit, offset))(this.distributionService);
  add = ((service) => (year: any) => service.addPromotion(year))(this.distributionService);
  edit = ((service) => (year: any) => service.updatePromotion(year))(this.distributionService);
  del = ((service) => (yearId: number) => service.deletePromotion(yearId))(this.distributionService);

  constructor(private distributionService: DistributionService) {
  }


  ngOnInit() {
  }

}
