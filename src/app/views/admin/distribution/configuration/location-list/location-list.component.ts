import {Component, OnInit} from '@angular/core';
import {DistributionService} from '../../../../../service/distribution/distribution.service';

@Component({
  selector: 'rs-location-list',
  templateUrl: './location-list.component.html',
  styleUrls: ['./location-list.component.scss']
})
export class LocationListComponent implements OnInit {

  columns = [
    {
      id: 'id',
      name: 'id',
      editable: false,
      visible: true,
      width: 10
    },
    {
      id: 'name',
      name: 'Nom',
      editable: true,
      visible: true,
      width: 45
    },
    {
      id: 'distance',
      name: 'Distance',
      editable: true,
      visible: true,
      editType: 'number',
      width: 45
    }
  ];
  min = 0;
  pas = 0;
  get = ((service) => (limit: number, offset: number) => service.getLocations(limit, offset))(this.distributionService);
  add = ((service) => (year: any) => service.addLocation(year))(this.distributionService);
  edit = ((service) => (year: any) => service.updateLocation(year))(this.distributionService);
  del = ((service) => (yearId: number) => service.deleteLocation(yearId))(this.distributionService);

  constructor(private distributionService: DistributionService) {
  }


  ngOnInit() {
  }

}
