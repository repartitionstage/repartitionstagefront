import {Component, OnInit, ViewChild} from '@angular/core';
import {DistributionService} from '../../../../../service/distribution/distribution.service';
import {InfoCardComponent} from '../../../../utils/info-card/info-card.component';
import {Result} from '../../../../../service/rest-api/result';
import {Category} from '../../../../../service/distribution/models/category';
import {TerrainType} from '../../../../../service/distribution/models/terrain-type';
import {Location} from '../../../../../service/distribution/models/location';

@Component({
  selector: 'rs-terrain-type-list',
  templateUrl: './terrain-type-list.component.html',
  styleUrls: ['./terrain-type-list.component.scss']
})
export class TerrainTypeListComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  terrainTypes: TerrainType[];
  locations: Location[];
  categories: Category[];

  get = ((service) => (limit: number, offset: number) => service.getTerrainTypes(limit, offset))(this.distributionService);
  add = ((service) => (terrainType: any) => service.addTerrainType(terrainType))(this.distributionService);
  edit = ((service) => (terrainType: any) => service.updateTerrainType(terrainType))(this.distributionService);
  del = ((service) => (yearId: number) => service.deleteTerrainType(yearId))(this.distributionService);

  changedTerrainType = {
    maxChoices: -1,
    maxAttributions: -1,
    maxAttributionsPerYear: -1,
  };
  showAddForm = true;
  submit = false;

  showEditBtn = true;
  showEditForm = false;
  showEditConfirmation = false;

  deletedRow;
  showDeleteBtn = true;
  showDelConfirmation = false;
  confirmDeleteData = {
    title: 'Confirmer la suppression ?',
  };

  constructor(private distributionService: DistributionService) {
  }

  ngOnInit() {
    this.loadLocations();
    this.loadCategories();
    this.loadLocations();

    this.refreshTable();
  }

  refreshTable() {
    this.get(0, 0).subscribe(
      (result: Result) => {
        if (result.success) {
          this.terrainTypes = result.data; // result.params.get('data');
          this.terrainTypes.sort((a, b) => a.name.localeCompare(b.name));
        } else {
          this.card.error(result.errors, 2000);
        }
      }
    );
  }

  delElement($event: any) {
    if ($event) {
      this.del(this.deletedRow.id).subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.info(result.messages, 2000);
          } else {
            this.card.error(result.errors, 2000);
          }
          this.setBasicState();
          this.refreshTable();
        }
      );
    } else {
      this.setBasicState();
    }
  }

  confirmDelete(id: string | number) {
    this.showAddForm = false;
    this.showEditBtn = false;
    this.showDeleteBtn = false;

    this.showDelConfirmation = true;
    this.deletedRow = {id: id};
  }

  setBasicState() {
    this.submit = false;
    this.showEditForm = false;

    this.showAddForm = true;
    this.showEditBtn = true;
    this.showDeleteBtn = true;

    this.showEditConfirmation = false;
    this.showDelConfirmation = false;

    this.deletedRow = undefined;
    this.changedTerrainType = {
      maxChoices: -1,
      maxAttributions: -1,
      maxAttributionsPerYear: -1,
    };
  }

  addNewTerrainType() {
    this.submit = true;
    this.add(this.changedTerrainType).subscribe(
      (result: Result) => {
        if (result.success) {
          this.card.info(['Nouveau type de terrain créé'], 2000);
          this.setBasicState();
          this.refreshTable();
        } else {
          this.card.error(result.errors, 2000);
        }
      }
    );
  }

  getLocationName(id: number): string {
    for (const loc of this.locations) {
      if (loc.id === id) {
        return loc.name;
      }
    }
  }

  editTerrainType(terrainType) {
    this.showAddForm = false;
    this.showEditForm = true;

    this.showEditBtn = false;
    this.showDeleteBtn = false;

    this.changedTerrainType = terrainType;
  }

  saveEditedTerrainType() {
    this.submit = true;
    this.edit(this.changedTerrainType).subscribe(
      (result: Result) => {
        if (result.success) {
          this.card.info(['Sauvegarde réussie'], 2000);
          this.setBasicState();
          this.refreshTable();
        } else {
          this.card.error(result.errors, 2000);
        }
      }
    );
  }

  cancelEdition() {
    this.setBasicState();
  }

  private loadLocations() {
    this.distributionService.getLocations(0, 0).subscribe(
      (result: Result) => {
        if (result.success) {
          this.locations = result.data;
          this.locations.sort(
            (a: Location, b: Location) => {
              return a.name.localeCompare(b.name);
            }
          );
        } else {
          this.card.error(result.errors, 2000);
        }
      }
    );
  }

  private loadCategories() {
    this.distributionService.getCategories(0, 0).subscribe(
      (result: Result) => {
        if (result.success) {
          this.categories = result.data;
          this.categories.sort(
            (a: Category, b: Category) => {
              return a.name.localeCompare(b.name);
            }
          );
        } else {
          this.card.error(result.errors, 2000);
        }
      }
    );
  }
}
