import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TerrainTypeListComponent} from './terrain-type-list.component';

describe('TerrainTypeListComponent', () => {
  let component: TerrainTypeListComponent;
  let fixture: ComponentFixture<TerrainTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TerrainTypeListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerrainTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
