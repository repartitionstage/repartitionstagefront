import {Component, OnInit} from '@angular/core';
import {DistributionService} from '../../../../../service/distribution/distribution.service';

@Component({
  selector: 'rs-year-list',
  templateUrl: './year-list.component.html',
  styleUrls: ['./year-list.component.scss']
})
export class YearListComponent implements OnInit {

  columns = [
    {
      id: 'id',
      name: 'id',
      editable: false,
      visible: true,
      width: 25
    },
    {
      id: 'name',
      name: 'Nom',
      editable: true,
      visible: true,
      width: 75
    }
  ];
  min = 0;
  pas = 0;
  get = ((service) => (limit: number, offset: number) => service.getYears(limit, offset))(this.distributionService);
  add = ((service) => (year: any) => service.addYear(year))(this.distributionService);
  edit = ((service) => (year: any) => service.updateYear(year))(this.distributionService);
  del = ((service) => (yearId: number) => service.deleteYear(yearId))(this.distributionService);

  constructor(private distributionService: DistributionService) {
  }

  ngOnInit() {
  }

}
