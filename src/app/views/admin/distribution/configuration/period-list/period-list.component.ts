import {Component, OnInit, ViewChild} from '@angular/core';
import {OrderedEditableTableComponent} from '../../../../utils/ordered-editable-table/ordered-editable-table.component';
import {DistributionService} from '../../../../../service/distribution/distribution.service';
import {Result} from '../../../../../service/rest-api/result';
import {Router} from '@angular/router';
import {HistoryService} from '../../../../../service/history/history.service';
import {InfoCardComponent} from '../../../../utils/info-card/info-card.component';

@Component({
  selector: 'rs-period-list',
  templateUrl: './period-list.component.html',
  styleUrls: ['./period-list.component.scss']
})
export class PeriodListComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  years: { id: number, name: string }[];
  promotions: { id: number, name: string }[];


  columns = [
    {
      id: 'id',
      name: 'ID',
      editable: false,
      visible: true,
      width: 5
    },
    {
      id: 'order',
      name: 'Ordre',
      editable: false,
      visible: true,
      width: 5
    },
    {
      id: 'beginDate',
      name: 'Date de début',
      editType: 'datetime-local',
      editable: true,
      visible: true,
      width: 25
    },
    {
      id: 'endDate',
      name: 'Date de fin',
      editType: 'datetime-local',
      editable: true,
      visible: true,
      width: 25
    },
    {
      id: 'group',
      name: 'Groupe',
      editType: 'select',
      choices:
        [
          {
            name: 'Aucun',
            value: 1
          },
          {
            name: 'Groupe A',
            value: 2
          },
          {
            name: 'Groupe B',
            value: 3
          },
          {
            name: 'Les deux',
            value: 4
          }
        ],
      editable: true,
      visible: true,
      width: 20
    }
  ];
  data: any = {};

  add = ((service) => (period: any) => {
    period.promotionId = this.data.promotionId;
    period.yearId = this.data.yearId;
    return service.addPeriod(period);
  })(this.distributionService);
  del = ((service) => (period: any) => service.deletePeriod(period))(this.distributionService);
  edit = ((service) => (period: any) => {
    period.promotionId = this.data.promotionId;
    period.yearId = this.data.yearId;
    return service.updatePeriod(period);
  })(this.distributionService);
  get = ((service) => (() => service.getPeriods(this.data.yearId, this.data.promotionId)))
  (this.distributionService);

  @ViewChild('periodList') child: OrderedEditableTableComponent;

  constructor(private distributionService: DistributionService,
              private router: Router,
              private history: HistoryService) {
  }

  ngOnInit() {
    this.distributionService.getYears(0, 0).subscribe(
      (result: Result) => {
        if (result.success) {
          this.years = result.data;
          if (this.history.map.has('period-list-selected-year')) {
            this.data.yearId = this.history.map.get('period-list-selected-year');
            this.refresh();
          }
        } else {
          this.card.error(result.errors, 2000,
            () => {
              this.router.navigate(['/admin']);
            });
        }
      }
    );

    this.distributionService.getPromotions().subscribe(
      (result: Result) => {
        if (result.success) {
          this.promotions = result.data;
          if (this.history.map.has('period-list-selected-promotion')) {
            this.data.promotionId = this.history.map.get('period-list-selected-promotion');
            this.refresh();
          }
        } else {
          this.card.error(result.errors, 2000,
            () => {
              this.router.navigate(['/admin']);
            });
        }
      }
    );
  }

  refresh() {
    if (this.data.promotionId && this.data.yearId) {
      this.history.map.set('period-list-selected-year', this.data.yearId);
      this.history.map.set('period-list-selected-promotion', this.data.promotionId);

      if (this.child) {
        this.child.refreshTable();
      }
    }
  }
}
