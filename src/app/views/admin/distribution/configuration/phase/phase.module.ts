import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PhaseRoutingModule} from './phase-routing.module';
import {PhasesListComponent} from './phases-list/phases-list.component';
import {PhaseEditComponent} from './phase-edit/phase-edit.component';
import {FormsModule} from '@angular/forms';
import {RsUtilsModule} from '../../../../utils/rs-utils.module';
import {NgxEditorModule} from 'ngx-editor';
import {TextualPhaseEditerComponent} from './textual-phase-editer/textual-phase-editer.component';
import {DragulaModule} from 'ng2-dragula';
import {GroupPhaseEditerComponent} from './group-phase-editer/group-phase-editer.component';
import {TerrainPhaseEditerComponent} from './terrain-phase-editer/terrain-phase-editer.component';

@NgModule({
  declarations: [
    PhasesListComponent,
    PhaseEditComponent,
    TextualPhaseEditerComponent,
    GroupPhaseEditerComponent,
    TerrainPhaseEditerComponent
  ],
  imports: [
    CommonModule,
    PhaseRoutingModule,
    FormsModule,
    RsUtilsModule,
    NgxEditorModule,
    DragulaModule
  ]
})
export class PhaseModule {
}
