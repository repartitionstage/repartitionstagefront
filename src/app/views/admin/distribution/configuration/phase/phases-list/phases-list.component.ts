import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DistributionService} from '../../../../../../service/distribution/distribution.service';
import {Result} from '../../../../../../service/rest-api/result';
import {InfoCardComponent} from '../../../../../utils/info-card/info-card.component';
import {Subscription} from 'rxjs';
import {DragulaService} from 'ng2-dragula';
import {Phase} from '../../../../../../service/distribution/models/phase';
import {HistoryService} from '../../../../../../service/history/history.service';

@Component({
  selector: 'rs-phases-list',
  templateUrl: './phases-list.component.html',
  styleUrls: ['./phases-list.component.scss']
})
export class PhasesListComponent implements OnInit, OnDestroy {

  years: { id: number, name: string }[];

  @ViewChild('card') card: InfoCardComponent;

  @ViewChild('tablebody') tablebody: ElementRef;

  constructor(private distributionService: DistributionService,
              private dragulaService: DragulaService,
              private historyService: HistoryService) {
  }
  promotions: { id: number, name: string }[];
  lines = [];

  data: any = {};

  showAddBtn = true;
  showEditBtn = true;
  showDelBtn = true;
  showDelConfirmation = false;
  deletedRow;
  confirmDeleteData = {
    title: 'Confirmer la suppression ?',
  };
  subs = new Subscription();

  static getStandardConfig(type: string) {
    const config = {type: type};
    switch (type) {
      case 'terrain':
      case 'spreading':
        config['availableTerrainTypes'] = [];
        config['maxChoicesPerPeriod'] = -1;
        break;
      case 'group':
        config['onlyRedoubling'] = false;
        break;
      case 'textual':
        config['htmlContent'] = '';
        break;
    }
    return config;
  }

  ngOnInit() {
    this.distributionService.getPromotions()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.promotions = result.data;

            if (this.historyService.map.has('phases-list-selected-promotions')) {
              this.data['promotionId'] = this.historyService.map.get('phases-list-selected-promotions');
              this.refresh();
            }
          } else {
            this.card.error(result.errors, 2000);
          }
        }
      );

    this.distributionService.getYears()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.years = result.data;

            if (this.historyService.map.has('phases-list-selected-year')) {
              this.data['yearId'] = this.historyService.map.get('phases-list-selected-year');
              this.refresh();
            }
          } else {
            this.card.error(result.errors, 2000);
          }
        }
      );

  }

  refresh() {
    if (this.data['yearId'] && this.data['promotionId']) {
      this.historyService.map.set('phases-list-selected-promotions', this.data['promotionId']);
      this.historyService.map.set('phases-list-selected-year', this.data['yearId']);

      this.refreshTable();

      this.subs.add(
        this.dragulaService.drop('RESULT_LINE')
          .subscribe(() => {
              this.updateAndSaveOrder();
            }
          )
      );

    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  confirmDelete(id: string) {
    this.showAddBtn = false;
    this.showEditBtn = false;
    this.showDelBtn = false;

    this.showDelConfirmation = true;
    this.deletedRow = {id: id};
  }

  delElement($event: any) {
    if ($event) {
      this.distributionService.deletePhase(this.deletedRow.id).subscribe(
        (result: Result) => {
          this.setResultMessages(result);
          for (let i = 0; i < this.lines.length; i++) {
            if (this.lines[i]['id'] === this.deletedRow.id) {
              break;
            }
          }
          this.updateAndSaveOrder();
        }
      );
    } else {
      this.setBasicState();
    }
  }

  setBasicState() {
    this.showAddBtn = true;
    this.showEditBtn = true;
    this.showDelBtn = true;

    this.showDelConfirmation = false;

    this.deletedRow = undefined;
  }

  updateAndSaveOrder() {
    const tbody = this.tablebody.nativeElement;
    let i = 0;
    for (const el of tbody.children) {
      const id = Number(el.id.split('row-')[1]);
      for (const line of this.lines) {
        if (line['id'] === id) {
          line.order = i;
          i++;
          break;
        }
      }
    }
    i = 0;
    for (const line of this.lines) {
      this.distributionService.updatePhase(line).subscribe(
        (result: Result) => {
          this.setResultMessages(result);
          i++;
          if (i === this.lines.length) {
            this.refreshTable();
            this.setBasicState();
          }
        }
      );
    }
  }

  setResultMessages(result: Result) {
    if (result.success) {
      this.card.info(result.messages, 2000);
    } else {
      this.card.error(result.errors, 2000);
    }
  }

  createPhase(type: string) {
    const phase = new Phase(-1, this.lines.length, 'Sans nom', new Date(), new Date(), false, false, this.data.promotionId, this.data.yearId);
    phase.config = PhasesListComponent.getStandardConfig(type);
    delete phase.id;
    this.distributionService.addPhase(phase).subscribe(
      (result: Result) => {
        this.setResultMessages(result);
        if (result.success) {
          this.refreshTable();
        }
      }
    );
  }

  private refreshTable() {
    this.setBasicState();
    this.distributionService.getPhases(this.data.yearId, this.data.promotionId).subscribe(
      (result: Result) => {
        if (result.success) {
          this.lines = result.data;
          this.lines.sort((a, b) => (a['order'] - b['order']));
        }
        if (result.success) {
          this.card.info(result.messages, 2000);
        } else {
          this.card.error(result.errors, 2000);
        }
      }
    );
  }
}
