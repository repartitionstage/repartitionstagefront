import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PhasesListComponent} from './phases-list.component';

describe('PhasesListComponent', () => {
  let component: PhasesListComponent;
  let fixture: ComponentFixture<PhasesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PhasesListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhasesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should post', () => {
    expect(component).toBeTruthy();
  });
});
