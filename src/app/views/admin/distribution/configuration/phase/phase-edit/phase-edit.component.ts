import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DistributionService} from '../../../../../../service/distribution/distribution.service';
import {Result} from '../../../../../../service/rest-api/result';
import {PhaseEditer} from '../phase-editer';
import {Phase} from '../../../../../../service/distribution/models/phase';
import {InfoCardComponent} from '../../../../../utils/info-card/info-card.component';

@Component({
  selector: 'rs-phase-edit',
  templateUrl: './phase-edit.component.html',
  styleUrls: ['./phase-edit.component.scss']
})
export class PhaseEditComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  @ViewChild('phaseEditer') phaseEditer: PhaseEditer;

  phaseId: number;
  phase: Phase;
  submitted = false;

  constructor(private route: ActivatedRoute,
              private dbSrv: DistributionService,
              private router: Router) {
  }

  ngOnInit() {
    this.phaseId = Number(this.route.snapshot.params['id']);
    if (this.phaseId === undefined || isNaN(this.phaseId)) {
      this.router.navigate(['/admin/distribution/phases']);
      return;
    }

    this.dbSrv.getPhase(this.phaseId).subscribe(
      (result: Result) => {
        if (result.success) {
          this.phase = result.data;
        } else {
          this.router.navigate(['/admin/distribution/phases']);
        }
      }
    );

  }

  save() {
    this.submitted = true;
    this.card.warning(['Sauvegarde en cours']);

    this.phase.config = this.phaseEditer.config;
    this.dbSrv.updatePhase(this.phase).subscribe(
      (result: Result) => {
        this.card.hide();
        if (result.success) {
          this.card.info(['Sauvegarde réussie'], 750,
            ((router: Router) => {
              return () => {
                router.navigate(['/admin/distribution/phases']);
                this.submitted = false;
              };
            })(this.router)
          );
        } else {
          this.card.error(result.errors, 2000);
          this.submitted = false;

        }
      }
    );
  }
}
