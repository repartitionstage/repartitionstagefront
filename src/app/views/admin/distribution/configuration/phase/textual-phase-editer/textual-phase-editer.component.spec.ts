import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TextualPhaseEditerComponent} from './textual-phase-editer.component';

describe('TextualPhaseEditerComponent', () => {
  let component: TextualPhaseEditerComponent;
  let fixture: ComponentFixture<TextualPhaseEditerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TextualPhaseEditerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextualPhaseEditerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
