import {Component, OnInit} from '@angular/core';
import {PhaseEditer} from '../phase-editer';

@Component({
  selector: 'rs-textual-phase-editer',
  templateUrl: './textual-phase-editer.component.html',
  styleUrls: ['./textual-phase-editer.component.scss']
})
export class TextualPhaseEditerComponent extends PhaseEditer implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}

