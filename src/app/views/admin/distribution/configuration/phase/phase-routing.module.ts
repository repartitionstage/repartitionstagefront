import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminGuard} from '../../../../../guard/admin/admin.guard';
import {PhasesListComponent} from './phases-list/phases-list.component';
import {PhaseEditComponent} from './phase-edit/phase-edit.component';

const routes: Routes = [
  {
    path: 'home',
    canActivate: [AdminGuard],
    component: PhasesListComponent,
    data: {
      title: 'Configuration des phases',
      breadcrumbs: 'Configuration'
    }
  },
  {
    path: 'edit/:id',
    canActivate: [AdminGuard],
    component: PhaseEditComponent,
    data: {
      title: 'Edition',
    }
  },
  {
    path: '*',
    redirectTo: 'home',
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhaseRoutingModule {
}
