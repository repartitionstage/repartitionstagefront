import {Component, OnInit} from '@angular/core';
import {PhaseEditer} from '../phase-editer';

@Component({
  selector: 'rs-group-phase-editer',
  templateUrl: './group-phase-editer.component.html',
  styleUrls: ['./group-phase-editer.component.scss']
})
export class GroupPhaseEditerComponent extends PhaseEditer implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
