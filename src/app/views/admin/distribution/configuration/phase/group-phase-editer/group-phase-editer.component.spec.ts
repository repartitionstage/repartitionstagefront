import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GroupPhaseEditerComponent} from './group-phase-editer.component';

describe('GroupPhaseEditerComponent', () => {
  let component: GroupPhaseEditerComponent;
  let fixture: ComponentFixture<GroupPhaseEditerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GroupPhaseEditerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupPhaseEditerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
