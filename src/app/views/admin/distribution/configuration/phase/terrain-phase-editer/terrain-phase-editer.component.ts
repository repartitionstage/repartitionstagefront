import {Component, Input, OnInit} from '@angular/core';
import {PhaseEditer} from '../phase-editer';
import {DistributionService} from '../../../../../../service/distribution/distribution.service';
import {Result} from '../../../../../../service/rest-api/result';

@Component({
  selector: 'rs-terrain-phase-editer',
  templateUrl: './terrain-phase-editer.component.html',
  styleUrls: ['./terrain-phase-editer.component.scss']
})
export class TerrainPhaseEditerComponent extends PhaseEditer implements OnInit {

  @Input() config: {
    type: 'spreading' | 'terrain',
    availableTerrainTypes: number[],
    maxChoicesPerPeriod: number,
    maxChoices: number,
  };
  @Input() yearId: number;
  @Input() promotionId: number;

  terrainTypes: { id: number, name: string }[];
  openedTerrainTypes: { id: number, name: string }[];
  showedOpenedTerrainTypes: { id: number, name: string }[];

  constructor(private distributionService: DistributionService) {
    super();
  }

  ngOnInit() {
    if (!this.config.availableTerrainTypes || !Array.isArray(this.config.availableTerrainTypes)) {
      this.config.availableTerrainTypes = [];
    }
    if (!this.config.maxChoicesPerPeriod) {
      this.config.maxChoicesPerPeriod = -1;
    }

    if (this.yearId && this.promotionId) {
      this.distributionService.getTerrainTypes(0, 0).subscribe(
        (result: Result) => {
          if (result.success) {
            this.terrainTypes = result.data;
            this.refresh();
          } else {
            console.error('Unable to load terrain types');
          }
        }
      );

      this.distributionService.getOpenTerrainTypeByYearAndPromotion(this.yearId, this.promotionId)
        .subscribe(
          (result: Result) => {
            if (result.success) {
              this.openedTerrainTypes = result.data;
              this.refresh();
            } else {
              console.error('Unable to load open terrain types');
            }
          }
        );
    }
  }

  refresh() {
    if (this.terrainTypes && this.openedTerrainTypes) {
      this.calculateShowedOpenedTerrainTypes();
    }
  }

  getTerrainTypeName(id: number): string {
    for (const tt of this.terrainTypes) {
      if (tt.id === id) {
        return tt.name;
      }
    }
  }

  addTerrainType(id: number | string) {
    id = Number(id);
    if (!isNaN(id) && this.showedOpenedTerrainTypes && this.showedOpenedTerrainTypes.length > 0) {
      this.config.availableTerrainTypes.push(id);
      this.calculateShowedOpenedTerrainTypes();
    }
  }

  removeTerrainType(id: number | string) {
    id = Number(id);
    if (!isNaN(id) && this.config.availableTerrainTypes.includes(id)) {
      this.config.availableTerrainTypes.splice(this.config.availableTerrainTypes.indexOf(id), 1);
      this.calculateShowedOpenedTerrainTypes();
    }
  }

  private calculateShowedOpenedTerrainTypes() {
    this.showedOpenedTerrainTypes = [];

    const contains = (id): boolean => {
      for (const ttId of this.config.availableTerrainTypes) {
        if (ttId === id) {
          return true;
        }
      }
      return false;
    };

    for (const tt of this.openedTerrainTypes) {
      if (!contains(tt.id)) {
        this.showedOpenedTerrainTypes.push(tt);
      }
    }

    this.sortOpenedTerrainTypes();
  }

  private sortOpenedTerrainTypes(): void {
    this.config.availableTerrainTypes.sort(
      (a: number, b: number) => {
        return this.getTerrainTypeName(a).localeCompare(this.getTerrainTypeName(b));
      }
    );
  }
}
