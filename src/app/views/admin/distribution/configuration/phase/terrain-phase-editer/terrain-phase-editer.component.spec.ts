import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TerrainPhaseEditerComponent} from './terrain-phase-editer.component';

describe('TerrainPhaseEditerComponent', () => {
  let component: TerrainPhaseEditerComponent;
  let fixture: ComponentFixture<TerrainPhaseEditerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TerrainPhaseEditerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerrainPhaseEditerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
