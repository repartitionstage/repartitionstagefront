import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AttributionsPerUserComponent} from './attributions-per-user.component';

describe('AttributionsPerUserComponent', () => {
  let component: AttributionsPerUserComponent;
  let fixture: ComponentFixture<AttributionsPerUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AttributionsPerUserComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttributionsPerUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
