import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../../../../service/user/user';
import {UserService} from '../../../../service/user/user.service';
import {Result} from '../../../../service/rest-api/result';
import {InfoCardComponent} from '../../../utils/info-card/info-card.component';
import {TerrainAttributionState} from '../../../../service/distribution/models/terrain-attribution-state.enum';
import {DistributionService} from '../../../../service/distribution/distribution.service';

@Component({
  selector: 'rs-attributions-per-user',
  templateUrl: './attributions-per-user.component.html',
  styleUrls: ['./attributions-per-user.component.scss']
})
export class AttributionsPerUserComponent implements OnInit {

  TerrainAttributionState = TerrainAttributionState;
  @ViewChild('card') card: InfoCardComponent;

  allUsers: User[];

  selectedUserId: number;
  user: User;

  content: {
    year: { id: number, name: string },
    promotion: { id: number, name: string },
    terrains: { id: number, name: string, periodOrder: number, beginDate: string, endDate: string, state: TerrainAttributionState }[],
  }[] = [];

  constructor(private userService: UserService,
              private distributionService: DistributionService) {
  }

  ngOnInit() {
    this.userService.getAllUsers()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.allUsers = result.data.list;
            this.allUsers.sort(
              ((a, b) => a.lastName.localeCompare(b.lastName))
            );
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  showInfos() {
    this.userService.getProfileInfos(this.selectedUserId)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.user = result.data;
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );

    this.distributionService.getTerrainAttributionByUser(this.selectedUserId)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            const data = result.data;
            this.calculateContent(data);
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );

  }


  getByYearId(id: number) {
    for (const line of this.content) {
      if (line.year.id === id) {
        return line;
      }
    }
  }

  calculateContent(lines: Line[]) {
    this.content = [];
    for (const line of lines) {
      let contentLine = this.getByYearId(line.yearId);
      if (!contentLine) {
        contentLine = {
          year: {
            id: line.yearId,
            name: line.yearName,
          },
          promotion: {
            id: line.promotionId,
            name: line.promotionName
          },
          terrains: []
        };
        this.content.push(contentLine);
      }

      contentLine.terrains.push(
        {
          id: line.attribId,
          name: line.terrainTypeName,
          state: line.state,
          periodOrder: line.periodOrder,
          beginDate: line.beginDate,
          endDate: line.endDate,
        }
      );


    }

    this.content.forEach((line) => line.terrains.sort((a, b) => (a.periodOrder - b.periodOrder)));

    this.content.sort((a, b) => (a.year.id - b.year.id));
  }


  deleteAttribution(attribId: number) {
    this.distributionService.deleteTerrainAttribution(attribId)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Attribution supprimée avec succès'], 3000);
            this.showInfos();
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }
}

class Line {
  yearId: number;
  yearName: string;

  promotionId: number;
  promotionName: string;

  periodId: number;
  periodOrder: number;
  beginDate: string;
  endDate: string;

  terrainTypeId: number;
  terrainTypeName: string;

  attribId: number;
  state: TerrainAttributionState;
}
