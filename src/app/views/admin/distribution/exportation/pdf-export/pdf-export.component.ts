import {Component, OnInit, ViewChild} from '@angular/core';
import {DistributionService} from '../../../../../service/distribution/distribution.service';
import {Year} from '../../../../../service/distribution/models/year';
import {Result} from '../../../../../service/rest-api/result';
import {InfoCardComponent} from '../../../../utils/info-card/info-card.component';
import {Category} from '../../../../../service/distribution/models/category';
import {Promotion} from '../../../../../service/distribution/models/promotion';

@Component({
  selector: 'rs-distribution-export',
  templateUrl: './pdf-export.component.html',
  styleUrls: ['./pdf-export.component.scss']
})
export class PdfExportComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  years: Year[];
  promotions: Promotion[];
  categories: Category[];

  sortType: 'user' | 'terrain' = 'terrain';

  query: {
    yearId: number;
    promotionId: number;
    categoryId?: number;
    sortType: 'user' | 'terrain';
  } = {
    yearId: -1,
    promotionId: -1,
    categoryId: null,
    sortType: 'terrain',
  };

  constructor(private distribution: DistributionService) {
  }

  ngOnInit() {
    this.distribution.getYears()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.years = result.data;
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );

    this.distribution.getPromotions()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.promotions = result.data;
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );

    this.distribution.getCategories()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.categories = result.data;
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  getPromotionName(id: number): string {
    for (const promotion of this.promotions) {
      if (promotion.id === Number(id)) {
        return promotion.name;
      }
    }
  }

  getCategoryName(id: number): string {
    for (const category of this.categories) {
      if (category.id === Number(id)) {
        return category.name;
      }
    }
  }

  getExportation() {
    const title = this.getPromotionName(this.query.promotionId) + ' - '
      + ((this.query.categoryId !== null) ? this.getCategoryName(this.query.categoryId) + ' - ' : '')
      + (this.query.sortType === 'user' ? 'Stages par externe' : this.query.sortType === 'terrain' ? 'Externes par stage' : '')
      + '.xlsx';

    this.distribution.getDistributionExportation(this.query.yearId, this.query.promotionId, this.query.sortType, this.query.categoryId, title, 'application/ms-excel')
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Exportation réussie'], 3000);
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

}
