import {Component, OnInit, ViewChild} from '@angular/core';
import {InfoCardComponent} from '../../../../utils/info-card/info-card.component';
import {Year} from '../../../../../service/distribution/models/year';
import {DistributionService} from '../../../../../service/distribution/distribution.service';
import {Result} from '../../../../../service/rest-api/result';

@Component({
  selector: 'rs-csv-export',
  templateUrl: './csv-export.component.html',
  styleUrls: ['./csv-export.component.scss']
})
export class CsvExportComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  years: Year[];

  query: {
    yearId: number;
  } = {
    yearId: -1,
  };

  constructor(private distribution: DistributionService) {
  }

  ngOnInit() {
    this.distribution.getYears()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.years = result.data;
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  getExportation() {

    this.distribution.getDistributionExportationAsCSV(this.query.yearId)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Exportation réussie'], 3000);
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

}
