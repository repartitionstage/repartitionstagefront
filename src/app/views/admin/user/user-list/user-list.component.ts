import {Component, OnInit} from '@angular/core';
import {User} from '../../../../service/user/user';
import {Promotion} from '../../../../service/distribution/models/promotion';
import {Group} from 'src/app/service/distribution/models/group.enum';
import {UserService} from '../../../../service/user/user.service';
import {Result} from '../../../../service/rest-api/result';
import {DistributionService} from '../../../../service/distribution/distribution.service';
import {Router} from '@angular/router';

@Component({
  selector: 'rs-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  Group: Group;
  promotions: Promotion[] = [];

  allUsers: User[] = [];

  users: User[] = [];

  selectedUserId;

  totalAmount: number;
  maxPageId = 1;

  currentPageId = 0;
  pageIdsToShow: number[];

  limit = 10;
  offset = 0;

  constructor(private userService: UserService,
              private distribution: DistributionService,
              private router: Router) {
  }

  ngOnInit() {

    this.distribution.getPromotions()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.promotions = result.data;
          }
        }
      );

    this.userService.getAllUsers(0, 0)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.allUsers = result.data.list;

          }
        }
      );

    this.refresh();
  }

  refresh() {
    this.userService.getAllUsers(this.limit, this.offset + (this.limit * this.currentPageId))
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.users = result.data.list;
            this.totalAmount = result.data.totalAmount;
            this.maxPageId = Math.ceil(this.totalAmount / this.limit) - 1;

            this.pageIdsToShow = [];

            // CALCULATE NAV BAR
            if (this.currentPageId === 0) {
              this.pageIdsToShow = [0];

              if (this.maxPageId >= 1) {
                this.pageIdsToShow.push(1);
                if (this.maxPageId >= 2) {
                  this.pageIdsToShow.push(2);
                }
              }

            } else if (this.currentPageId === this.maxPageId) {
              this.pageIdsToShow = [this.maxPageId];
              if ((this.maxPageId - 1) >= 0) {
                this.pageIdsToShow.unshift(this.maxPageId - 1);
                if ((this.maxPageId - 2) >= 0) {
                  this.pageIdsToShow.unshift(this.maxPageId - 2);
                }
              }

            } else {// > 0 && < this.maxPageId
              this.pageIdsToShow = [this.currentPageId];

              if ((this.currentPageId - 1) >= 0) {
                this.pageIdsToShow.unshift(this.currentPageId - 1);
              }

              if ((this.currentPageId + 1) <= this.maxPageId) {
                this.pageIdsToShow.push(this.currentPageId + 1);
              }

            }


          }
        }
      );
  }

  getPromotionName(id: number): string {
    for (const promotion of this.promotions) {
      if (promotion.id === id) {
        return promotion.name;
      }
    }
  }

  switchPage(id: number) {
    if (id >= 0
      && id <= this.maxPageId
      && id !== this.currentPageId) {
      this.currentPageId = id;
      this.refresh();
    }
  }

  navigateToSelectedProfile() {
    this.router.navigate(['/admin/users/profile/' + this.selectedUserId]);
  }
}
