import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../../../../service/user/user';
import {UserService} from '../../../../service/user/user.service';
import {ActivatedRoute} from '@angular/router';
import {Result} from '../../../../service/rest-api/result';
import {InfoCardComponent} from '../../../utils/info-card/info-card.component';
import {Promotion} from '../../../../service/distribution/models/promotion';
import {DistributionService} from '../../../../service/distribution/distribution.service';

@Component({
  selector: 'rs-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  userId: number;
  user: User;

  promotions: Promotion[];

  edition = false;

  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private distribution: DistributionService) {
  }

  getPromotionName(id: number) {
    for (const promotion of this.promotions) {
      if (promotion.id === id) {
        return promotion.name;
      }
    }
  }

  ngOnInit() {
    this.userId = this.route.snapshot.params['userId'];

    this.distribution.getPromotions()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.promotions = result.data;
            this.refresh();
          } else {
            this.card.error(result.errors);
          }
        }
      );
  }

  refresh() {
    this.userService.getProfileInfos(this.userId)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.user = result.data;
            this.userService.isUserAdmin(this.userId)
              .subscribe(
                (result1: Result) => {
                  if (result.success) {
                    this.user.admin = result1.success && result1.data.admin;
                    this.user.admin = (this.user.admin !== null ? this.user.admin : false);
                  } else {
                    this.card.error(result.errors, 4000);
                  }
                }
              );
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  startEdition() {
    this.edition = true;
  }

  saveEdition() {
    const admin = this.user.admin !== null ? this.user.admin : false;

    this.userService.updateUser(this.user)
      .subscribe(
        (result: Result) => {
          if (result.success) {

            this.userService.setUserAdmin(this.userId, admin)
              .subscribe(
                (result2: Result) => {
                  if (result2.success) {
                    this.edition = false;
                    this.refresh();

                    this.card.success(['Sauvegarde  effectuée !'], 2000);

                  } else {
                    this.card.error(result2.errors, 4000);
                  }
                }
              );
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }
}
