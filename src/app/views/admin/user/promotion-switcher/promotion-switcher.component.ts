import {Component, OnInit, ViewChild} from '@angular/core';
import {Promotion} from '../../../../service/distribution/models/promotion';
import {DistributionService} from '../../../../service/distribution/distribution.service';
import {Result} from '../../../../service/rest-api/result';
import {InfoCardComponent} from '../../../utils/info-card/info-card.component';
import {User} from '../../../../service/user/user';
import {UserService} from '../../../../service/user/user.service';

@Component({
  selector: 'rs-promotion-switcher',
  templateUrl: './promotion-switcher.component.html',
  styleUrls: ['./promotion-switcher.component.scss']
})
export class PromotionSwitcherComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  promotions: Promotion[];
  promotionId: number;

  users: User[];
  selectedIds: number[] = [];

  switchSelectAllBtn = true;

  promotionIdForNotRedoubling: number;

  constructor(private distributionService: DistributionService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.distributionService.getPromotions()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.promotions = result.data;
            this.selectedIds = [];
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  refresh() {
    this.userService.getUsersByPromotion(this.promotionId)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.users = result.data;
            this.users.sort(
              (a: User, b: User) => {
                if (a.lastName === b.lastName) {
                  return a.firstName.localeCompare(b.firstName);
                } else {
                  return a.lastName.localeCompare(b.lastName);
                }
              }
            );
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  switchSelection(id: number) {
    if (this.selectedIds.indexOf(id) !== -1) {
      this.selectedIds.splice(this.selectedIds.indexOf(id), 1);
    } else {
      this.selectedIds.push(id);
    }
  }

  switchSelectAll() {
    this.switchSelectAllBtn = !this.switchSelectAllBtn;
    if (!this.switchSelectAllBtn) {
      this.users.forEach(
        (a: User) => {
          this.selectedIds.push(a.id);
        }
      );
    } else {
      this.selectedIds = [];
    }

  }

  markAsRedoubling() {
    this.userService.setRedoubling(this.selectedIds, true)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Utilisateurs marqués comme redoublants avec succès'], 3000);
            this.refresh();
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  markAsNotRedoubling() {
    this.userService.setRedoubling(this.selectedIds, false)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Utilisateurs marqués comme non redoublants avec succès'], 3000);
            this.refresh();
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  switchNotRedoublingToSelectedPromotion() {
    this.userService.switchNotRedoublingToPromotion(this.promotionId, this.promotionIdForNotRedoubling)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Utilisateurs non redoublants changés de promotions'], 3000);
            this.refresh();
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }
}
