import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PromotionSwitcherComponent} from './promotion-switcher.component';

describe('PromotionSwitcherComponent', () => {
  let component: PromotionSwitcherComponent;
  let fixture: ComponentFixture<PromotionSwitcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PromotionSwitcherComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionSwitcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
