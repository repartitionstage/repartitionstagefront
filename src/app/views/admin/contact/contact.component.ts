import {Component, OnInit} from '@angular/core';
import {ContactService} from '../../../service/contact/contact.service';

@Component({
  selector: 'rs-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  columns = [
    {
      id: 'name',
      name: 'Nom',
      editable: true,
      visible: true,
      width: 20
    },
    {
      id: 'description',
      name: 'Fonction',
      editable: true,
      visible: true,
      width: 40
    },
    {
      id: 'contact',
      name: 'Contact',
      editable: true,
      visible: true,
      width: 40
    }
  ];
  min = 0;
  pas = 0;
  get = ((service) => (limit: number, offset: number) => service.getContacts(limit, offset))(this.contact);
  add = ((service) => (year: any) => service.addContact(year))(this.contact);
  edit = ((service) => (year: any) => service.updateContact(year))(this.contact);
  del = ((service) => (id: number) => service.deleteContact(id))(this.contact);

  constructor(private contact: ContactService) {
  }

  ngOnInit() {
  }

}
