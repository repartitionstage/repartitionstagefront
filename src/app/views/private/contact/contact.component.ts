import {Component, OnInit, ViewChild} from '@angular/core';
import {ContactService} from '../../../service/contact/contact.service';
import {Contact} from '../../../service/contact/contact';
import {Result} from '../../../service/rest-api/result';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';

@Component({
  selector: 'rs-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  contacts: Contact[];

  constructor(private contactService: ContactService) {
  }

  ngOnInit() {
    this.contactService.getContacts()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.contacts = result.data;
            this.contacts.sort((a: Contact, b: Contact) => a.name.localeCompare(b.name));

          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

}
