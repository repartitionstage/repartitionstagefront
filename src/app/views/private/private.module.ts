import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PrivateRoutingModule} from './private-routing.module';
import {ProfileComponent} from './profile/profile.component';
import {PrivateHomeComponent} from './private-home/private-home.component';
import {RsPipeModule} from '../../pipe/rs-pipe.module';
import {RsDirectiveModule} from '../../directive/rs-directive.module';
import {RsUtilsModule} from '../utils/rs-utils.module';
import {FaqComponent} from './faq/faq.component';
import {InternshipsComponent} from './internships/internships.component';
import {ContactComponent} from './contact/contact.component';
import {DocumentComponent} from './document/document.component';
import {FormsModule} from '@angular/forms';
import {AllGardesComponent} from './all-gardes/all-gardes.component';
import {OwnGardesComponent} from './own-gardes/own-gardes.component';
import {CollapseModule} from 'ngx-bootstrap';
import {ExchangeGardesComponent} from './exchange-gardes/exchange-gardes.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    ProfileComponent,
    PrivateHomeComponent,
    FaqComponent,
    InternshipsComponent,
    ContactComponent,
    DocumentComponent,
    AllGardesComponent,
    OwnGardesComponent,
    ExchangeGardesComponent
  ],
  imports: [
    CommonModule,
    PrivateRoutingModule,
    RsDirectiveModule,
    RsPipeModule,
    RsUtilsModule,
    FormsModule,
    CollapseModule,
    FontAwesomeModule
  ]
})
export class PrivateModule {
}
