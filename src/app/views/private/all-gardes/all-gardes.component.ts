import {Component, OnInit, ViewChild} from '@angular/core';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';
import {Garde} from '../../../service/garde/garde';
import {GardeService} from '../../../service/garde/garde.service';
import {Result} from '../../../service/rest-api/result';
import {GardeTerrain} from '../../../service/garde/garde-terrain';
import {TokenService} from '../../../service/token/token.service';

@Component({
  selector: 'rs-all-gardes',
  templateUrl: './all-gardes.component.html',
  styleUrls: ['./all-gardes.component.scss']
})
export class AllGardesComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  terrains: GardeTerrain[];
  gardes: Garde[];

  table: {
    rows: {
      terrain: GardeTerrain,
      attribs: Garde[]
    }[]
  } = {
    rows: []
  };

  baseDate = new Date();
  dates: Date[];

  selfUserId: number;

  holydays: string[] = [];

  constructor(private gardeService: GardeService,
              private tokenService: TokenService) {
  }

  ngOnInit() {
    this.selfUserId = this.tokenService.getAccessTokenPayload().userId;

    while (this.baseDate.getDay() !== 1) { // Back to Monday
      this.baseDate.setDate(this.baseDate.getDate() - 1);
    }

    this.dates = [];
    while (this.dates.length === 0 || this.baseDate.getDay() !== 1) {

      this.dates.push(new Date(this.baseDate.getTime()));

      this.baseDate.setDate(this.baseDate.getDate() + 1);
    }

    const dateStr = this.dates[0].getFullYear() + '-' + (this.dates[0].getMonth() + 1) + '-' + this.dates[0].getDate();
    this.gardeService.getWeekHolydaysByDate(dateStr)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.holydays = result.data;
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );

    this.gardes = [];
    this.gardeService.getTerrains()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.terrains = result.data;
            this.terrains.sort((a, b) => (a.id - b.id));

            this.gardeService.getWeekGardesByDate(dateStr)
              .subscribe(
                (result2: Result) => {
                  if (result2.success) {
                    this.gardes = result2.data;
                    this.genTable();
                  } else {
                    this.card.error(result2.errors, 4000);
                  }
                }
              );

          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );


  }

  genTable() {
    this.table = {
      rows: []
    };

    this.terrains.forEach(
      (terrain: GardeTerrain) => {
        terrain['collapsed'] = true;
        this.table.rows.push({
          terrain: terrain,
          attribs: []
        });
      }
    );

    const getRow = (terrainId) => {
      for (const row of this.table.rows) {
        if (row.terrain.id === terrainId) {
          return row;
        }
      }
    };

    this.gardes.forEach(
      (garde: Garde) => {
        getRow(garde.terrainId).attribs.push(garde);
      }
    );

    this.table.rows.sort((a, b) => a.terrain.id - b.terrain.id);
    this.table.rows.forEach(
      (row) => {
        row.attribs.sort((a, b) => (a.date.localeCompare(b.date)));
      }
    );
  }

  nextWeek() {
    this.baseDate = new Date(this.dates[0].getTime());
    this.baseDate.setDate(this.baseDate.getDate() + 7);
    this.ngOnInit();
  }

  previousWeek() {
    this.baseDate = new Date(this.dates[0].getTime());
    this.baseDate.setDate(this.baseDate.getDate() - 7);
    this.ngOnInit();
  }

  isHolyday(day: number | string) {
    const date = (typeof day === 'number') ? this.dates[day].getFullYear() + '-' + (this.dates[day].getMonth() + 1) + '-' + this.dates[day].getDate() : day;
    return (this.holydays.indexOf(date) !== -1);
  }

}
