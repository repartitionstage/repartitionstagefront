import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AllGardesComponent} from './all-gardes.component';

describe('AllGardesComponent', () => {
  let component: AllGardesComponent;
  let fixture: ComponentFixture<AllGardesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AllGardesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllGardesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
