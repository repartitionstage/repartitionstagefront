import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProfileComponent} from './profile/profile.component';
import {PrivateHomeComponent} from './private-home/private-home.component';
import {AuthGuard} from '../../guard/auth/auth.guard';
import {InternshipsComponent} from './internships/internships.component';
import {ContactComponent} from './contact/contact.component';
import {DocumentComponent} from './document/document.component';
import {OwnGardesComponent} from './own-gardes/own-gardes.component';
import {AllGardesComponent} from './all-gardes/all-gardes.component';

const routes: Routes = [
  {
    path: 'home',
    component: PrivateHomeComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Accueil Privée'
    }
  },
  {
    path: 'distribution',
    canActivate: [AuthGuard],
    loadChildren: './distribution/distribution.module#DistributionModule',
    data: {
      title: '',
      breadcrumbs: 'Répartition'
    }
  },
  {
    path: 'interships',
    canActivate: [AuthGuard],
    component: InternshipsComponent,
    data: {
      title: 'Stages'
    }
  },
  {
    path: 'all-gardes',
    component: AllGardesComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Planning de garde'
    }
  },
  {
    path: 'own-gardes',
    component: OwnGardesComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Mes gardes'
    }
  },
  /*{
    path: 'exchange-gardes',
    component: ExchangeGardesComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Echange de gardes'
    }
  },*/
  {
    path: 'profile/:id',
    canActivate: [AuthGuard],
    component: ProfileComponent,
    data: {
      title: 'Profil'
    }
  },
  {
    path: 'contact',
    canActivate: [AuthGuard],
    component: ContactComponent,
    data: {
      title: 'Contacts importants',
      breadcrumbs: 'Contacts'
    }
  },
  {
    path: 'document',
    canActivate: [AuthGuard],
    component: DocumentComponent,
    data: {
      title: 'Documents importants',
      breadcrumbs: 'Documents'
    }
  },
  {
    path: '*',
    redirectTo: 'home',
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivateRoutingModule {
}
