import {Component, OnInit} from '@angular/core';
import {faArrowsAltH} from '@fortawesome/free-solid-svg-icons';
import {library} from '@fortawesome/fontawesome-svg-core';

@Component({
  selector: 'rs-exchange-gardes',
  templateUrl: './exchange-gardes.component.html',
  styleUrls: ['./exchange-gardes.component.scss']
})
export class ExchangeGardesComponent implements OnInit {

  exchanges: {
    vendorId: number,
    vendorName: string,
    vendorGardeId: number,
    vendorGardeName: string,
    vendorGardeDate: string,
    targetId: number,
    targetGardeId: number,
    targetGardeName: string,
    targetGardeDate: string,
  }[] = [];

  constructor() {
    library.add(faArrowsAltH);
  }

  ngOnInit() {

    this.exchanges.push({
      vendorId: 1,
      vendorName: 'Marvin Charlier',
      vendorGardeId: 2,
      vendorGardeName: 'SAUV 1',
      vendorGardeDate: '2019-09-30',
      targetId: 3,
      targetGardeId: 4,
      targetGardeName: 'SAUV 2',
      targetGardeDate: '2019-09-30',
    });

  }

}
