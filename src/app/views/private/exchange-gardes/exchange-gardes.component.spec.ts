import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ExchangeGardesComponent} from './exchange-gardes.component';

describe('ExchangeGardesComponent', () => {
  let component: ExchangeGardesComponent;
  let fixture: ComponentFixture<ExchangeGardesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExchangeGardesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExchangeGardesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
