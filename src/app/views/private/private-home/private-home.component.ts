import {Component, OnInit} from '@angular/core';
import {Result} from '../../../service/rest-api/result';
import {PageContentService} from '../../../service/page-content/page-content.service';

@Component({
  selector: 'rs-private-home',
  templateUrl: './private-home.component.html',
  styleUrls: ['./private-home.component.scss']
})
export class PrivateHomeComponent implements OnInit {

  htmlContent: string;

  constructor(private homeContentService: PageContentService) {
  }

  ngOnInit() {
    this.homeContentService.getPageContent('private').subscribe(
      (result: Result) => {
        this.htmlContent = result.data;
      }
    );
  }

}
