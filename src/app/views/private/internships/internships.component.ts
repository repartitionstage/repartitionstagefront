import {Component, OnInit, ViewChild} from '@angular/core';
import {DistributionService} from '../../../service/distribution/distribution.service';
import {Result} from '../../../service/rest-api/result';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';
import {TerrainAttributionState} from '../../../service/distribution/models/terrain-attribution-state.enum';

@Component({
  selector: 'rs-internships',
  templateUrl: './internships.component.html',
  styleUrls: ['./internships.component.scss']
})
export class InternshipsComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  content: {
    year: { id: number, name: string },
    promotion: { id: number, name: string },
    terrains: { id: number, name: string, periodOrder: number, beginDate: string, endDate: string, evaluated: boolean }[],
  }[] = [];

  constructor(private distribution: DistributionService) {
  }

  ngOnInit() {
    this.distribution.getTerrainAttributionByUser()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            const data = result.data;
            this.calculateContent(data);
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  getByYearId(id: number) {
    for (const line of this.content) {
      if (line.year.id === id) {
        return line;
      }
    }
  }

  calculateContent(lines: Line[]) {

    for (const line of lines) {
      if (line.state === TerrainAttributionState.ACCEPTED) {
        let contentLine = this.getByYearId(line.yearId);
        if (!contentLine) {
          contentLine = {
            year: {
              id: line.yearId,
              name: line.yearName,
            },
            promotion: {
              id: line.promotionId,
              name: line.promotionName
            },
            terrains: []
          };
          this.content.push(contentLine);
        }

        contentLine.terrains.push(
          {
            id: line.attribId,
            name: line.terrainTypeName,
            periodOrder: line.periodOrder,
            beginDate: line.beginDate,
            endDate: line.endDate,
            evaluated: false
          }
        );

      }
    }

    this.content.forEach((line) => line.terrains.sort((a, b) => (a.periodOrder - b.periodOrder)));

    this.content.sort((a, b) => (a.year.id - b.year.id));
  }

}

class Line {
  yearId: number;
  yearName: string;

  promotionId: number;
  promotionName: string;

  periodId: number;
  periodOrder: number;
  beginDate: string;
  endDate: string;

  terrainTypeId: number;
  terrainTypeName: string;

  attribId: number;
  state: TerrainAttributionState;
}
