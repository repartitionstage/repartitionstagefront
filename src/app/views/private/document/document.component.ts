import {Component, OnInit, ViewChild} from '@angular/core';
import {DocumentService} from '../../../service/document/document.service';
import {Document} from '../../../service/document/document';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';
import {Result} from '../../../service/rest-api/result';

@Component({
  selector: 'rs-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  documents: Document[];

  constructor(private document: DocumentService) {
  }

  ngOnInit() {

    this.document.getDocumentListByPromotion(1)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.documents = result.data;
            this.documents.sort((a: Document, b: Document) => a.name.localeCompare(b.name));
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  downloadDoc(id: number, fileName: string) {
    this.document.downloadDocument(id, fileName)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Téléchargement lancé !']);
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }
}
