import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OwnGardesComponent} from './own-gardes.component';

describe('OwnGardesComponent', () => {
  let component: OwnGardesComponent;
  let fixture: ComponentFixture<OwnGardesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OwnGardesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnGardesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
