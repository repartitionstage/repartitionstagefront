import {Component, OnInit, ViewChild} from '@angular/core';
import {GardeService} from '../../../service/garde/garde.service';
import {Garde} from '../../../service/garde/garde';
import {Result} from '../../../service/rest-api/result';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';

@Component({
  selector: 'rs-own-gardes',
  templateUrl: './own-gardes.component.html',
  styleUrls: ['./own-gardes.component.scss']
})
export class OwnGardesComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  planning: { id: number, name: string, gardes: Garde[] }[];
  gardes: Garde[];

  constructor(private gardeService: GardeService) {
  }

  getYear(id: number) {
    for(const year of this.planning) {
      if(year.id === id) {
        return year;
      }
    }
    return false;
  }

  ngOnInit() {
    this.gardeService.getGardesByUserId('self')
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.gardes = result.data;

            this.planning = [];

            for (const garde of result.data) {
              let year = this.getYear(garde.yearId);

              if(!year){
                year = {id: garde.yearId, name: garde.yearName, gardes: []};
                this.planning.push(year);
              }

                year.gardes.push(garde);
            }

          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

}
