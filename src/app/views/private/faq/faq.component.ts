import {Component, OnInit} from '@angular/core';
import {Result} from '../../../service/rest-api/result';
import {PageContentService} from '../../../service/page-content/page-content.service';

@Component({
  selector: 'rs-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  htmlContent: string;

  constructor(private pageContentService: PageContentService) {
  }

  ngOnInit() {
    this.pageContentService.getPageContent('faq').subscribe(
      (result: Result) => {
        this.htmlContent = result.data;
      }
    );
  }

}
