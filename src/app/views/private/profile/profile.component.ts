import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../../../service/user/user.service';
import {ActivatedRoute} from '@angular/router';
import {Result} from '../../../service/rest-api/result';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';
import {User} from '../../../service/user/user';
import {Promotion} from '../../../service/distribution/models/promotion';
import {DistributionService} from '../../../service/distribution/distribution.service';
import {EncryptService} from '../../../service/encrypt/encrypt.service';

@Component({
  selector: 'rs-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  userId: number;
  user: User;

  promotions: Promotion[];

  change = {
    password: '',
    confirmPassword: '',
  };
  submitted = false;

  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private distribution: DistributionService) {
  }

  getPromotionName(id: number) {
    for (const promotion of this.promotions) {
      if (promotion.id === id) {
        return promotion.name;
      }
    }
  }

  ngOnInit() {
    this.userId = this.route.snapshot.params['id'];

    this.distribution.getPromotions()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.promotions = result.data;
            this.refresh();
          } else {
            this.card.error(result.errors);
          }
        }
      );
  }

  refresh() {
    this.userService.getProfileInfos('self')
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.user = result.data;
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  changePassword() {
    this.submitted = true;
    if (this.change.confirmPassword !== this.change.password) {
      this.submitted = false;
      return;
    }

    this.userService.updatePassword({password: EncryptService.encryptForBackEndPassword(this.change.password)})
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Mot de passe modifié avec succès']);
          } else {
            this.card.error(result.errors, 4000);
          }
          this.change.password = '';
          this.change.confirmPassword = '';
          this.submitted = true;
        }
      );

  }
}
