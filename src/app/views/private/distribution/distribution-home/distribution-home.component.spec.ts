import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DistributionHomeComponent} from './distribution-home.component';

describe('DistributionHomeComponent', () => {
  let component: DistributionHomeComponent;
  let fixture: ComponentFixture<DistributionHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DistributionHomeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributionHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should post', () => {
    expect(component).toBeTruthy();
  });
});
