import {Component, OnInit} from '@angular/core';
import {DistributionService} from '../../../../service/distribution/distribution.service';
import {Result} from '../../../../service/rest-api/result';
import {Phase} from '../../../../service/distribution/models/phase';

@Component({
  selector: 'rs-distribution-home',
  templateUrl: './distribution-home.component.html',
  styleUrls: ['./distribution-home.component.scss']
})
export class DistributionHomeComponent implements OnInit {

  activatedPhase: Phase;
  nextPhaseArrayId;
  phases: Phase[];

  constructor(private distributionService: DistributionService) {
  }

  ngOnInit() {
    this.distributionService.getUserPhases().subscribe(
      (result: Result) => {
        if (result.success) {
          this.phases = result.data;
          this.phases.sort((a, b) => (a.order - b.order));

          for (let i = 0; i < this.phases.length; i++) {
            if (this.phases[i].active) {
              this.activatedPhase = this.phases[i];
              this.nextPhaseArrayId = (i + 1) < this.phases.length ? (i + 1) : -1;
              break;
            }
          }
        }
      }
    );
  }

}
