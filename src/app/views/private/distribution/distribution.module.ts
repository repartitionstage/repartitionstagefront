import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DistributionHomeComponent} from './distribution-home/distribution-home.component';
import {DistributionRoutingModule} from './distribution-routing.module';
import {FormsModule} from '@angular/forms';
import {RsDirectiveModule} from '../../../directive/rs-directive.module';
import {PrivateModule} from '../private.module';
import {DistributionGroupComponent} from './distribution-group/distribution-group.component';
import {UiSwitchModule} from 'ngx-ui-switch';
import {DistributionMessageComponent} from './distribution-message/distribution-message.component';
import {RsPipeModule} from '../../../pipe/rs-pipe.module';
import {DistributionTerrainComponent} from './distribution-terrain/distribution-terrain.component';
import {RsUtilsModule} from '../../utils/rs-utils.module';
import {AccordionModule} from 'angular-admin-lte';
import {CollapseModule} from 'ngx-bootstrap';
import {DistributionSpreadingComponent} from './distribution-spreading/distribution-spreading.component';

@NgModule({
  declarations: [
    DistributionHomeComponent,
    DistributionGroupComponent,
    DistributionMessageComponent,
    DistributionTerrainComponent,
    DistributionSpreadingComponent],
  imports: [
    CommonModule,
    FormsModule,
    DistributionRoutingModule,
    RsDirectiveModule,
    PrivateModule,
    UiSwitchModule,
    RsPipeModule,
    RsUtilsModule,
    AccordionModule,
    CollapseModule
  ]
})
export class DistributionModule {
}

