import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DistributionTerrainComponent} from './distribution-terrain.component';

describe('DistributionTerrainComponent', () => {
  let component: DistributionTerrainComponent;
  let fixture: ComponentFixture<DistributionTerrainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DistributionTerrainComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributionTerrainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
