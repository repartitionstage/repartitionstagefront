import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DistributionSpreadingComponent} from './distribution-spreading.component';

describe('DistributionSpreadingComponent', () => {
  let component: DistributionSpreadingComponent;
  let fixture: ComponentFixture<DistributionSpreadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DistributionSpreadingComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributionSpreadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
