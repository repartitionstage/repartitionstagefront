import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Period} from '../../../../service/distribution/models/period';
import {TerrainType} from '../../../../service/distribution/models/terrain-type';
import {Terrain} from '../../../../service/distribution/models/terrain';
import {TerrainAttribution} from '../../../../service/distribution/models/terrain-attribution';
import {DistributionService} from '../../../../service/distribution/distribution.service';
import {Phase} from '../../../../service/distribution/models/phase';
import {Result} from '../../../../service/rest-api/result';
import {TerrainAttributionState} from '../../../../service/distribution/models/terrain-attribution-state.enum';
import {UserService} from '../../../../service/user/user.service';
import {Router} from '@angular/router';
import {KeyValue} from '@angular/common';
import {TokenService} from '../../../../service/token/token.service';
import {Location} from '../../../../service/distribution/models/location';
import {InfoCardComponent} from '../../../utils/info-card/info-card.component';

@Component({
  selector: 'rs-distribution-spreading',
  templateUrl: './distribution-spreading.component.html',
  styleUrls: ['./distribution-spreading.component.scss']
})
export class DistributionSpreadingComponent implements OnInit {

  TerrainAttributionState = TerrainAttributionState;

  @ViewChild('card') card: InfoCardComponent;
  @Input() phase: Phase;

  table: Table = new Table();
  locations: Map<number, Location>;
  periods: Map<number, Period>;
  terrainTypes: Map<number, TerrainType>;
  terrains: Map<number, Terrain>;
  attributions: TerrainAttribution[];
  dataCalculated = false;

  userId: number;

  blockedPeriods: number[];

  constructor(private distributionService: DistributionService,
              private userService: UserService,
              private router: Router,
              private tokenService: TokenService) {
  }

  static countStatedAttribution(attribs: TerrainAttribution[], state: TerrainAttributionState): number {
    let i = 0;
    for (const attrib of attribs) {
      if (attrib.state === state) {
        i++;
      }
    }
    return i;
  }

  static arrayToMap<A, B>(array: B[], ft: (B) => A): Map<A, B> {
    const map = new Map<A, B>();
    array.forEach(
      (value) => {
        map.set(ft(value), value);
      }
    );
    return map;
  }

  sorter(a: KeyValue<number, Row>, b: KeyValue<number, Row>): number {
    return a.value.headerCell.terrainType.name.localeCompare(b.value.headerCell.terrainType.name);
  }

  ngOnInit() {
    this.refresh();
  }

  refresh() {
    this.dataCalculated = false;

    if (this.tokenService.getAccessTokenPayload()) {
      this.userId = this.tokenService.getAccessTokenPayload().userId;
    } else {
      this.tokenService.refreshAccessToken()
        .subscribe(
          () => {
            this.userId = this.tokenService.getAccessTokenPayload().userId;
          }
        );
    }

    this.distributionService.getTerrainAttributionByPhase(this.phase.id)
      .subscribe(
        (result: Result) => {
          if (result.success) {

            this.locations = DistributionSpreadingComponent.arrayToMap(result.data.locations, (location: Location) => location.id);
            this.periods = DistributionSpreadingComponent.arrayToMap(result.data.periods, (period: Period) => period.id);
            this.terrainTypes = DistributionSpreadingComponent.arrayToMap(result.data.terrainTypes, (tt: TerrainType) => tt.id);
            this.terrains = DistributionSpreadingComponent.arrayToMap(result.data.terrains, (t: Terrain) => t.id);
            this.attributions = result.data.attributions;

            this.calculateData();
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );

    this.distributionService.getBlockedPeriods()
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.blockedPeriods = result.data.map((period) => period.id);

            this.calculateData();
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );

  }

  calculateData() {
    if (this.periods && this.terrainTypes && this.terrains && this.attributions
      && this.blockedPeriods) {

      this.terrains.forEach(
        (terrain) => {
          let row: Row;
          if (!this.table.rows.has(terrain.terrainTypeId)) {
            this.table.rows.set(terrain.terrainTypeId, row = new Row());

            row.headerCell = new HeaderCell();
            row.headerCell.terrainType = this.terrainTypes.get(terrain.terrainTypeId);
            row.headerCell.location = this.locations.get(row.headerCell.terrainType.locationId);

          } else {
            row = this.table.rows.get(terrain.terrainTypeId);
          }

          let cell: TerrainCell;
          if (row.terrainCells.has(terrain.periodId)) {
            cell = this.genTerrainCell(terrain, row.terrainCells.get(terrain.periodId));
          } else {
            cell = this.genTerrainCell(terrain);
          }
          row.terrainCells.set(terrain.periodId, cell);
        }
      );
      this.dataCalculated = true;
    }
  }

  getAttributionsByTerrain(terrainId: number): TerrainAttribution[] {
    const attrib: TerrainAttribution[] = [];

    for (const ta of this.attributions) {
      if (ta.terrainId === terrainId) {
        attrib.push(ta);
      }
    }

    attrib.sort((a, b) => {
      if (a.lastName === b.lastName) {
        return a.firstName.localeCompare(b.firstName);
      } else {
        return a.lastName.localeCompare(b.lastName);
      }
    });

    return attrib;
  }

  genTerrainCell(terrain: Terrain, oldCell?: TerrainCell): TerrainCell {
    const cell = oldCell ? oldCell : new TerrainCell();
    cell.terrain = terrain;
    cell.attribution = this.getAttributionsByTerrain(terrain.id);

    cell.collapsable = true;
    cell.disabled = false;
    cell.leavable = false;
    cell.positioned = false;

    if (cell.attribution.length >= cell.terrain.maxAmountPlaces) {
      if (DistributionSpreadingComponent.countStatedAttribution(cell.attribution, TerrainAttributionState.ACCEPTED) === cell.attribution.length
        && cell.attribution.length > 0) {
        cell.borderColor = '#28a745';
        cell.disabled = true;
      } else {
        if (cell.attribution.length === cell.terrain.maxAmountPlaces) {
          if (cell.terrain.maxAmountPlaces === 0) {
            cell.borderColor = '#a3a7a1';
            cell.disabled = true;
            cell.collapsable = false;
          } else {
            cell.borderColor = '#f39c12';
          }
        } else {
          cell.borderColor = '#f33231';
        }
      }
    } else {
      cell.borderColor = '#f39c12';
    }

    for (const attrib of cell.attribution) {
      if (attrib.userId === this.userId) {
        cell.positioned = true;
        if (attrib.state === TerrainAttributionState.WAITING || attrib.state === TerrainAttributionState.NOT_PRIORITY) {
          cell.leavable = true;
        } else if (attrib.state === TerrainAttributionState.ACCEPTED) {
          cell.disabled = true;
        }
        break;
      }
    }

    return cell;
  }

  join(terrainId: number) {
    const ta = new TerrainAttribution();
    ta.userId = this.userId;
    ta.terrainId = terrainId;
    this.distributionService.addTerrainAttributionOnDistribution(ta)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.refresh();
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  leave(attributionId: number) {
    this.distributionService.deleteTerrainAttributionOnDistribution(attributionId)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.refresh();
          } else {
            this.card.error(result.errors, 4000);
          }
        }
      );
  }

  swapJoin(cell: TerrainCell) {
    for (const attrib of cell.attribution) {
      if (attrib.userId === this.userId) {
        this.leave(attrib.id);
        return;
      }
    }
    this.join(cell.terrain.id);
  }

}

class Table {
  rows: Map<number, Row> = new Map<number, Row>(); // Map<TerrainTypeId, Row>
}

class Row {
  headerCell: HeaderCell;
  terrainCells: Map<number, TerrainCell> = new Map<number, TerrainCell>(); // Map<PeriodId, Row>
}

class Cell {
  borderColor = 'default';
  collapsed = true;
  collapsable = true;
}

class HeaderCell extends Cell {
  terrainType: TerrainType;
  location: Location;
}

class TerrainCell extends Cell {
  terrain: Terrain;
  attribution: TerrainAttribution[] = [];

  disabled = false;
  leavable = false;
  positioned = false;
}

