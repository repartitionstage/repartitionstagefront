import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'rs-distribution-message',
  templateUrl: './distribution-message.component.html',
  styleUrls: ['./distribution-message.component.scss']
})
export class DistributionMessageComponent implements OnInit {

  @Input() phase: any;

  constructor() {
  }

  ngOnInit() {
  }

}
