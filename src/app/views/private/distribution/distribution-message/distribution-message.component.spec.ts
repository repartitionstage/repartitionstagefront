import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DistributionMessageComponent} from './distribution-message.component';

describe('DistributionMessageComponent', () => {
  let component: DistributionMessageComponent;
  let fixture: ComponentFixture<DistributionMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DistributionMessageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributionMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
