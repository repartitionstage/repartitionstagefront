import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DistributionGroupComponent} from './distribution-group.component';

describe('DistributionGroupComponent', () => {
  let component: DistributionGroupComponent;
  let fixture: ComponentFixture<DistributionGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DistributionGroupComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributionGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
