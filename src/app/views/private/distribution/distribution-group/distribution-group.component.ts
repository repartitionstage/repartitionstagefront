import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {UiSwitchComponent} from 'ngx-ui-switch';
import {UserService} from '../../../../service/user/user.service';
import {Result} from '../../../../service/rest-api/result';
import {InfoCardComponent} from '../../../utils/info-card/info-card.component';
import {Phase} from '../../../../service/distribution/models/phase';

@Component({
  selector: 'rs-distribution-group',
  templateUrl: './distribution-group.component.html',
  styleUrls: ['./distribution-group.component.scss']
})
export class DistributionGroupComponent implements OnInit {

  Math = Math;
  @ViewChild('groupSwitcher') groupSwitcher: UiSwitchComponent;

  @ViewChild('card') card: InfoCardComponent;

  @Input() phase: Phase;

  redoubling: boolean;
  groupId: number;
  groupBlocked = true;

  groupA: { firstName: string, lastName: string, group: number }[];
  groupB: { firstName: string, lastName: string, group: number }[];

  users: { firstName: string, lastName: string, group: number }[];

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    console.dir(this.phase);
    this.userService.getProfileInfos('self')
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.groupId = result.data.group;
            this.groupBlocked = Boolean(result.data.groupBlocked);
            this.redoubling = Boolean(result.data.redoubling);
            this.configSwitcher();
          }
        }
      );
    this.refreshUsers();
  }

  refreshUsers() {
    this.userService.getUsersByUserPromotion().subscribe(
      (result: Result) => {
        if (result.success) {
          this.users = result.data;
          this.filterUsers();
        }
      }
    );
  }

  filterUsers() {
    const sort = (user1: { firstName: string, lastName: string, group: number }, user2: { firstName: string, lastName: string, group: number }): number => {
      if (user1.lastName !== user2.lastName) {
        return user1.lastName.localeCompare(user2.lastName);
      } else {
        return user1.firstName.localeCompare(user2.firstName);
      }
    };

    this.users.sort(sort);
    this.groupA = this.users.filter(user => (user.group === 2));
    let index = 1;
    this.groupA.forEach(
      (user) => {
        user['showedId'] = index++;
      }
    );
    this.groupB = this.users.filter(user => (user.group === 3));
    index = 1;
    this.groupB.forEach(
      (user) => {
        user['showedId'] = index++;
      }
    );
  }

  configSwitcher() {
    let checkedLabel = '';
    let uncheckedLabel = '';

    if (this.groupId === 1) { // NO-GROUP
      uncheckedLabel = 'Cliquer pour choisir';
      this.groupSwitcher.checked = false;
      this.groupSwitcher.color = '#bfc3c2';

    } else if (this.groupId === 2 || this.groupId === 3) {
      uncheckedLabel = 'Groupe A';
      checkedLabel = 'Groupe B';
      this.groupSwitcher.checked = this.groupId === 3;

      this.groupSwitcher.color = '#8BBBFF';
      this.groupSwitcher.defaultBgColor = '#FFDB67';
    }

    this.groupSwitcher.checkedLabel = checkedLabel;
    this.groupSwitcher.uncheckedLabel = uncheckedLabel;
  }

  swapGroup() {
    if (this.groupId === 1) {
      this.groupId = 2;
      this.configSwitcher();
    } else {
      this.groupId = this.groupSwitcher.checked === true ? 3 : 2;
      this.userService.updateGroup(this.groupId)
        .subscribe(
          (result: Result) => {
            if (result.success) {
              this.refreshUsers();
            } else {
              this.card.error(result.errors, 2000);
            }
          }
        );
    }
  }
}
