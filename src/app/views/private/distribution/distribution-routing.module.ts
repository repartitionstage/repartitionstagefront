import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../../../guard/auth/auth.guard';
import {DistributionHomeComponent} from './distribution-home/distribution-home.component';


const routes: Routes = [
  {
    path: '',
    component: DistributionHomeComponent,
    canActivate: [AuthGuard],
    children: []
  },
  {
    path: '*',
    redirectTo: '',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DistributionRoutingModule {
}
