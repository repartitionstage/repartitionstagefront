import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PublicRoutingModule} from './public-routing.module';
import {HomeComponent} from './home/home.component';
import {RsDirectiveModule} from '../../directive/rs-directive.module';
import {RsPipeModule} from '../../pipe/rs-pipe.module';
import {RsUtilsModule} from '../utils/rs-utils.module';
import {MarvinComponent} from './marvin/marvin.component';
import {BnpComponent} from './partners/bnp/bnp.component';
import {MacsfComponent} from './partners/macsf/macsf.component';
import {CguComponent} from './cgu/cgu.component';

@NgModule({
  declarations: [
    HomeComponent,
    MarvinComponent,
    BnpComponent,
    MacsfComponent,
    CguComponent
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    RsDirectiveModule,
    RsPipeModule,
    RsUtilsModule
  ],
  providers: [
  ]
})
export class PublicModule {
}
