import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MacsfComponent} from './macsf.component';

describe('MacsfComponent', () => {
  let component: MacsfComponent;
  let fixture: ComponentFixture<MacsfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MacsfComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MacsfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
