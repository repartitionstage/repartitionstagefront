import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BnpComponent} from './bnp.component';

describe('BnpComponent', () => {
  let component: BnpComponent;
  let fixture: ComponentFixture<BnpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BnpComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BnpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
