import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'rs-marvin',
  templateUrl: './marvin.component.html',
  styleUrls: ['./marvin.component.scss']
})
export class MarvinComponent implements OnInit {

  maxId = 7;

  photoId;

  constructor() {
  }

  ngOnInit() {
    this.refresh();
  }

  refresh() {
    const newId = Math.floor(Math.random() * this.maxId);

    if (newId === this.photoId) {
      this.refresh();
    } else {
      this.photoId = newId;
    }
  }
}

