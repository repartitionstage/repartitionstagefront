import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {MarvinComponent} from './marvin/marvin.component';
import {BnpComponent} from './partners/bnp/bnp.component';
import {MacsfComponent} from './partners/macsf/macsf.component';
import {CguComponent} from './cgu/cgu.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    data: {
      title: 'Accueil',
      description: 'Page d\'accueil du site',
      breadcrumbs: 'Accueil'
    }
  },
  {
    path: 'partners',
    data: {
      breadcrumbs: 'Partenaires'
    },
    children: [
      {
        path: 'bnp',
        component: BnpComponent,
        data: {
          title: 'BNP Paribas',
          description: '',
          breadcrumbs: 'BNP Paribas'
        }
      },
      {
        path: 'macsf',
        component: MacsfComponent,
        data: {
          title: 'MACSF',
          description: '',
          breadcrumbs: 'MACSF'
        }
      }
    ]
  },
  {
    path: 'cgu',
    component: CguComponent,
    data: {
      title: 'CGU',
      description: 'Conditions générales d\'utilisations',
      breadcrumbs: 'CGU'
    }
  },
  {
    path: 'marvin',
    component: MarvinComponent,
    data: {
      title: 'Histoire de Marvin',
      description: 'a.k.a. le danseur',
      breadcrumbs: 'Photo Marv'
    }
  },
  {
    path: '*',
    redirectTo: 'home',
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule {
}
