import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../../service/auth/auth.service';
import {Result} from '../../../service/rest-api/result';
import {Router} from '@angular/router';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';

@Component({
  selector: 'rs-request-password',
  templateUrl: './request-password.component.html',
  styleUrls: ['./request-password.component.scss']
})
export class RequestPasswordComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  submitted: false;
  data: {
    email: string
  } = {email: ''};

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
  }

  requestPassword() {
    this.authService.requestPassword(this.data.email).subscribe(
      (result: Result) => {
        if (result.success) {
          this.card.success(['Un mail vous a été envoyé'], 2000,
            () => {
              this.router.navigate(['/auth/login']);
            });
        } else {
          this.card.error(result.errors, 4000);
        }
      }
    );
  }
}
