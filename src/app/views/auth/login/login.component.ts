import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../../service/auth/auth.service';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {EncryptService} from '../../../service/encrypt/encrypt.service';
import {Result} from '../../../service/rest-api/result';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';

@Component({
  selector: 'rs-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  redirectDelay = 500;

  @ViewChild('card') card: InfoCardComponent;

  user: { email: string, password: string, rememberMe: boolean } = {
    email: '',
    password: '',
    rememberMe: false
  };
  submitted = false;

  constructor(
    private cookieService: CookieService,
    private router: Router,
    private authService: AuthService
  ) {

  }

  ngOnInit() {
    if (Boolean(this.cookieService.get('a'))) {
      this.user.email = EncryptService.localDecryptPassword(this.cookieService.get('b'));
      this.user.password = EncryptService.localDecryptPassword(this.cookieService.get('c'));
      this.user.rememberMe = true;
    }
  }

  login() {
    this.card.hide();

    this.submitted = true;

    this.authService.login(this.user.email, EncryptService.encryptForBackEndPassword(this.user.password))
      .subscribe(
        (result: Result) => {
          this.submitted = false;

          if (result.success) {
            if (this.user.rememberMe) {
              this.cookieService.set('a', true.toString());
              this.cookieService.set('b', EncryptService.localEncryptData(this.user.email));
              this.cookieService.set('c', EncryptService.localEncryptData(this.user.password));
            } else {
              this.cookieService.delete('a');
              this.cookieService.delete('b');
              this.cookieService.delete('c');
            }

            this.card.success(['Connexion réussie'], this.redirectDelay,
              () => {
                this.router.navigate([this.authService.redirectURL]);
                this.authService.redirectURL = '';
              });

          } else {
            this.card.error(result.errors, 5000);
          }
        },
        (err) => {
          console.error(err);
        });


  }
}
