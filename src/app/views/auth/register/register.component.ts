import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../../service/auth/auth.service';
import {RegistrationRequest} from '../../../service/auth/registration-request';
import {EncryptService} from '../../../service/encrypt/encrypt.service';
import {Router} from '@angular/router';
import {Result} from '../../../service/rest-api/result';
import {DistributionService} from '../../../service/distribution/distribution.service';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';

@Component({
  selector: 'rs-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  submitted = false;
  @ViewChild('card') card: InfoCardComponent;

  user = {
    firstName: '',
    lastName: '',
    email: '',
    studentNumber: null,
    phone: null,
    promotion: undefined,
    password: '',
    confirmPassword: ''
  };

  promotions: {
    id: number,
    name: string,
  }[];

  constructor(
    private authService: AuthService,
    private distributionService: DistributionService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.distributionService.getPromotions(true)
      .subscribe(
        (result: Result) => {
          if (result && result.data !== undefined && Array.isArray(result.data) && result.data.length > 0) {
            this.promotions = result.data;
          } else {
            this.card.error(['Inscription impossible pour le moment. Pas d\'inscription ouverte.'], 2000,
              () => {
                this.router.navigate(['']);
              });
          }
        }
      );
  }

  register() {
    this.submitted = true;
    if (this.user.confirmPassword !== this.user.password) {
      this.submitted = false;
      return;
    }

    const request = new RegistrationRequest();
    request.firstName = this.user.firstName;
    request.lastName = this.user.lastName;
    request.email = this.user.email;
    request.studentNumber = this.user.studentNumber;
    request.phone = this.user.phone;
    request.promotionId = this.user.promotion;
    request.password = EncryptService.encryptForBackEndPassword(this.user.password);

    this.authService.register(request)
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Inscription réussie !', 'Redirection en cours...'], 2000,
              () => {
                this.router.navigate(['/auth/login']);
              });
          } else {
            this.card.error(result.errors, 4000);
          }
          this.submitted = false;
        }
      );

  }
}
