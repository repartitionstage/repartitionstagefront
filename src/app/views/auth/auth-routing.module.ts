import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {LogoutComponent} from './logout/logout.component';
import {AuthGuard} from '../../guard/auth/auth.guard';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {RequestPasswordComponent} from './request-password/request-password.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    data: {
      customLayout: true
    }
  },
  {
    path: 'logout',
    component: LogoutComponent,
    canActivate: [AuthGuard],
    data: {
      customLayout: true
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      customLayout: true
    }
  },
  {
    path: 'request-password',
    component: RequestPasswordComponent,
    data: {
      customLayout: true
    }
  },
  {
    path: 'reset-password/:token',
    component: ResetPasswordComponent,
    data: {
      customLayout: true
    }
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
