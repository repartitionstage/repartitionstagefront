import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../../service/auth/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EncryptService} from '../../../service/encrypt/encrypt.service';
import {Result} from '../../../service/rest-api/result';
import {InfoCardComponent} from '../../utils/info-card/info-card.component';

@Component({
  selector: 'rs-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  @ViewChild('card') card: InfoCardComponent;

  data: {
    token?: string;
    password?: string
  } = {
    password: ''
  };
  submitted = false;

  constructor(private router: Router,
              private authService: AuthService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.data.token = this.route.snapshot.params.token;
    if (this.data.token === undefined) {
      this.router.navigate(['/']);
      return;
    }
    this.authService.isRPTokenValid(this.data.token).subscribe(
      (result: Result) => {
        if (!result.success) {
          this.card.error(['Token invalide.', 'Redirection en cours...'], 2000,
            () => {
              this.router.navigate(['/']);
            });
        }
      }
    );
  }

  resetPassword() {
    this.authService.resetPassword(this.data.token, EncryptService.encryptForBackEndPassword(this.data.password))
      .subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(['Réinitialisation réussie', 'Redirection en cours...'], 2000,
              () => {
                this.router.navigate(['/auth/login']);
              });
          } else {
            this.card.error(result.errors, 2000,
              () => {
                this.router.navigate(['/auth/login']);
              });
          }
        }
      );
  }
}
