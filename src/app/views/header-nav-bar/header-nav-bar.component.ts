import {Component, OnInit} from '@angular/core';
import {UserService} from '../../service/user/user.service';
import {Result} from '../../service/rest-api/result';
import {TokenService} from '../../service/token/token.service';

@Component({
  selector: 'rs-header-nav-bar',
  templateUrl: './header-nav-bar.component.html',
  styleUrls: ['./header-nav-bar.component.scss']
})
export class HeaderNavBarComponent implements OnInit {

  user: {
    id: number,
    firstName: string,
    lastName: string,
  };

  constructor(private userService: UserService, private tokenService: TokenService) {
  }

  ngOnInit() {
    this.tokenService.isConnect().subscribe(
      (connect: boolean) => {
        if (connect) {
          this.userService.getHeaderInfos().subscribe(
            (result: Result) => {
              if (result.success) {
                this.user = result.data;
              }
            }
          );
        }
      }
    );
  }

}
