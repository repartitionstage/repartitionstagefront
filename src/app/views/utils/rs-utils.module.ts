import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfirmationCardComponent} from './confirmation-card/confirmation-card.component';
import {EditableTableComponent} from './editable-table/editable-table.component';
import {OrderedEditableTableComponent} from './ordered-editable-table/ordered-editable-table.component';
import {InfoCardComponent} from './info-card/info-card.component';
import {FormsModule} from '@angular/forms';
import {DragulaModule} from 'ng2-dragula';
import {LoadingComponent} from './loading/loading.component';
import {StepProgressBarComponent} from './step-progress-bar/step-progress-bar.component';
import {PageComponent} from './page/page.component';
import {RsPipeModule} from '../../pipe/rs-pipe.module';

@NgModule({
  declarations: [
    ConfirmationCardComponent,
    EditableTableComponent,
    OrderedEditableTableComponent,
    InfoCardComponent,
    LoadingComponent,
    StepProgressBarComponent,
    PageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DragulaModule,
    RsPipeModule
  ],
  exports: [
    ConfirmationCardComponent,
    EditableTableComponent,
    OrderedEditableTableComponent,
    InfoCardComponent,
    LoadingComponent,
    StepProgressBarComponent,
    PageComponent
  ]
})
export class RsUtilsModule {
}
