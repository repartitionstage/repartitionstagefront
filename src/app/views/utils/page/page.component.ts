import {Component, Input, OnInit} from '@angular/core';
import {PageContentService} from '../../../service/page-content/page-content.service';
import {Result} from '../../../service/rest-api/result';

@Component({
  selector: 'rs-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  @Input() name;

  htmlContent: string;

  constructor(private pageContentService: PageContentService) {
  }

  ngOnInit() {
    this.pageContentService.getPageContent(this.name).subscribe(
      (result: Result) => {
        this.htmlContent = result.data.htmlContent;
      }
    );
  }
}
