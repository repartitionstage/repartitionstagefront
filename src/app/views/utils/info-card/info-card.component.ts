import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'rs-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.scss']
})
export class InfoCardComponent implements OnInit {

  successes: string[];
  infos: string[];
  errors: string[];
  warnings: string[];

  constructor() {
  }

  ngOnInit() {
  }

  success(successes: string[], time?: number, callback?): void {
    this.successes = successes;

    if (time && time > 0) {
      setTimeout(
        () => {
          this.successes = undefined;
          if (callback) {
            callback();
          }
        }, time
      );
    }
  }

  info(infos: string[], time?: number, callback?): void {
    this.infos = infos;

    if (time && time > 0) {
      setTimeout(
        () => {
          this.infos = undefined;
          if (callback) {
            callback();
          }
        }, time
      );
    }
  }

  warning(warnings: string[], time?: number, callback?): void {
    this.warnings = warnings;

    if (time && time > 0) {
      setTimeout(
        () => {
          this.warnings = undefined;
          if (callback) {
            callback();
          }
        }, time
      );
    }
  }

  error(errors: string[], time?: number, callback?): void {
    this.errors = errors;

    if (time && time > 0) {
      setTimeout(
        () => {
          this.errors = undefined;
          if (callback) {
            callback();
          }
        }, time
      );
    }
  }

  hide() {
    this.successes = [];
    this.infos = [];
    this.warnings = [];
    this.errors = [];
  }
}
