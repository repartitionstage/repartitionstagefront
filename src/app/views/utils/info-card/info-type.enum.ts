export class InfoType {
  static INFO = new InfoType('alert-info');
  static WARNING = new InfoType('alert-warning');
  static ERROR = new InfoType('alert-danger');

  className: string;

  private constructor(className: string) {
    this.className = className;
  }
}
