import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'rs-step-progress-bar',
  templateUrl: './step-progress-bar.component.html',
  styleUrls: ['./step-progress-bar.component.scss']
})
export class StepProgressBarComponent implements OnInit {

  @Input() steps: {
    name: string,
    desc?: string,
    active?: false
    selected?: false,
  }[];

  constructor() {
  }

  ngOnInit() {
  }

}
