import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Result} from '../../../service/rest-api/result';
import {InfoCardComponent} from '../info-card/info-card.component';

@Component({
  selector: 'rs-editable-table',
  templateUrl: './editable-table.component.html',
  styleUrls: ['./editable-table.component.scss']
})
export class EditableTableComponent implements OnInit {

  @Input() columns: {
    id: string,
    name: string;
    editType?: string;
    editable: boolean,
    visible: boolean,
    width: number
  }[];

  @Input() add: Function;
  @Input() edit: Function;
  @Input() del: Function;
  @Input() get: Function;
  @Input() min: number;
  @Input() pas: number;
  page = 0;

  @ViewChild('card') card: InfoCardComponent;

  resultLines = [];

  addedRow = {};
  showAddForm = true;
  showAddConfirmation = false;
  confirmAddData = {
    title: 'Confirmer l\'ajout ?'
  };

  editedRow;
  showEditBtn = true;
  showEditConfirmation = false;
  confirmEditData = {
    title: 'Voulez-vous sauvegarder ?'
  };

  deletedRow;
  showDeleteBtn = true;
  showDelConfirmation = false;
  confirmDeleteData = {
    title: 'Confirmer la suppression ?',
  };


  constructor() {
  }

  ngOnInit() {
    this.refreshTable();
  }

  refreshTable() {
    this.get(this.min * this.page, this.pas).subscribe(
      (result: Result) => {
        if (result.success) {
          this.resultLines = result.data;
        } else {
          this.card.error(result.errors, 2000);
        }
      }
    );
  }

  addElement($event: any) {
    if ($event) {
      this.add(this.addedRow).subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(result.messages, 2000);
          } else {
            this.card.error(result.errors, 2000);
          }
          this.setBasicState();
          this.refreshTable();
        }
      );
    } else {
      this.setBasicState();
    }
  }

  editElement($event: any) {
    if ($event) {
      this.edit(this.editedRow).subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(result.messages, 2000);
          } else {
            this.card.error(result.errors, 2000);
          }
          this.setBasicState();
          this.refreshTable();
        }
      );
    } else {
      this.setBasicState();
      this.refreshTable();

    }
  }

  delElement($event: any) {
    if ($event) {
      this.del(this.deletedRow.id).subscribe(
        (result: Result) => {
          if (result.success) {
            this.card.success(result.messages, 2000);
          } else {
            this.card.error(result.errors, 2000);
          }
          this.setBasicState();
          this.refreshTable();
        }
      );
    } else {
      this.setBasicState();
    }
  }

  showEditForm(resultLine: any) {
    this.showAddForm = false;
    this.showEditBtn = false;
    this.showDeleteBtn = false;

    this.editedRow = resultLine;
  }

  confirmAdd() {
    this.showAddForm = false;
    this.showEditBtn = false;
    this.showDeleteBtn = false;

    this.showAddConfirmation = true;
  }

  confirmEdit() {
    this.showAddForm = false;
    this.showEditBtn = false;
    this.showDeleteBtn = false;

    this.showEditConfirmation = true;
  }

  confirmDelete(id: string) {
    this.showAddForm = false;
    this.showEditBtn = false;
    this.showDeleteBtn = false;

    this.showDelConfirmation = true;
    this.deletedRow = {id: id};
  }

  cancelEdit() {
    this.setBasicState();
    this.refreshTable();
  }

  setBasicState() {
    this.showAddForm = true;
    this.showEditBtn = true;
    this.showDeleteBtn = true;

    this.showAddConfirmation = false;
    this.showEditConfirmation = false;
    this.showDelConfirmation = false;

    this.editedRow = undefined;
    this.addedRow = {};
    this.deletedRow = undefined;
  }
}
