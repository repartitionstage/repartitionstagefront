import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'rs-confirmation-card',
  templateUrl: './confirmation-card.component.html',
  styleUrls: ['./confirmation-card.component.scss']
})
export class ConfirmationCardComponent implements OnInit {

  @Input() input: {
    title: string,
    additionalInfo?: string
    textBtnConfirm?: string,
    textBtnCancel?: string,
  };
  @Output() output = new EventEmitter();

  constructor() {
  }

  ngOnInit() {

  }

  confirm() {
    this.output.emit(true);
  }

  cancel() {
    this.output.emit(false);
  }
}
