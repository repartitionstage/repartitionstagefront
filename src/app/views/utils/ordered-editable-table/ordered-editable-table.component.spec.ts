import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrderedEditableTableComponent} from './ordered-editable-table.component';

describe('OrderedEditableTableComponent', () => {
  let component: OrderedEditableTableComponent;
  let fixture: ComponentFixture<OrderedEditableTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderedEditableTableComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderedEditableTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should post', () => {
    expect(component).toBeTruthy();
  });
});
