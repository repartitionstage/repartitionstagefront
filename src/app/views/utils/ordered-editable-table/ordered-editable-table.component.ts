import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Result} from '../../../service/rest-api/result';
import {DragulaService} from 'ng2-dragula';
import {Subscription} from 'rxjs';
import {InfoCardComponent} from '../info-card/info-card.component';

@Component({
  selector: 'rs-ordered-editable-table',
  templateUrl: './ordered-editable-table.component.html',
  styleUrls: ['./ordered-editable-table.component.scss']
})
export class OrderedEditableTableComponent implements OnInit, OnDestroy {

  constructor(private el: ElementRef,
              private dragulaService: DragulaService) {
  }

  @ViewChild('tablebody') tablebody: ElementRef;

  @ViewChild('card') card: InfoCardComponent;

  /**
   * Must have a column "id" and a column "order"
   */
  @Input() columns: {
    id: string,
    name: string,
    editType?: string,
    choices?: { name: string, value: number }[],
    editable: boolean,
    visible: boolean,
    width: number
  }[];

  @Input() data: any;
  @Input() add: Function;
  @Input() edit: Function;
  @Input() del: Function;
  @Input() get: Function;
  @Input() min: number;
  @Input() pas: number;

  page = 0;

  resultLines = [];

  addedRow = {};
  showAddForm = true;
  showAddConfirmation = false;
  confirmAddData = {
    title: 'Confirmer l\'ajout ?'
  };

  editedRow;
  showEditBtn = true;
  showEditConfirmation = false;
  confirmEditData = {
    title: 'Voulez-vous sauvegarder ?'
  };

  deletedRow;
  showDeleteBtn = true;
  showDelConfirmation = false;
  confirmDeleteData = {
    title: 'Confirmer la suppression ?',
  };
  subs = new Subscription();

  ngOnInit() {
    this.refreshTable();

    this.subs.add(
      this.dragulaService.drop('RESULT_LINE')
        .subscribe(() => {
            this.updateAndSaveOrder();
          }
        )
    );
  }

  updateAndSaveOrder() {
    const tbody = this.tablebody.nativeElement;
    let i = 0;
    for (const el of tbody.children) {
      const id = Number(el.id.split('row-')[1]);
      for (const line of this.resultLines) {
        if (line['id'] === id) {
          line['order'] = i;
          i++;
          break;
        }
      }
    }
    i = 0;
    for (const line of this.resultLines) {
      this.edit(line).subscribe(
        (result: Result) => {
          this.setResultMessages(result);

          i++;
          if (i === this.resultLines.length) {
            this.refreshTable();
            this.setBasicState();
          }
        }
      );
    }
  }

  refreshTable() {
    this.setBasicState();
    this.get(this.min * this.page, this.pas).subscribe(
      (result: Result) => {
        if (result.success) {
          this.resultLines = result.data;
          this.resultLines.sort((a, b) => {
            if (a.order < b.order) {
              return -1;
            } else if (a.order > b.order) {
              return 1;
            } else {
              return 0;
            }
          });
        }
        this.setResultMessages(result);
      }
    );
  }

  addElement($event: any) {
    if ($event) {
      this.addedRow['order'] = this.resultLines.length;
      this.add(this.addedRow).subscribe(
        (result: Result) => {
          this.setResultMessages(result);
          this.setBasicState();
          this.refreshTable();
        }
      );
    } else {
      this.setBasicState();
    }
  }

  editElement($event: any) {
    if ($event) {
      this.edit(this.editedRow).subscribe(
        (result: Result) => {
          this.setResultMessages(result);
          this.setBasicState();
          this.refreshTable();
        }
      );
    } else {
      this.setBasicState();
      this.refreshTable();

    }
  }

  delElement($event: any) {
    if ($event) {
      this.del(this.deletedRow.id).subscribe(
        (result: Result) => {
          this.setResultMessages(result);
          for (let i = 0; i < this.resultLines.length; i++) {
            if (this.resultLines[i].id === this.deletedRow.id) {
              break;
            }
          }
          this.updateAndSaveOrder();
        }
      );
    } else {
      this.setBasicState();
    }
  }

  showEditForm(resultLine: any) {
    this.showAddForm = false;
    this.showEditBtn = false;
    this.showDeleteBtn = false;

    this.editedRow = resultLine;
  }

  confirmAdd() {
    this.showAddForm = false;
    this.showEditBtn = false;
    this.showDeleteBtn = false;

    this.showAddConfirmation = true;
  }

  confirmEdit() {
    this.showAddForm = false;
    this.showEditBtn = false;
    this.showDeleteBtn = false;

    this.showEditConfirmation = true;
  }

  confirmDelete(id: string) {
    this.showAddForm = false;
    this.showEditBtn = false;
    this.showDeleteBtn = false;

    this.showDelConfirmation = true;
    this.deletedRow = {id: id};
  }

  cancelEdit() {
    this.setBasicState();
    this.refreshTable();
  }

  setBasicState() {
    this.showAddForm = true;
    this.showEditBtn = true;
    this.showDeleteBtn = true;

    this.showAddConfirmation = false;
    this.showEditConfirmation = false;
    this.showDelConfirmation = false;

    this.editedRow = undefined;
    this.addedRow = {};
    this.deletedRow = undefined;
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  setResultMessages(result: Result) {
    if (result.success) {
      this.card.success(result.messages, 2000);
    } else {
      this.card.error(result.errors, 2000);
    }
  }

  getChoiceName(choices: { name: string, value: number }[], value: string | number): string {
    for (const choice of choices) {
      if (choice.value === value) {
        return choice.name;
      }
    }
    return 'Non défini';
  }
}
