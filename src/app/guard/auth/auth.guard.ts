import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../../service/auth/auth.service';
import {TokenService} from '../../service/token/token.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private tokenService: TokenService,
              private authService: AuthService,
              private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return new Observable<boolean>(
      observer => {
        this.tokenService.isConnect().subscribe(
          (isConnect) => {
            if (isConnect) {
              observer.next(true);
            } else {
              this.authService.redirectURL = state.url;
              this.router.navigate(['/auth/login']);
              observer.next(false);
            }
          }
        );
      }
    );
  }
}
