import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {UserService} from '../../service/user/user.service';
import {Result} from '../../service/rest-api/result';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {


  constructor(private userService: UserService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Observable<boolean>(
      observer => {
        this.userService.isAdmin()
          .subscribe(
          (result: Result) => {
            if (result.success && result.data.admin) {
              observer.next(true);
            } else {
              observer.next(false);
            }
          }
        );
      }
    );
  }
}
