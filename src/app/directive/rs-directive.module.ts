import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminDirective} from './admin/admin.directive';
import {NotAuthDirective} from './not-auth/not-auth.directive';
import {AuthDirective} from './auth/auth.directive';

@NgModule({
  declarations: [
    AdminDirective,
    AuthDirective,
    NotAuthDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    AdminDirective,
    AuthDirective,
    NotAuthDirective
  ]
})
export class RsDirectiveModule {
}
