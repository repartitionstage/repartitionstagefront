import {Directive, ElementRef, OnInit, Renderer2} from '@angular/core';
import {TokenService} from '../../service/token/token.service';

@Directive({
  selector: '[rsNotAuth]'
})
export class NotAuthDirective implements OnInit {

  constructor(private el: ElementRef,
              private renderer: Renderer2,
              private tokenService: TokenService,
  ) {

  }

  ngOnInit(): void {
    this.tokenService.isConnect().subscribe(
      (isConnect) => {
        if (isConnect) {
          this.renderer.parentNode(this.el.nativeElement).removeChild(this.el.nativeElement);
        }
      }
    );
  }

}
