import {Directive, ElementRef, OnInit, Renderer2} from '@angular/core';
import {UserService} from '../../service/user/user.service';
import {Result} from '../../service/rest-api/result';

@Directive({
  selector: '[rsAdmin]'
})
export class AdminDirective implements OnInit {

  constructor(private el: ElementRef,
              private renderer: Renderer2,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.userService.isAdmin()
      .subscribe(
        (result: Result) => {
          if (!result.success || !result.data.admin) {
            this.renderer.parentNode(this.el.nativeElement).removeChild(this.el.nativeElement);
          }
        }
      );
  }
}
