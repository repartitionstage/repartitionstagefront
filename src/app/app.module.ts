import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import localeFr from '@angular/common/locales/fr';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {adminLteConf} from './admin-lte-conf';
import {BoxModule, LayoutModule} from 'angular-admin-lte';
import {FormsModule} from '@angular/forms';
import {CookieService} from 'ngx-cookie-service';
import {AuthService} from './service/auth/auth.service';
import {RestApiService} from './service/rest-api/rest-api.service';
import {AuthGuard} from './guard/auth/auth.guard';
import {HttpClientModule} from '@angular/common/http';
import {HeaderNavBarComponent} from './views/header-nav-bar/header-nav-bar.component';
import {RsDirectiveModule} from './directive/rs-directive.module';
import {NgxEditorModule} from 'ngx-editor';
import {DragulaModule} from 'ng2-dragula';
import {HashLocationStrategy, LocationStrategy, registerLocaleData} from '@angular/common';
import {RsUtilsModule} from './views/utils/rs-utils.module';
import {CollapseModule} from 'ngx-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CookieLawModule} from 'angular2-cookie-law';

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    HeaderNavBarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BoxModule,
    LayoutModule.forRoot(adminLteConf),
    RsDirectiveModule,
    NgxEditorModule,
    DragulaModule.forRoot(),
    RsUtilsModule,
    CollapseModule.forRoot(),
    BrowserAnimationsModule,
    CookieLawModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    RestApiService,
    CookieService,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {provide: LOCALE_ID, useValue: 'fr'},
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
