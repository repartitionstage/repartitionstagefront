export const adminLteConf = {
  skin: 'blue',
  layout: 'fixed',
  isSidebarLeftCollapsed: true,
  isSidebarLeftExpandOnOver: true,
  sidebarRightSkin: 'dark',
  sidebarLeftMenu: [
    {label: 'NAVIGATION', separator: true},
    {label: 'Répartition des stages', route: '/private/distribution', iconClasses: 'fa fa-calendar'},
    {label: 'Mes stages', route: '/private/interships', iconClasses: 'fa fa-stethoscope'},
    {label: 'Planning de garde', route: '/private/all-gardes', iconClasses: 'fa fa-calendar'},
    {label: 'Mes gardes', route: '/private/own-gardes', iconClasses: 'fa fa-ambulance'},
    // {label: 'Echange de gardes', route: '/private/exchange-gardes', iconClasses: 'fa fa-handshake-o'},
    // {label: 'FAQ', route: '/private/faq', iconClasses: 'fa fa-question-circle'},
    {label: 'Documents', route: '/private/document', iconClasses: 'fa fa-file'},
    {label: 'Contacts', route: '/private/contact', iconClasses: 'fa fa-address-book'},
    {label: 'PARTENAIRES', separator: true},
    {label: 'BNP', route: '/public/partners/bnp', iconClasses: 'fa fa-star'},
    {label: 'MACSF', route: '/public/partners/macsf', iconClasses: 'fa fa-star'},
    // {label: 'TROLL', separator: true},
    // {label: 'Photos Marv', route: '/public/marvin', iconClasses: 'fa fa-camera'},
  ]
};


