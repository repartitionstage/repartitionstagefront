import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SafeHtmlPipe} from './safe-html/safe-html.pipe';
import {PeriodsByGroupPipe} from './periods-by-group/periods-by-group.pipe';

@NgModule({
  declarations: [
    SafeHtmlPipe,
    PeriodsByGroupPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SafeHtmlPipe,
    PeriodsByGroupPipe
  ]
})
export class RsPipeModule {
}
