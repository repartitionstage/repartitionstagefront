import {Pipe, PipeTransform} from '@angular/core';
import {Period} from '../../service/distribution/models/period';
import {Group} from '../../service/distribution/models/group.enum';
import {KeyValue} from '@angular/common';

@Pipe({
  name: 'periodsByGroup'
})
export class PeriodsByGroupPipe implements PipeTransform {

  transform(periods: Map<number, Period>, args?: Group): KeyValue<number, Period>[] {
    const keyvalue: KeyValue<number, Period>[] = [];

    periods.forEach(
      (value, key) => {
        if (value.group === args || value.group === Group.TWICE) {
          keyvalue.push({key: key, value: value});
        }
      }
    );

    return keyvalue;
  }

}
