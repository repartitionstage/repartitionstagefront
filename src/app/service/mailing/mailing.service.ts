import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Result} from '../rest-api/result';
import {RestApiService} from '../rest-api/rest-api.service';
import {PATH_MAILING_CSV} from '../../config';

@Injectable({
  providedIn: 'root'
})
export class MailingService {

  constructor(private api: RestApiService) {
  }

  getMailingCSV(promotionId: number, title: string): Observable<Result> {
    return this.api.downloadFile(PATH_MAILING_CSV, {promotionId}, {}, title);
  }

}
