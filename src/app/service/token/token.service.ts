import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import * as jwt_decode from 'jwt-decode';
import {environment} from '../../../environments/environment';
import {PATH_AUTH_ACCESSTK_REFRESH, PATH_AUTH_REFRESHTK_ISVLD} from '../../config';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private accessTokenCookieName = 'access_token';
  private refreshTokenCookieName = 'refresh_token';

  private lastAccessTokenDate = 0;
  private refreshTokenDate = 0;

  constructor(private httpClient: HttpClient,
              private cookieService: CookieService) {
  }

  static getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }

  getAccessTokenPayload(): any {
    return TokenService.getDecodedAccessToken(this.cookieService.get(this.accessTokenCookieName));
  }

  setAccessToken(token: string): void {
    this.updateLastAccessTokenDate();
    const accessExpireDate = new Date();
    accessExpireDate.setMinutes(accessExpireDate.getMinutes() + 5);
    this.cookieService.set(this.accessTokenCookieName, token, accessExpireDate);
  }

  setRefreshToken(token: string): void {
    this.updateRefreshTokenDate();
    const refreshExpireDate = new Date();
    refreshExpireDate.setHours(refreshExpireDate.getHours() + 2);
    this.cookieService.set(this.refreshTokenCookieName, token, refreshExpireDate);
  }

  deleteAccessToken(): void {
    this.cookieService.delete(this.accessTokenCookieName);
    this.lastAccessTokenDate = 0;
  }

  deleteRefreshToken(): void {
    this.cookieService.delete(this.refreshTokenCookieName);
  }

  /**
   * Refresh current access token if possible. Return new access token if request succeed, return null if not.
   */
  refreshAccessToken(): Observable<string> {
    return new Observable<string>(
      observer => {
        if (this.isRefreshTokenStored()) {
          const headers = new HttpHeaders();
          headers.set('Content-Type', 'application/x-www-form-urlencoded');

          const httpParams = new HttpParams().set('token', this.cookieService.get(this.refreshTokenCookieName));

          this.httpClient.get(
            environment.API_ENDPOINT.concat(PATH_AUTH_ACCESSTK_REFRESH.toString()),
            {headers: headers, params: httpParams})
            .subscribe(
              (response: any) => {
                this.updateLastAccessTokenDate();
                this.setAccessToken(response['accessToken']);
                observer.next(this.cookieService.get(this.accessTokenCookieName));
              },
              (response: any) => {
                observer.error(response.error);
              }
            );
        } else {
          observer.error();
        }
      }
    );
  }

  /**
   * Check if refresh token is always valid
   */
  isRefreshTokenValid(): Observable<boolean> {
    return new Observable<boolean>(
      observer => {
        if (this.isRefreshTokenStored()) {

          if (this.isRefreshTokenStillValidInTime()) {
            observer.next(true);
          } else {

            const headers = new HttpHeaders();
            headers.set('Content-Type', 'application/x-www-form-urlencoded');

            const body = new HttpParams()
              .set('token', this.cookieService.get(this.refreshTokenCookieName));

            this.httpClient.get(environment.API_ENDPOINT.concat(PATH_AUTH_REFRESHTK_ISVLD.toString()), {headers: headers, params: body})
              .subscribe(
                () => {
                  observer.next(true);
                },
                () => {
                  observer.next(false);
                }
              );
          }
        } else {
          observer.next(false);
        }
      }
    );
  }

  /**
   * Check if access token is always valid
   */
  isAccessTokenValid(): Observable<boolean> {
    return new Observable<boolean>(
      observer => {
        observer.next(this.isAccessTokenStored() && this.isAccessTokenStillValidInTime());
      });
  }

  getAccessToken(): Observable<string> {
    return new Observable<string>(
      observer => {
        this.isAccessTokenValid()
          .subscribe(
            (isValid: boolean) => {
              if (isValid) {
                observer.next(this.cookieService.get(this.accessTokenCookieName));
              } else {
                this.refreshAccessToken()
                  .subscribe(
                    (value) => {
                      observer.next(value);
                    },
                    (error) => {
                      observer.error(error);
                    }
                  );
              }
            }
          );
      }
    );
  }

  isConnect(): Observable<boolean> {
    return new Observable<boolean>(
      observer => {
        if (this.isRefreshTokenStored()) {
          this.isRefreshTokenValid()
            .subscribe(
              (valid: boolean) => {
                observer.next(valid);
              }
            );
        } else {
          observer.next(false);
        }
      }
    );
  }

  isAccessTokenStillValidInTime(): boolean { // Access token is valid during 5 minutes, reduce here to 4min45sec to be sure
    const currentDate = new Date().getTime() / 1000;
    return !((currentDate - this.lastAccessTokenDate) > (4 * 60 + 45));
  }

  isRefreshTokenStillValidInTime(): boolean { // Refresh token is valid during 2 hours
    const currentDate = new Date().getTime() / 1000;
    return !((currentDate - this.lastAccessTokenDate) > (2 * 60 * 60));
  }

  updateLastAccessTokenDate(): void {
    this.lastAccessTokenDate = new Date().getTime() / 1000;
  }

  updateRefreshTokenDate(): void {
    this.refreshTokenDate = new Date().getTime() / 1000;
  }

  isRefreshTokenStored(): boolean {
    return this.cookieService.get(this.refreshTokenCookieName) !== undefined
      && this.cookieService.get(this.refreshTokenCookieName) !== '';
  }

  isAccessTokenStored(): boolean {
    return this.cookieService.get(this.accessTokenCookieName) !== undefined
      && this.cookieService.get(this.accessTokenCookieName) !== '';
  }
}
