export class User {

  id: number;
  firstName: string;
  lastName: string;
  email: string;
  studentNumber: number;
  phone: string;
  promotionId: number;
  group: number;
  redoubling: boolean | number;
  admin: boolean;
}
