import {Injectable} from '@angular/core';
import {RestApiService} from '../rest-api/rest-api.service';
import {Observable} from 'rxjs';
import {Result} from '../rest-api/result';
import {
  PATH_USER,
  PATH_USER_ADMIN,
  PATH_USER_BY_PROMOTION,
  PATH_USER_BY_USER_PROMOTION,
  PATH_USER_GROUP,
  PATH_USER_REDOUBLING,
  PATH_USER_SWITCH_PROMOTION,
  PATH_USER_UPDATE_PASSWORD,
  PATH_USER_WITH_ID
} from '../../config';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  admin: boolean;

  constructor(private api: RestApiService) {
  }

  getProfileInfos(userId: number | 'self'): Observable<Result> {
    return this.api.get(PATH_USER_WITH_ID, {id: userId}, {type: 'profile'});
  }

  getHeaderInfos(): Observable<Result> {
    return this.api.get(PATH_USER_WITH_ID, {id: 'self'}, {type: 'header'});
  }

  getUsersByPromotion(promotionId: number): Observable<Result> {
    return this.api.get(PATH_USER_BY_PROMOTION, {id: promotionId});
  }

  getUsersByUserPromotion(): Observable<Result> {
    return this.api.get(PATH_USER_BY_USER_PROMOTION);
  }

  isAdmin(): Observable<Result> {
    return new Observable<Result>(
      observer => {
        if (this.admin) {
          const result = new Result();
          result.success = true;
          result.data.admin = true;
          observer.next(result);
        } else {
          this.isUserAdmin('self')
            .subscribe(
              (result: Result) => {
                if (result.success) {
                  this.admin = result.data.admin;
                }
                observer.next(result);
              }
            );
        }
      }
    );
  }

  isUserAdmin(userId: number | 'self'): Observable<Result> {
    return this.api.get(PATH_USER_ADMIN, {userId});
  }

  setUserAdmin(userId: number, admin: boolean): Observable<Result> {
    return this.api.post(PATH_USER_ADMIN, {userId}, {admin});
  }

  updateUser(user: { id: number, }): Observable<Result> {
    delete user['admin'];
    return this.api.put(PATH_USER_WITH_ID, {id: user.id}, user);
  }

  updatePassword(data: { password: string }): Observable<Result> {
    return this.api.put(PATH_USER_UPDATE_PASSWORD, {}, data);
  }

  updateGroup(groupId: number): Observable<Result> {
    return this.api.put(PATH_USER_GROUP, {}, {group: groupId});
  }

  getAllUsers(limit: number = 0, offset: number = 0): Observable<Result> {
    return this.api.get(PATH_USER, {}, {limit, offset});
  }

  setRedoubling(selectedIds: number[], redoubling: boolean): Observable<Result> {
    return this.api.put(PATH_USER_REDOUBLING, {}, {ids: selectedIds, redoubling: redoubling});
  }

  switchNotRedoublingToPromotion(promotionFromId: number, promotionToId: number): Observable<Result> {
    return this.api.put(PATH_USER_SWITCH_PROMOTION, {promotionFromId, promotionToId});
  }
}
