import {Injectable} from '@angular/core';
import * as sha1 from 'js-sha1';


@Injectable({
  providedIn: 'root'
})
export class EncryptService {

  constructor() {
  }


  static reverseString(input: string): string {
    return input.split('').reverse().join('');
  }

  static getRandomChar(): string {
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@&#,;:=-_$*£%!()[]';
    return possible.charAt(Math.floor(Math.random() * possible.length));
  }

  static localEncryptData(input: string): string {
    let returnString = '';

    for (let i = 0; i < input.length; i++) {
      for (let j = 0; j < 3; j++) {
        returnString += EncryptService.getRandomChar();
      }
      returnString += input[input.length - (i + 1)];
    }

    return returnString;
  }

  static localDecryptPassword(input: string): string {
    let stockInput: string = input;
    let returnString = '';

    for (let i = 0; i < input.length / 4; i++) {
      stockInput = stockInput.substring(3, stockInput.length);
      returnString += stockInput[0];
      stockInput = stockInput.substring(1, stockInput.length);
    }

    return EncryptService.reverseString(returnString);
  }

  static encryptForBackEndPassword(input: string): string {
    return sha1(('intranetUltraVraiment').concat(input).concat('TropCoolSaMere'));
  }
}
