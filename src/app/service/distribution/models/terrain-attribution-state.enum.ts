export enum TerrainAttributionState {
  WAITING = 0,
  REFUSED = 1,
  ACCEPTED = 2,
  NOT_PRIORITY = 3,
}
