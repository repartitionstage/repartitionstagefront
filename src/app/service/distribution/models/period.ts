export class Period {

  id: number;
  order: number;
  beginDate: string;
  endDate: string;
  group: number;
  yearId: number;
  promotionId: number;
}
