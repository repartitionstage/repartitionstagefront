export class TerrainType {

  id: number;
  name: string;
  locationId: number;
  desc: number;

  categoriesIds: number[];

  maxChoices: number;
  maxAttributions: number;
  maxAttributionsPerYear: number;

}
