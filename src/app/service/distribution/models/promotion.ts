export class Promotion {

  id: number;
  name: string;
  selectableOnRegistration: boolean;


  constructor(id: number, name: string, selectableOnRegistration: boolean) {
    this.id = id;
    this.name = name;
    this.selectableOnRegistration = selectableOnRegistration;
  }
}
