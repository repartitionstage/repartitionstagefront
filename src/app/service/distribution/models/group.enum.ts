export enum Group {
  NONE = 1,
  A = 2,
  B = 3,
  TWICE = 4
}
