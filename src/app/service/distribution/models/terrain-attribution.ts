import {TerrainAttributionState} from './terrain-attribution-state.enum';

export class TerrainAttribution {

  firstName?: string;
  lastName?: string;
  id?: number;
  userId?: number;
  terrainId?: number;
  state?: TerrainAttributionState;

}
