export class Terrain {
  id: number;
  maxAmountPlaces: number;
  periodId: number;
  promotionId: number;
  terrainTypeId: number;
  yearId: number;

  constructor(maxAmountPlaces: number, periodId: number, promotionId: number, terrainTypeId: number, yearId: number, id?: number) {
    this.maxAmountPlaces = maxAmountPlaces;
    this.periodId = periodId;
    this.promotionId = promotionId;
    this.terrainTypeId = terrainTypeId;
    this.yearId = yearId;
    if (id) {
      this.id = id;
    }
  }
}
