export class Location {

  id: number;
  name: string;
  distance: number;

}
