export class Phase {
  id: number;
  order: number;
  name: string;
  beginDate: Date;
  endDate: Date;
  active: boolean;
  finish: boolean;
  promotionId: number;
  yearId: number;

  config: { type: string, htmlContent?: string } | string;

  constructor(id: number, order: number, name: string, beginDate: Date, endDate: Date, active: boolean, finish: boolean, promotionId: number, yearId: number) {
    this.id = id;
    this.order = order;
    this.name = name;
    this.beginDate = beginDate;
    this.endDate = endDate;
    this.active = active;
    this.finish = finish;
    this.promotionId = promotionId;
    this.yearId = yearId;
  }

}
