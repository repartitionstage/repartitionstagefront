import {Injectable} from '@angular/core';
import {RestApiService} from '../rest-api/rest-api.service';
import {Observable} from 'rxjs';
import {Result} from '../rest-api/result';
import {
  PATH_DISTRIBUTION_ACTIVE_YEAR,
  PATH_DISTRIBUTION_ACTIVE_YEAR_WITH_ID,
  PATH_DISTRIBUTION_ADMIN_TERRAIN_ATTRIBUTION,
  PATH_DISTRIBUTION_CATEGORIES,
  PATH_DISTRIBUTION_CATEGORIES_WITH_ID,
  PATH_DISTRIBUTION_EXPORT_CSV,
  PATH_DISTRIBUTION_EXPORT_PDF,
  PATH_DISTRIBUTION_LOCATIONS,
  PATH_DISTRIBUTION_LOCATIONS_WITH_ID,
  PATH_DISTRIBUTION_PERIODS,
  PATH_DISTRIBUTION_PERIODS_BLOCKED,
  PATH_DISTRIBUTION_PERIODS_WITH_ID,
  PATH_DISTRIBUTION_PHASE_ACTIVE_WITH_ID,
  PATH_DISTRIBUTION_PHASES,
  PATH_DISTRIBUTION_PHASES_BY_PROMOTION_AND_YEAR,
  PATH_DISTRIBUTION_PHASES_BY_USER,
  PATH_DISTRIBUTION_PHASES_WITH_ID,
  PATH_DISTRIBUTION_PROMOTIONS,
  PATH_DISTRIBUTION_PROMOTIONS_WITH_ID,
  PATH_DISTRIBUTION_TERRAIN,
  PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION,
  PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_BY_PHASE,
  PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_BY_USER,
  PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_MULTIPLE,
  PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_PRIVATE,
  PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_WITH_ID,
  PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_WITH_ID_PRIVATE,
  PATH_DISTRIBUTION_TERRAIN_WITH_ID,
  PATH_DISTRIBUTION_TERRAIN_WITH_YEAR_ID_AND_PROMOTION_ID,
  PATH_DISTRIBUTION_TERRAINTYPES,
  PATH_DISTRIBUTION_TERRAINTYPES_OPEN_WITH_YEAR_ID_AND_PROMOTION_ID,
  PATH_DISTRIBUTION_TERRAINTYPES_WITH_ID,
  PATH_DISTRIBUTION_YEARS,
  PATH_DISTRIBUTION_YEARS_WITH_ID,
} from '../../config';
import {Phase} from './models/phase';
import {Terrain} from './models/terrain';
import {TerrainAttribution} from './models/terrain-attribution';
import {TerrainAttributionState} from './models/terrain-attribution-state.enum';

@Injectable({
  providedIn: 'root'
})
export class DistributionService {

  constructor(private api: RestApiService) {
  }

  /**
   * YEAR
   */
  addYear(year: any): Observable<Result> {
    return this.api.post(PATH_DISTRIBUTION_YEARS, {}, year);
  }

  getYears(offset: number = 0, limit: number = 0): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_YEARS, {}, {offset: offset, limit: limit});
  }

  updateYear(year: any): Observable<Result> {
    return this.api.put(PATH_DISTRIBUTION_YEARS_WITH_ID, {id: year.id}, year);
  }

  deleteYear(yearId: number): Observable<Result> {
    return this.api.del(PATH_DISTRIBUTION_YEARS_WITH_ID, {id: yearId});
  }

  getActiveYear(): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_ACTIVE_YEAR, {}, {});
  }

  updateActiveYear(yearId: number): Observable<Result> {
    return this.api.put(PATH_DISTRIBUTION_ACTIVE_YEAR_WITH_ID, {id: yearId});
  }

  /**
   * PROMOTION
   */
  addPromotion(promotion: any): Observable<Result> {
    return this.api.post(PATH_DISTRIBUTION_PROMOTIONS, {}, promotion);
  }

  getPromotion(promotionId: number): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_PROMOTIONS_WITH_ID, {id: promotionId});
  }

  getPromotions(onlySelectableOnRegistration: boolean = false, offset: number = 0, limit: number = 0): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_PROMOTIONS, {}, {onlySelectableOnRegistration: onlySelectableOnRegistration});
  }

  updatePromotion(promotion: any): Observable<Result> {
    return this.api.put(PATH_DISTRIBUTION_PROMOTIONS_WITH_ID, {id: promotion.id}, promotion);
  }

  deletePromotion(promotionId: number): Observable<Result> {
    return this.api.del(PATH_DISTRIBUTION_PROMOTIONS_WITH_ID, {id: promotionId});
  }

  /**
   * PHASE
   */
  addPhase(phase: any): Observable<Result> {
    phase.config = JSON.stringify(phase.config);
    return this.api.post(PATH_DISTRIBUTION_PHASES, {}, phase);
  }

  getPhase(id: number): Observable<Result> {
    return new Observable<Result>(
      (observer) => {
        this.api.get(PATH_DISTRIBUTION_PHASES_WITH_ID, {id: id}).subscribe(
          (result: Result) => {
            if (result.success) {
              const phase = new Phase(result.data.id, result.data.order, result.data.name, new Date(result.data.beginDate), new Date(result.data.endDate), result.data.active, result.data.finish, result.data.promotionId, result.data.yearId);
              phase.config = JSON.parse(result.data.config);
              result.data = phase;
            }
            observer.next(result);
          }
        );
      }
    );
  }

  getPhases(yearId: number | 'active', promotionId: number, offset: number = 0, limit: number = 0): Observable<Result> {
    return new Observable<Result>(
      (observer) => {
        this.api.get(PATH_DISTRIBUTION_PHASES_BY_PROMOTION_AND_YEAR,
          {promotionId: promotionId, yearId: yearId},
          {offset: offset, limit: limit})
          .subscribe(
            (result: Result) => {
              if (result.success) {
                const phases = [];
                for (const data of result.data) {
                  const phase = new Phase(data.id, data.order, data.name, new Date(data.beginDate), new Date(data.endDate), data.active, data.finish, data.promotionId, data.yearId);
                  phase.config = JSON.parse(data.config);
                  phases.push(phase);
                }
                result.data = phases;
              }
              observer.next(result);
            }
          );
      }
    );
  }

  updatePhase(phase: any): Observable<Result> {
    phase.config = JSON.stringify(phase.config);
    return this.api.put(PATH_DISTRIBUTION_PHASES_WITH_ID, {id: phase.id}, phase);
  }

  deletePhase(phaseId: number): Observable<Result> {
    return this.api.del(PATH_DISTRIBUTION_PHASES_WITH_ID, {id: phaseId});
  }

  getUserPhases(): Observable<Result> {
    return new Observable<Result>(
      observer => {
        this.api.get(PATH_DISTRIBUTION_PHASES_BY_USER)
          .subscribe(
            (result: Result) => {
              if (result.success) {
                const phases = [];
                for (const data of result.data) {
                  const phase = new Phase(data.id, data.order, data.name, new Date(data.beginDate), new Date(data.endDate), data.active, data.finish, data.promotionId, data.yearId);
                  phase.config = JSON.parse(data.config);
                  phases.push(phase);
                }
                result.data = phases;
              }
              observer.next(result);
            }
          );
      }
    );
  }

  activePhase(phaseId: number): Observable<Result> {
    return this.api.put(PATH_DISTRIBUTION_PHASE_ACTIVE_WITH_ID, {id: phaseId});
  }

  /**
   * LOCATION
   */
  addLocation(location: any): Observable<Result> {
    return this.api.post(PATH_DISTRIBUTION_LOCATIONS, {}, location);
  }

  getLocations(offset: number, limit: number): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_LOCATIONS, {}, {offset: offset, limit: limit});
  }

  updateLocation(location: any): Observable<Result> {
    return this.api.put(PATH_DISTRIBUTION_LOCATIONS_WITH_ID, {id: location.id}, location);
  }

  deleteLocation(locationId: number): Observable<Result> {
    return this.api.del(PATH_DISTRIBUTION_LOCATIONS_WITH_ID, {id: locationId});
  }

  /**
   * CATEGORY
   */
  addCategory(category: any): Observable<Result> {
    return this.api.post(PATH_DISTRIBUTION_CATEGORIES, {}, category);
  }

  getCategories(offset: number = 0, limit: number = 0): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_CATEGORIES, {}, {offset: offset, limit: limit});
  }

  updateCategory(category: any): Observable<Result> {
    return this.api.put(PATH_DISTRIBUTION_CATEGORIES_WITH_ID, {id: category.id}, category);
  }

  deleteCategory(categoryId: number): Observable<Result> {
    return this.api.del(PATH_DISTRIBUTION_CATEGORIES_WITH_ID, {id: categoryId});

  }

  /**
   * PERIOD
   */
  addPeriod(period: any): Observable<Result> {
    return this.api.post(PATH_DISTRIBUTION_PERIODS, {}, period);
  }

  getPeriods(yearId: number | 'active', promotionId: number): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_PERIODS, {}, {promotionId: promotionId, yearId: yearId});
  }

  updatePeriod(period: any): Observable<Result> {
    return this.api.put(PATH_DISTRIBUTION_PERIODS_WITH_ID, {id: period.id}, period);
  }

  deletePeriod(id: number): Observable<Result> {
    return this.api.del(PATH_DISTRIBUTION_PERIODS_WITH_ID, {id: id});
  }

  getBlockedPeriods(): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_PERIODS_BLOCKED, {}, {});
  }

  /**
   * TERRAINTYPE
   */
  addTerrainType(object: any): Observable<Result> {
    return this.api.post(PATH_DISTRIBUTION_TERRAINTYPES, {}, object);
  }

  getTerrainTypes(offset: number = 0, limit: number = 0): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_TERRAINTYPES, {}, {offset: offset, limit: limit});
  }

  updateTerrainType(object: any): Observable<Result> {
    return this.api.put(PATH_DISTRIBUTION_TERRAINTYPES_WITH_ID, {id: object.id}, object);
  }

  deleteTerrainType(id: number): Observable<Result> {
    return this.api.del(PATH_DISTRIBUTION_TERRAINTYPES_WITH_ID, {id: id});
  }

  getOpenTerrainTypeByYearAndPromotion(yearId: number, promotionId: number): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_TERRAINTYPES_OPEN_WITH_YEAR_ID_AND_PROMOTION_ID, {yearId: yearId, promotionId: promotionId}, {});
  }

  /**
   * TERRAIN
   */
  addTerrain(object: any): Observable<Result> {
    return this.api.post(PATH_DISTRIBUTION_TERRAIN, {}, object);
  }

  getTerrains(offset: number = 0, limit: number = 0): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_TERRAIN, {}, {offset: offset, limit: limit});
  }

  updateTerrain(object: any): Observable<Result> {
    return this.api.put(PATH_DISTRIBUTION_TERRAIN_WITH_ID, {id: object.id}, object);
  }

  deleteTerrain(id: number): Observable<Result> {
    return this.api.del(PATH_DISTRIBUTION_TERRAIN_WITH_ID, {id: id});
  }

  updateTerrainsByYearAndPromotion(yearId: number, promotionId: number, values: { toUpdate: Terrain[], toDelete: number[] }): Observable<Result> {
    return this.api.post(PATH_DISTRIBUTION_TERRAIN_WITH_YEAR_ID_AND_PROMOTION_ID, {yearId: yearId, promotionId: promotionId}, values);
  }

  getTerrainsByYearAndPromotion(yearId: number | 'active', promotionId: number): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_TERRAIN_WITH_YEAR_ID_AND_PROMOTION_ID, {yearId: yearId, promotionId: promotionId});
  }

  /**
   * TERRAIN ATTRIBUTIONS
   */
  addTerrainAttribution(object: TerrainAttribution): Observable<Result> {
    delete object.firstName;
    delete object.lastName;
    return this.api.post(PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION, {}, object);
  }

  updateTerrainAttribution(object: TerrainAttribution, phaseId: number): Observable<Result> {
    delete object.firstName;
    delete object.lastName;

    return this.api.put(PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_WITH_ID, {id: object.id, phaseId}, object);
  }

  deleteTerrainAttribution(id: number, phaseId?: number): Observable<Result> {
    return this.api.del(PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_WITH_ID, {id, phaseId});
  }

  getTerrainAttributionByPhase(phaseId: number): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_BY_PHASE, {phaseId});
  }

  getTerrainAttributionByUser(userId: 'self' | number = 'self'): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_BY_USER, {userId});
  }

  getTerrainAttributionByPhaseAndGroup(phaseId: number): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_BY_PHASE, {phaseId}, {group: true});
  }

  getTerrainAttributionForAdmin(phaseId: number, groupId: number, withAssets: boolean): Observable<Result> {
    return this.api.get(PATH_DISTRIBUTION_ADMIN_TERRAIN_ATTRIBUTION, {}, {phaseId, groupId, withAssets});
  }

  validateMultipleTerrainAttributions(phaseId: number, terrainId: number): Observable<Result> {
    return this.api.put(PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_MULTIPLE, {terrainId}, {state: TerrainAttributionState.ACCEPTED, phaseId});
  }

  unvalidateMultipleTerrainAttributions(phaseId: number, terrainId: number): Observable<Result> {
    return this.api.put(PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_MULTIPLE, {terrainId}, {state: TerrainAttributionState.WAITING, phaseId});
  }

  deleteMultipleTerrainAttributions(terrainId: number): Observable<Result> {
    return this.api.del(PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_MULTIPLE, {terrainId});
  }

  addTerrainAttributionOnDistribution(object: TerrainAttribution): Observable<Result> {
    return this.api.post(PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_PRIVATE, {}, object);
  }

  deleteTerrainAttributionOnDistribution(id: number): Observable<Result> {
    return this.api.del(PATH_DISTRIBUTION_TERRAIN_ATTRIBUTION_WITH_ID_PRIVATE, {id});
  }

  getDistributionExportation(yearId: number | 'active', promotionId: number, sortType: 'user' | 'terrain', categoryId?: number, title?: string, mimeType?: string): Observable<Result> {
    return this.api.downloadFile(PATH_DISTRIBUTION_EXPORT_PDF, {}, {yearId, promotionId, sortType, categoryId}, title, mimeType);
  }

  getDistributionExportationAsCSV(yearId: number | 'active'): Observable<Result> {
    return this.api.downloadFile(PATH_DISTRIBUTION_EXPORT_CSV, {}, {yearId});
  }


}
