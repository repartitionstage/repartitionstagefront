import {TestBed} from '@angular/core/testing';

import {GardeService} from './garde.service';

describe('GardeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GardeService = TestBed.get(GardeService);
    expect(service).toBeTruthy();
  });
});
