export class Garde {
  id: number;

  date: string;

  terrainId: number;
  terrainName: string;
  isNight: boolean;

  userId: number;
  userFirstName: string;
  userLastName: string;
}
