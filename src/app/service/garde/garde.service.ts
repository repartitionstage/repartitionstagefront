import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Result} from '../rest-api/result';
import {
  PATH_GARDE_CONFIG_WITH_YEAR_ID,
  PATH_GARDE_PLANNING_WITH_YEAR_ID,
  PATH_GARDE_TERRAIN,
  PATH_GARDE_TERRAIN_WITH_ID,
  PATH_GARDE_WEEK_HOLYDAYS_BY_DATE,
  PATH_GARDES,
  PATH_GARDES_FREE,
  PATH_GARDES_FREE_WITH_ID,
  PATH_GARDES_WITH_ID,
} from '../../config';
import {RestApiService} from '../rest-api/rest-api.service';

@Injectable({
  providedIn: 'root'
})
export class GardeService {

  constructor(private api: RestApiService) {
  }

  // TERRAINS
  addTerrain(terrain: any): Observable<Result> {
    return this.api.post(PATH_GARDE_TERRAIN, {}, terrain);
  }

  getTerrain(terrainId: number): Observable<Result> {
    return this.api.get(PATH_GARDE_TERRAIN_WITH_ID, {id: terrainId});
  }

  getTerrains(offset: number = 0, limit: number = 0): Observable<Result> {
    return this.api.get(PATH_GARDE_TERRAIN);
  }

  updateTerrain(terrain: any): Observable<Result> {
    return this.api.put(PATH_GARDE_TERRAIN_WITH_ID, {id: terrain.id}, terrain);
  }

  deleteTerrain(terrainId: number): Observable<Result> {
    return this.api.del(PATH_GARDE_TERRAIN_WITH_ID, {id: terrainId});
  }


  // CONFIG
  getConfig(yearId: number): Observable<Result> {
    return this.api.get(PATH_GARDE_CONFIG_WITH_YEAR_ID, {yearId});
  }

  updateConfig(yearId: number, config: any): Observable<Result> {
    return this.api.put(PATH_GARDE_CONFIG_WITH_YEAR_ID, {yearId}, {config});
  }

  genPlanning(yearId: number): Observable<Result> {
    return this.api.put(PATH_GARDE_PLANNING_WITH_YEAR_ID, {yearId}, {});
  }

  downloadPlanning(yearId: number, title: string): Observable<Result> {
    return this.api.downloadFile(PATH_GARDE_PLANNING_WITH_YEAR_ID, {yearId}, {}, title, 'application/ms-excel');
  }

  getWeekHolydaysByDate(date: string): Observable<Result> {
    return this.api.get(PATH_GARDE_WEEK_HOLYDAYS_BY_DATE, {}, {date});
  }

  // GARDES

  getGardesByUserId(userId: number | 'self'): Observable<Result> {
    return this.api.get(PATH_GARDES, {}, {userId});
  }

  getWeekGardesByDate(date: string): Observable<Result> {
    return this.api.get(PATH_GARDES, {}, {date, week: true});
  }

  updateGarde(object: any): Observable<Result> {
    return this.api.put(PATH_GARDES_WITH_ID, {id: object.id}, object);
  }

  getFreeGardes(): Observable<Result> {
    return this.api.get(PATH_GARDES_FREE);
  }

  removeUserFromGarde(gardeId: number): Observable<Result> {
    return this.api.del(PATH_GARDES_FREE_WITH_ID, {gardeId});
  }
}
