export class GardeTerrain {
  id: number;
  name: string;
  description: string;
  night: boolean;
  collapsed?: boolean;
}
