import {Injectable} from '@angular/core';
import {RestApiService} from '../rest-api/rest-api.service';
import {Observable} from 'rxjs';
import {Result} from '../rest-api/result';
import {PATH_CONTACT, PATH_CONTACT_WITH_ID} from '../../config';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private api: RestApiService) {
  }

  addContact(year: any): Observable<Result> {
    return this.api.post(PATH_CONTACT, {}, year);
  }

  getContacts(offset: number = 0, limit: number = 0): Observable<Result> {
    return this.api.get(PATH_CONTACT, {}, {offset: offset, limit: limit});
  }

  updateContact(contact: any): Observable<Result> {
    return this.api.put(PATH_CONTACT_WITH_ID, {id: contact.id}, contact);
  }

  deleteContact(id: number): Observable<Result> {
    return this.api.del(PATH_CONTACT_WITH_ID, {id});
  }

}
