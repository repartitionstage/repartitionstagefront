export class Contact {

  id: number;
  name: string;
  description: string;
  contact: string;

}
