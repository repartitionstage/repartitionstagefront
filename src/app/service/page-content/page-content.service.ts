import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Result} from '../rest-api/result';
import {RestApiService} from '../rest-api/rest-api.service';
import {PATH_PAGE_CONTENT, PATH_PAGE_CONTENT_WITH_NAME, PATH_PAGE_NAMES} from '../../config';

@Injectable({
  providedIn: 'root'
})
export class PageContentService {

  constructor(private api: RestApiService) {
  }

  updatePageContent(name: string, htmlContent: string): Observable<Result> {
    return this.api.post(PATH_PAGE_CONTENT, {}, {name: name, htmlContent: htmlContent});
  }

  getPageContent(name: string): Observable<Result> {
    return this.api.get(PATH_PAGE_CONTENT_WITH_NAME, {name: name});
  }

  getPageNames(): Observable<Result> {
    return this.api.get(PATH_PAGE_NAMES);
  }
}
