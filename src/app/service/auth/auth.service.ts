import {Injectable} from '@angular/core';
import {
  PATH_AUTH_LOGIN,
  PATH_USER,
  PATH_USER_REQUEST_PASSWORD,
  PATH_USER_RESET_PASSWORD,
  PATH_USER_RESET_PASSWORD_WITH_TOKEN
} from '../../config';
import {Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';
import {RegistrationRequest} from './registration-request';
import {TokenService} from '../token/token.service';
import {RestApiService} from '../rest-api/rest-api.service';
import {Result} from '../rest-api/result';
import {LayoutStore} from 'angular-admin-lte';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public redirectURL = '';

  constructor(private cookieService: CookieService,
              private api: RestApiService,
              private tokenService: TokenService,
              private layout: LayoutStore) {
  }

  register(user: RegistrationRequest): Observable<Result> {
    return new Observable<Result>(
      (observer) => {
        if (user.isComplete()) {
          delete user.isComplete;
          this.api.post(PATH_USER, {}, user)
            .subscribe(
              (result: Result) => {
                observer.next(result);
              }
            );
        } else {
          const result = new Result();
          result.errors = ['L\'inscription n\'est pas complète.'];
          result.success = false;
          observer.next(result);
        }
      }
    );
  }

  login(email: string, password: string): Observable<Result> {
    return new Observable<Result>(
      (observer) => {
        this.api.post(PATH_AUTH_LOGIN, {}, {email: email, password: password})
          .subscribe(
            (response) => {
              if (response.success) {
                this.tokenService.setAccessToken(response.data['accessToken']);
                this.tokenService.setRefreshToken(response.data['refreshToken']);
                if (email === 'marvin_charlier@orange.fr') {
                  this.layout.setSkin('purple-light');
                }
              }
              observer.next(response);
            }
          );
      }
    );
  }

  logout(): void {
    this.tokenService.deleteAccessToken();
    this.tokenService.deleteRefreshToken();
  }

  requestPassword(email: string): Observable<Result> {
    return this.api.post(PATH_USER_REQUEST_PASSWORD, {}, {email: email});
  }

  resetPassword(token: string, password: string): Observable<Result> {
    return this.api.post(PATH_USER_RESET_PASSWORD, {}, {token: token, password: password});
  }

  isRPTokenValid(token: string): Observable<Result> {
    return this.api.get(PATH_USER_RESET_PASSWORD_WITH_TOKEN, {token: token});
  }

}
