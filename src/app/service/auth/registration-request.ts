export class RegistrationRequest {

  firstName: string;
  lastName: string;
  email: string;
  studentNumber: number;
  phone: string;
  promotionId: number;
  password: string;

  isComplete(): boolean {
    for (const x in this) {
      if (this.hasOwnProperty(x) && this[x] === undefined) {
        return false;
      }
    }
    return true;
  }
}
