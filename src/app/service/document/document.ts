export class Document {
  id: number;
  name: string;
  description: string;
  promotionId: number;
  fileName: string;
}
