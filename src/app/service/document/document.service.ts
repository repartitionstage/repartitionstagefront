import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Result} from '../rest-api/result';
import {RestApiService} from '../rest-api/rest-api.service';
import {PATH_FILE_DOCUMENT, PATH_FILE_DOCUMENT_BY_PROMOTION, PATH_FILE_DOCUMENT_WITH_ID} from '../../config';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(private api: RestApiService) {
  }

  uploadNewDocument(formData: FormData): Observable<Result> {
    return this.api.uploadFile(PATH_FILE_DOCUMENT, {}, formData);
  }

  downloadDocument(id: number, fileName?: string): Observable<Result> {
    return this.api.downloadFile(PATH_FILE_DOCUMENT_WITH_ID, {id}, {}, fileName);
  }

  getDocumentListByPromotion(promotionId: number): Observable<Result> {
    return this.api.get(PATH_FILE_DOCUMENT_BY_PROMOTION, {promotionId});
  }

  deleteDocument(id: number): Observable<Result> {
    return this.api.del(PATH_FILE_DOCUMENT_WITH_ID, {id});
  }

  getDocumentList(offset: number = 0, limit: number = 0) {
    return this.api.get(PATH_FILE_DOCUMENT, {}, {offset, limit});
  }
}
