export class Result {

  get success(): boolean {
    return this._success;
  }

  set success(value: boolean) {
    this._success = value;
  }

  get messages(): string[] {
    return this._messages;
  }

  set messages(value: string[]) {
    this._messages = value;
  }

  get errors(): string[] {
    return this._errors;
  }

  set errors(value: string[]) {
    this._errors = value;
  }

  private _success = false;
  private _messages: string[] = [];
  private _errors: string[] = [];

  private _data: any = {};

  get data(): any {
    return this._data;
  }

  set data(value: any) {
    this._data = value;
  }

}
