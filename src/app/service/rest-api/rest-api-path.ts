export class RestApiPath {

  path: string;

  protection: {
    post?: boolean,
    get?: boolean,
    put?: boolean,
    del?: boolean
  };


  constructor(path: string, protection: { post?: boolean; get?: boolean; put?: boolean; del?: boolean } = {}) {
    this.path = path;
    this.protection = protection;
  }

  public toString = (): string => this.path;


  public isPostProtected(): boolean {
    return this.protection.post === true;
  }

  public isGetProtected(): boolean {
    return this.protection.get === true;
  }

  public isPutProtected(): boolean {
    return this.protection.put === true;
  }

  public isDelProtected(): boolean {
    return this.protection.del === true;
  }

  public isPostAllowed(): boolean {
    return this.protection.post !== undefined;
  }

  public isGetAllowed(): boolean {
    return this.protection.get !== undefined;
  }

  public isPutAllowed(): boolean {
    return this.protection.put !== undefined;
  }

  public isDelAllowed(): boolean {
    return this.protection.del !== undefined;
  }
}
