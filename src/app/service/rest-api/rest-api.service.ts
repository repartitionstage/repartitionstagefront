import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TokenService} from '../token/token.service';
import {environment} from '../../../environments/environment';
import {RestApiPath} from './rest-api-path';
import {Result} from './result';
import {saveAs} from 'file-saver/src/FileSaver';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {


  constructor(private httpClient: HttpClient,
              private tokenService: TokenService,
              private router: Router) {
  }

  static ERRORS = {
    'error-occurs': 'Une erreur est survenue.',
    'sql-error': 'Une erreur SQL est survenue.',
    'invalid-format': 'Format de l\'objet invalide',
    'object-not-found': 'Objet introuvable',
    'invalid-access-token': 'Token d\'accès invalide',
    'invalid-resfresh-token': 'Token de rafraichissement invalide',
    'admin-privileges-required': 'Des privilèges administrateurs sont requis',
    'token-not-load': 'Le token n\'a pas été chargé sur la route',
    'params-taken': 'Paramètres déjà utilisés',
    'user-password-mismatch': 'L\'email et le mot de passe ne correspondent pas.',
    'mail-sending-error': 'Le mail n\'a pas pu être envoyé',

    'invalid-active-phase': 'La phase activé n\'est pas la bonne pour effectuer cela.',
    'invalid-distribution-phase': 'La phase de la répartition comporte un problème.',

    'not-redoubling': 'Cette personne ne redouble pas.',
    'group-blocked': 'Le groupe de cette personne est bloqué.',

    'too-much-choice': 'Le nombre maximal de choix a été atteint.',
    'too-much-choice-per-period': 'Le nombre maximal de choix pour cette période a été atteint.',
    'too-much-choice-per-category': 'Le nombre maximal de choix pour cette catégorie a été atteint.',

    'already-attrib': 'Cette personne a déjà une attribution sur cette période.'
  };

  static parseErrors(result: Result, reason: string): void {
    const value = this.ERRORS[reason] ? this.ERRORS[reason] : reason;
    result.errors.push(value);
  }

  errorHandler = (observer) => (error) => {
    const result = new Result();
    result.success = false;
    result.errors = ['Votre token n\'est plus valide', 'Merci de vous reconnecter'];
    observer.next(result);
    setTimeout(
      () => {
        this.router.navigate(['/auth/login']);
      }, 2000);
  };

  /**
   * POST request
   * @param path restApi path
   * @param params arguments directly in url
   * @param object data to post
   */
  post(path: RestApiPath, params: {}, object: {}): Observable<Result> {
    if (!path.isPostAllowed()) {
      console.error('Trying to make a post request which is not support on path : ' + path.toString());
    } else {
      return new Observable<Result>(
        (observer) => {
          let headers = new HttpHeaders().append('Content-Type', 'application/x-www-form-urlencoded');

          let pathStr = path.toString();
          for (const param in params) {
            if (params.hasOwnProperty(param)) {
              pathStr = pathStr.replace(':'.concat(param), params[param]);
            }
          }

          const httpParams = new HttpParams().append('object', JSON.stringify(object));
          const send = () => {
            const result = new Result();
            this.httpClient.post(environment.API_ENDPOINT.concat(pathStr), httpParams, {headers: headers})
              .subscribe(
                (response: any) => {
                  result.success = true;
                  if (response) {
                    result.data = response.data;
                  }
                  observer.next(result);
                },
                (response) => {
                  result.success = false;
                  RestApiService.parseErrors(result, response.error.reason);
                  observer.next(result);
                }
              );
          };

          if (path.isPostProtected()) {
            this.tokenService.getAccessToken()
              .subscribe(
                (accessToken) => {
                  headers = headers.append('authorization', 'Bearer ' + accessToken);
                  send();
                },
                this.errorHandler(observer)
              );
          } else {
            send();
          }
        }
      );
    }
  }

  /**
   * GET request
   * @param path restApi path
   * @param params argument before '?'
   * @param query argument after '?'
   */
  get(path: RestApiPath, params: {} = {}, query: {} = {}): Observable<Result> {
    if (!path.isGetAllowed()) {
      console.error('Trying to make a get request which is not support on path : ' + path.toString());
    } else {
      return new Observable<Result>(
        (observer) => {
          let headers = new HttpHeaders().append('Content-Type', 'application/x-www-form-urlencoded');

          let pathStr = path.toString();
          for (const param in params) {
            if (params.hasOwnProperty(param)) {
              pathStr = pathStr.replace(':'.concat(param), params[param]);
            }
          }

          const send = () => {
            const result = new Result();
            this.httpClient.get(environment.API_ENDPOINT.concat(pathStr), {headers: headers, params: query})
              .subscribe(
                (response: any) => {
                  result.success = true;
                  if (response) {
                    result.data = response.data;
                  }
                  observer.next(result);
                },
                (response: any) => {
                  result.success = false;
                  RestApiService.parseErrors(result, response.error.reason);
                  observer.next(result);
                }
              );
          };

          if (path.isGetProtected()) {
            this.tokenService.getAccessToken()
              .subscribe(
                (accessToken) => {
                  headers = headers.append('authorization', 'Bearer ' + accessToken);
                  send();
                },
                this.errorHandler(observer)
              );
          } else {
            send();
          }
        }
      );
    }
  }

  /**
   * PUT request
   * @param path restApi path
   * @param params arguments directly in url
   * @param object request body
   */
  put(path: RestApiPath, params: {} = {}, object: {} = {}): Observable<Result> {
    if (!path.isPutAllowed()) {
      console.error('Trying to make a put request which is not support on path : ' + path.toString());
    } else {
      return new Observable<any>(
        (observer) => {
          let headers = new HttpHeaders().append('Content-Type', 'application/x-www-form-urlencoded');

          let pathStr = path.toString();
          for (const param in params) {
            if (params.hasOwnProperty(param)) {
              pathStr = pathStr.replace(':'.concat(param), params[param]);
            }
          }

          const httpParams = new HttpParams().append('object', JSON.stringify(object));


          const send = () => {
            const result = new Result();
            this.httpClient.put(environment.API_ENDPOINT.concat(pathStr), httpParams, {headers: headers})
              .subscribe(
                () => {
                  result.success = true;
                  observer.next(result);
                },
                (response: any) => {
                  result.success = false;
                  RestApiService.parseErrors(result, response.error.reason);
                  observer.next(result);
                }
              );
          };

          if (path.isPutProtected()) {
            this.tokenService.getAccessToken()
              .subscribe(
                (accessToken) => {
                  headers = headers.append('authorization', 'Bearer ' + accessToken);
                  send();
                },
                this.errorHandler(observer)
              );
          } else {
            send();
          }
        }
      );
    }
  }

  /**
   * DELETE request
   * @param path restApi path
   * @param params argument directly in url
   */
  del(path: RestApiPath, params: {} = {}): Observable<Result> {
    if (!path.isDelAllowed()) {
      console.error('Trying to make a delete request which is not support on path : ' + path.toString());
    } else {
      return new Observable<any>(
        (observer) => {
          let headers = new HttpHeaders().append('Content-Type', 'application/x-www-form-urlencoded');

          let pathStr = path.toString();
          for (const param in params) {
            if (params.hasOwnProperty(param)) {
              pathStr = pathStr.replace(':'.concat(param), params[param]);
            }
          }

          const send = () => {
            const result = new Result();
            this.httpClient.delete(environment.API_ENDPOINT.concat(pathStr), {headers: headers})
              .subscribe(
                () => {
                  result.success = true;
                  observer.next(result);
                },
                (response: any) => {
                  result.success = false;
                  RestApiService.parseErrors(result, response.error.reason);
                  observer.next(result);
                }
              );
          };

          if (path.isDelProtected()) {
            this.tokenService.getAccessToken()
              .subscribe(
                (accessToken) => {
                  headers = headers.append('authorization', 'Bearer ' + accessToken);
                  send();
                },
                this.errorHandler(observer)
              );
          } else {
            send();
          }
        }
      );
    }
  }

  downloadFile(path: RestApiPath, params: {} = {}, query: {} = {}, fileName: string = 'NoName', mimeType: string = 'application/octet-stream'): Observable<Result> {
    if (!path.isGetAllowed()) {
      console.error('Trying to make a download file get request which is not support on path : ' + path.toString());
    } else {
      return new Observable<Result>(
        (observer) => {
          let headers = new HttpHeaders().append('Content-Type', 'application/x-www-form-urlencoded');

          let pathStr = path.toString();
          for (const param in params) {
            if (params.hasOwnProperty(param)) {
              pathStr = pathStr.replace(':'.concat(param), params[param]);
            }
          }

          const send = () => {
            const result = new Result();
            this.httpClient.get(environment.API_ENDPOINT.concat(pathStr), {
              responseType: 'arraybuffer',
              headers: headers,
              params: query,
              observe: 'response'
            })
              .subscribe(
                (response: any) => {
                  const fileNameFromResult = (
                    response.headers.get('Content-Disposition')
                      .split(';')[1]
                      .trim()
                      .split('=')[1]
                      .replace(/"/gm, '')
                      .replace(/%20/g, ' '));
                  const file = new File([response.body], (fileNameFromResult ? fileNameFromResult : fileName), {type: mimeType});
                  saveAs(file);

                  result.success = true;
                  observer.next(result);
                },
                (response: any) => {
                  result.success = false;
                  RestApiService.parseErrors(result, response.error.reason);
                  observer.next(result);
                }
              );
          };

          if (path.isGetProtected()) {
            this.tokenService.getAccessToken()
              .subscribe(
                (accessToken) => {
                  headers = headers.append('authorization', 'Bearer ' + accessToken);
                  send();
                },
                this.errorHandler(observer)
              );
          } else {
            send();
          }
        }
      );
    }
  }

  uploadFile(path: RestApiPath, params: {} = {}, formData: FormData) {
    if (!path.isPostAllowed()) {
      console.error('Trying to make a post request which is not support on path : ' + path.toString());
    } else {
      return new Observable<Result>(
        (observer) => {
          let headers = new HttpHeaders();

          let pathStr = path.toString();
          for (const param in params) {
            if (params.hasOwnProperty(param)) {
              pathStr = pathStr.replace(':'.concat(param), params[param]);
            }
          }

          const send = () => {
            const result = new Result();
            this.httpClient.post(environment.API_ENDPOINT.concat(pathStr), formData, {headers})
              .subscribe(
                (response: any) => {
                  result.success = true;
                  if (response) {
                    result.data = response.data;
                  }
                  observer.next(result);
                },
                (response) => {
                  result.success = false;
                  RestApiService.parseErrors(result, response.error.reason);
                  observer.next(result);
                }
              );
          };

          if (path.isPostProtected()) {
            this.tokenService.getAccessToken()
              .subscribe(
                (accessToken) => {
                  headers = headers.append('authorization', 'Bearer ' + accessToken);
                  send();
                },
                this.errorHandler(observer)
              );
          } else {
            send();
          }
        }
      );
    }
  }
}
