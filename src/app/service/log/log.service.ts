import {Injectable} from '@angular/core';
import {RestApiService} from '../rest-api/rest-api.service';
import {PATH_LOGS} from '../../config';
import {Observable} from 'rxjs';
import {Result} from '../rest-api/result';
import {User} from '../user/user';
import {TerrainAttributionState} from '../distribution/models/terrain-attribution-state.enum';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  static TYPES = [
    {
      type: 'LOGIN',
      shortTranslation: 'Connexion',
      longTranslation: '%target% s\'est connecté.'
    },
    {
      type: 'ADD_PERSONAL_ATTRIBUTION',
      shortTranslation: 'Rajout personnel sur un terrain',
      longTranslation: '%target% s\'est ajouté sur le terrain n°%terrainId%.'

    },
    {
      type: 'REMOVE_PERSONAL_ATTRIBUTION',
      shortTranslation: 'Retrait personnel d\'un terrain',
      longTranslation: '%target% s\'est retiré du terrain n°%terrainId%.'
    },
    {
      type: 'ADD_ADMIN_ATTRIBUTION',
      shortTranslation: 'Rajout par un admin sur un terrain',
      longTranslation: '%target% s\'est fait ajouté sur le terrain n°%terrainId% par %executor%.'
    },
    {
      type: 'REMOVE_ADMIN_ATTRIBUTION',
      shortTranslation: 'Retrait par un admin d\'un terrain',
      longTranslation: '%target% s\'est fait retiré du terrain n°%terrainId% par %executor%.'
    },
    {
      type: 'UPDATE_ATTRIBUTION_STATE',
      shortTranslation: 'Mise à jour de l\'état d\'une attribution par un admin',
      longTranslation: '%executor% a %attrib-state% l\'attribution de %target% sur le terrain n°%terrainId%.'
    },
    {
      type: 'CHANGE_PERSONAL_GROUP',
      shortTranslation: 'Changement personnel de groupe',
      longTranslation: '%target% a changé son groupe pour %group%.'
    },
    {
      type: 'CHANGE_ADMIN_GROUP',
      shortTranslation: 'Changement par un admin de groupe',
      longTranslation: '%executor% a changé le groupe de %target% pour %group%.'
    },
  ];


  constructor(private api: RestApiService) {
  }

  getLogs(executorId: number, targetId: number, type: string): Observable<Result> {
    return this.api.get(PATH_LOGS, {}, {executorId, targetId, type});
  }

  getLongTranslation(type: string) {
    for (const line of LogService.TYPES) {
      if (line.type === type) {
        return line.longTranslation;
      }
    }
  }

  getUserNameFromId(users: User[], id: number): string {
    for (const user of users) {
      if (user.id === id) {
        return user.firstName + ' ' + user.lastName;
      }
    }
  }

  getGroupTranslation(group: number) {
    switch (group) {
      case 1:
        return 'aucun groupe';
      case 2:
        return 'le groupe A';
      case 3:
        return 'le groupe B';
      case 4:
        return 'les deux groupes';
    }
  }


  getAttribStateTranslation(state: number): string {
    switch (state) {
      case TerrainAttributionState.WAITING:
        return 'mis en attente';
      case TerrainAttributionState.ACCEPTED:
        return 'validé';
      case TerrainAttributionState.NOT_PRIORITY:
        return 'a mis en non prioritaire';
    }
  }

  getTranslation(type: string, executorId: number, targetId: number, data: any, users: User[]): string {
    const ref = this.getLongTranslation(type);
    let lt = ref.slice(0, ref.length);

    lt = lt.replace('%target%', this.getUserNameFromId(users, targetId));
    lt = lt.replace('%executor%', this.getUserNameFromId(users, executorId));

    if (lt.indexOf('%terrainId%') !== -1) {
      lt = lt.replace('%terrainId%', data.terrainId);
    }

    if (lt.indexOf('%group%') !== -1) {
      lt = lt.replace('%group%', this.getGroupTranslation(data.toGroup));
    }

    if (lt.indexOf('%attrib-state%') !== -1) {
      lt = lt.replace('%attrib-state%', this.getAttribStateTranslation(data.state));
    }

    return lt;
  }

}
