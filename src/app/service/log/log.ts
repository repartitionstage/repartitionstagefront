export class Log {

  date: number;
  executorId: number;
  executorIp: string;
  targetId: number;
  type: string;
  data: string | any;
}
