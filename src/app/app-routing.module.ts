import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminGuard} from './guard/admin/admin.guard';
import {AuthGuard} from './guard/auth/auth.guard';

const routes: Routes = [
  {
    path: 'public',
    loadChildren: './views/public/public.module#PublicModule'
  },
  {
    path: 'auth',
    loadChildren: './views/auth/auth.module#AuthModule'
  },
  {
    path: 'private',
    canActivate: [AuthGuard],
    loadChildren: './views/private/private.module#PrivateModule',
  },
  {
    path: 'admin',
    canActivate: [AuthGuard, AdminGuard],
    loadChildren: './views/admin/admin.module#AdminModule'
  },
  {
    path: '*',
    redirectTo: 'public',
  },
  {
    path: '',
    redirectTo: 'public',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
